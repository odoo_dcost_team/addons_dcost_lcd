## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com 
{
    "name": "Sequence Number Dcost",
    "version": "1.0",
    "category": "Custom Module",
    "website": "http://ibrohimbinladin.wordpress.com",
    "author": "Ibrohim Binladin | +62838-7190-9782 | ibradiiin@gmail.com",
    "description": """Modul kustom ini digunakan untuk menghasilkan nomor urut setiap dokumen (generate sequence number) dengan menyertakan kode warehouse (outlet).""",
    "init_xml": [],
    "application": True,
    "installable": True,
    "depends": [
        "account",
        "purchase",
        "stock",
        "siu_import_pos",
    ],
    "data": [
        "security/ir.model.access.csv",
        ##"data/journal_data.xml",
        "views/sequence.xml",
        "views/sequence_view.xml",
        "views/purchase_view.xml",
        "views/account_view.xml",
    ],
    "demo_xml": [],
    "active": False,
}
