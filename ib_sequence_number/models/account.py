# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
###########################################################################################################
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm
from openerp.osv import osv, fields as Fields

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.one
    @api.depends('invoice_line.price_subtotal', 'tax_line.amount', 'round_off')  #'discount_method','discount'
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
        self.amount_tax = sum(line.amount for line in self.tax_line)
        self.amount_total = self.amount_untaxed + self.amount_tax + self.round_off
        # account_ids = self.env['account.account'].search(['|', ('code', 'like', '%240000.00.07%'),
        #                 ('name', 'like', '%Potongan Penjualan%')], limit=1)
        # subtotal = []
        # for line in self.invoice_line:
        #     if (line.account_id.id == account_ids[-1].id or line.name == 'Discount') and line.price_subtotal < 0.0:
        #         subtotal.append(line.price_subtotal * -1)
        #     else:
        #         subtotal.append(line.price_subtotal)
        #self.amount_untaxed = sum(subtotal)
        # if self.discount_method == 'fixed':
        #     self.amount_discount = self.discount
        #     self.amount_tax = self.amount_tax - ((self.amount_tax / self.amount_untaxed) * self.discount)
        # elif self.discount_method == 'percent':
        #     self.amount_discount = self.amount_untaxed * ((self.discount or 0.0) / 100.0)
        #     self.amount_tax = self.amount_tax - (self.amount_tax * ((self.discount or 0.0) / 100.0))
        # self.amount_total = (self.amount_untaxed - self.amount_discount) + self.amount_tax + self.round_off

    round_off = fields.Float(string='Round Off', digits=dp.get_precision('Account'),
            readonly=True, states={'draft': [('readonly', False)]}, default=0.0, help="Round Off Amount")
    # amount_discount = fields.Float(string='Discount', digits=dp.get_precision('Account'),
    #         store=True, readonly=True, compute='_compute_amount')
    discount_method = fields.Selection([('percent', 'Percentage'), ('fixed', 'Fixed Amount')], string='Discount Method',
            readonly=True, states={'draft': [('readonly', False)]})  #default='none'
    discount = fields.Float(string='Discount', digits=dp.get_precision('Account'),
            readonly=True, states={'draft': [('readonly', False)]}, default=0.0)
    supplier_invoice_number = fields.Char(string='Supplier Invoice Number',
            readonly=True, states={'draft': [('readonly', False)],'open': [('readonly', False)],
                'proforma': [('readonly', False)]}, help="The reference of this invoice as provided by the supplier.")

    @api.multi
    def onchange_discount_method(self, method):
        for invoice in self:
            if method in ('percent','fixed'):
                for line in invoice.invoice_line:
                    if line.discount > 0.0 or line.discount_method in ('percent', 'fixed'):
                        raise except_orm(_('Error!'),
                            _('Diskon total tidak bisa digunakan karena sudah ada diskon per produk, silahkan cek produknya.'))
                    else:
                        return {'value': {'discount_method': method, 'discount': 0.0}}
        return {'value': {'discount_method': '' or False, 'discount': 0.0}}

    @api.multi
    def action_number(self):
        # TODO: not correct fix but required a fresh values before reading it.
        self.write({})

        for inv in self:
            if inv.type in ('in_invoice', 'in_refund'):
                ref = inv.reference
                if inv.warehouse_id:
                    wh_code = inv.warehouse_id.code
                elif inv.user_id:
                    usr = self.env['res.users'].browse(inv.user_id.id)
                    if usr.warehouse_id:
                        wh_code = usr.warehouse_id.code
                    elif len(str(usr.login).split(".")) == 2:
                        wh_code = str(usr.login).split(".")[1]
                    elif len(str(usr.login).split(" ")) == 2:
                        wh_code = str(usr.login).split(" ")[1]
                    else:
                        wh_code = 'XXX'
                else:
                    user = self.env['res.users'].browse(self._uid)
                    if user.warehouse_id:
                        wh_code = user.warehouse_id.code
                    elif inv.warehouse_id:
                        wh_code = inv.warehouse_id.code
                    elif len(str(usr.login).split(".")) == 2:
                        wh_code = str(usr.login).split(".")[1]
                    elif len(str(usr.login).split(" ")) == 2:
                        wh_code = str(usr.login).split(" ")[1]
                    else:
                        wh_code = 'XXX'
                inv_sequences = self.env['ir.sequence'].get('supplier.invoice.dcost')
                supp_inv = list(str(inv_sequences))
                supp_inv.insert(4, wh_code + "/")
                nomor_inv = ''.join(supp_inv)
            else:
                ref = nomor_inv = inv.number

            self.write({'internal_number': nomor_inv, 'number': nomor_inv})  #inv.number

            if inv.type in ('in_invoice', 'in_refund'):
                if not inv.reference:
                    ref = inv.number
                else:
                    ref = inv.reference
            else:
                ref = inv.number

            if inv.type in ('in_invoice', 'in_refund'):
                self._cr.execute(""" UPDATE account_move SET ref=%s, name=%s 
                                WHERE id=%s AND (ref IS NULL OR ref = '')""",
                                 (ref, nomor_inv, inv.move_id.id))
            else:
                self._cr.execute(""" UPDATE account_move SET ref=%s
                                   WHERE id=%s AND (ref IS NULL OR ref = '')""",
                                     (ref, inv.move_id.id))
            self._cr.execute(""" UPDATE account_move_line SET ref=%s
                               WHERE move_id=%s AND (ref IS NULL OR ref = '')""",
                                 (ref, inv.move_id.id))
            self._cr.execute(""" UPDATE account_analytic_line SET ref=%s
                               FROM account_move_line
                               WHERE account_move_line.move_id = %s AND
                                     account_analytic_line.move_id = account_move_line.id""",
                                 (ref, inv.move_id.id))
            self.invalidate_cache()

        return True



class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity', 'product_id',
                 'invoice_id.partner_id', 'invoice_id.currency_id', 'discount_method', 'account_id', 'name')
    def _compute_price(self):
        price = self.price_unit * (1 - (self.discount or 0.0) / 100.0)
        if self.discount_method == 'fixed': #qty = 1.0  #qty = self.quantity
            price = self.price_unit - self.discount
        taxes = self.invoice_line_tax_id.compute_all(price, self.quantity, product=self.product_id,
                                                     partner=self.invoice_id.partner_id)
        self.price_subtotal = taxes['total']
        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)


    discount_method = fields.Selection([('fixed', 'Fixed Amount'), ('percent', 'Percentage')],
            string='Discount Method', change_default=True)
    discount = fields.Float(string='Discount', digits=dp.get_precision('Account'), default=0.0)
    price_subtotal = fields.Float(string='Amount', digits=dp.get_precision('Account'),
            store=True, readonly=True, compute='_compute_price')

    @api.multi
    def onchange_discount(self):
        return {'value': {'discount': 0.0}}

    # @api.multi
    # def product_id_change(self, product, uom_id, qty=0, name='', type='out_invoice',
    #                       partner_id=False, fposition_id=False, price_unit=False, currency_id=False,
    #                       company_id=None, warehouse_id=False, user_id=False):
    #     context = self._context
    #     company_id = company_id if company_id is not None else context.get('company_id', False)
    #     self = self.with_context(company_id=company_id, force_company=company_id)
    #     warehouse_id = self.env['stock.warehouse'].browse(warehouse_id).id
    #     if not warehouse_id:
    #         if user_id:
    #             warehouse_id = self.env['res.users'].browse(user_id).warehouse_id.id
    #         else:
    #             warehouse_id = self.env['res.users'].browse(self._uid).warehouse_id.id or False
    #
    #     if not partner_id:
    #         raise except_orm(_('No Partner Defined!'), _("You must first select a partner!"))
    #     if not product:
    #         if type in ('in_invoice', 'in_refund'):
    #             return {'value': {}, 'domain': {'uos_id': []}}
    #         else:
    #             return {'value': {'price_unit': 0.0}, 'domain': {'uos_id': []}}
    #
    #     values = {}
    #
    #     part = self.env['res.partner'].browse(partner_id)
    #     fpos = self.env['account.fiscal.position'].browse(fposition_id)
    #
    #     if part.lang:
    #         self = self.with_context(lang=part.lang)
    #     product = self.env['product.product'].browse(product)
    #
    #     values['name'] = product.partner_ref
    #     if type in ('out_invoice', 'out_refund'):
    #         account = product.property_account_income or product.categ_id.property_account_income_categ
    #     else:
    #         account = product.property_account_expense or product.categ_id.property_account_expense_categ
    #     account = fpos.map_account(account)
    #     if account:
    #         values['account_id'] = account.id
    #
    #     if type in ('out_invoice', 'out_refund'):
    #         taxes = product.taxes_id or account.tax_ids
    #         if product.description_sale:
    #             values['name'] += '\n' + product.description_sale
    #     else:
    #         taxes = product.supplier_taxes_id or account.tax_ids
    #         if product.description_purchase:
    #             values['name'] += '\n' + product.description_purchase
    #
    #     fp_taxes = fpos.map_tax(taxes)
    #     values['invoice_line_tax_id'] = fp_taxes.ids
    #
    #     if type in ('in_invoice', 'in_refund'):
    #         if price_unit and price_unit != product.standard_price:
    #             values['price_unit'] = price_unit
    #         else:
    #             values['price_unit'] = self.env['account.tax']._fix_tax_included_price(product.standard_price, taxes,
    #                                                                                    fp_taxes.ids)
    #     else:
    #         values['price_unit'] = self.env['account.tax']._fix_tax_included_price(product.lst_price, taxes,
    #                                                                                fp_taxes.ids)
    #
    #     values['uos_id'] = product.uom_id.id
    #     if uom_id:
    #         uom = self.env['product.uom'].browse(uom_id)
    #         if product.uom_id.category_id.id == uom.category_id.id:
    #             values['uos_id'] = uom_id
    #
    #     domain = {'uos_id': [('category_id', '=', product.uom_id.category_id.id)]}
    #
    #     company = self.env['res.company'].browse(company_id)
    #     currency = self.env['res.currency'].browse(currency_id)
    #     accounts = self.env['account.account'].browse(account.id)
    #
    #     if company and currency:
    #         if company.currency_id != currency:
    #             values['price_unit'] = values['price_unit'] * currency.rate
    #
    #         if values['uos_id'] and values['uos_id'] != product.uom_id.id:
    #             values['price_unit'] = self.env['product.uom']._compute_price(
    #                 product.uom_id.id, values['price_unit'], values['uos_id'])
    #     # if warehouse_id and accounts.analytic_line:
    #     mapping_analytic = {}
    #     for al in accounts.analytic_line:
    #         mapping_analytic[al.name.id] = al.analytic_id.id
    #     # if mapping_analytic and mapping_analytic[warehouse_id]:
    #     if mapping_analytic.has_key(warehouse_id):
    #         values['analytic_account_id'] = mapping_analytic[warehouse_id]
    #
    #     return {'value': values, 'domain': domain}

    @api.multi
    def onchange_account_id(self, product_id, partner_id, inv_type, fposition_id, account_id):
        if not account_id:
            return {}
        unique_tax_ids = []
        account = self.env['account.account'].browse(account_id)
        warehouse_id = self.env['res.users'].browse(self._uid).warehouse_id.id or False
        if not product_id:
            fpos = self.env['account.fiscal.position'].browse(fposition_id)
            unique_tax_ids = fpos.map_tax(account.tax_ids).ids
        else:
            product_change_result = self.product_id_change(product_id, False, type=inv_type,
                                                           partner_id=partner_id, fposition_id=fposition_id,
                                                           company_id=account.company_id.id)
            if 'invoice_line_tax_id' in product_change_result.get('value', {}):
                unique_tax_ids = product_change_result['value']['invoice_line_tax_id']

        account_analytic_id = False
        mapping_analytic = {}
        for al in account.analytic_line:
            mapping_analytic[al.name.id] = al.analytic_id.id
        if mapping_analytic.has_key(warehouse_id):
            account_analytic_id = mapping_analytic[warehouse_id]
        return {'value': {'invoice_line_tax_id': unique_tax_ids, 'account_analytic_id': account_analytic_id}}



class AccountInvoiceTax(models.Model):
    _inherit = "account.invoice.tax"

    @api.v8
    def compute(self, invoice):
        tax_grouped = {}
        tax_obj = self.env['account.tax']
        currency = invoice.currency_id.with_context(date=invoice.date_invoice or fields.Date.context_today(invoice))
        company_currency = invoice.company_id.currency_id
        # line_discount = 0.0
        for line in invoice.invoice_line:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0) #product_qty = line.quantity
            if line.discount_method == "fixed": #(line.quantity * line.price_unit) - line.discount
                price_unit = line.price_unit - line.discount  #product_qty = 1.0
            # if line.name == 'Discount' and (line.price_subtotal < 0.0 or line.price_unit < 0.0):
            #     line_discount += (line.price_subtotal * -1)
            taxes = line.invoice_line_tax_id.compute_all(price_unit, line.quantity,
                                                         line.product_id, invoice.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'invoice_id': invoice.id,
                    'name': tax['name'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'base': currency.round(tax['price_unit'] * line['quantity']),
                }
                if invoice.type in ('out_invoice', 'in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['base_sign'], company_currency, round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['tax_sign'], company_currency, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = currency.compute(val['base'] * tax['ref_base_sign'], company_currency,
                                                          round=False)
                    val['tax_amount'] = currency.compute(val['amount'] * tax['ref_tax_sign'], company_currency,
                                                         round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                if not val.get('account_analytic_id') and line.account_analytic_id and val[
                    'account_id'] == line.account_id.id:
                    val['account_analytic_id'] = line.account_analytic_id.id

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            # tax_id = tax_obj.search([('base_code_id', '=', t['base_code_id']),
            #                 ('tax_code_id', '=', t['tax_code_id'])])
            # tx = tax_obj.browse(tax_id and tax_id[0])
            # if line_discount and line_discount>0.0:
            #     t['base'] += line_discount
            #     t['base_amount'] += line_discount
            #     t['amount'] = t['base'] * 0.1
            #     t['tax_amount'] = t['base_amount'] * 0.1
            t['base'] = currency.round(t['base'])
            t['amount'] = currency.round(t['amount'])
            t['base_amount'] = currency.round(t['base_amount'])
            t['tax_amount'] = currency.round(t['tax_amount'])

        return tax_grouped



class account_move(osv.osv):
    _inherit = "account.move"

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        company_id = usr_obj._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)], context=context)
        user = usr_obj.browse(cr, uid, uid, context=context)
        warehouse_id = False
        if user.warehouse_id:
            warehouse_id = user.warehouse_id.id
        elif warehouse_ids:
            warehouse_id = warehouse_ids[0]
        return warehouse_id

    _columns = {
        'warehouse_id': Fields.many2one('stock.warehouse', 'Outlet [WH]'),
    }

    _defaults = {
        'warehouse_id': _get_warehouse,
    }



class account_move(osv.osv):
    _inherit = "account.move.line"
    _columns = {
        'warehouse_id': Fields.many2one('stock.warehouse', 'Outlet [WH]'),
        # 'warehouse_id': Fields.related('move_id', 'warehouse_id', type='many2one',
        #         relation='stock.warehouse', string='Outlet [WH]', store=True),
    }



