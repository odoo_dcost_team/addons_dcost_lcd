{
    "name":"Analytic Account DCOST",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://myabcerp.com",
    "category":"Custom Modules",
    "description": """
        The base module to generate excel report.
    """,
    "depends":["base"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":[
            "AmbonC.xml",
            "Cpulir.xml",
            "JambiC.xml",
            "JSenen.xml",
            "Cpulir.xml",
            "JambiC.xml",
            "JSenen.xml",
            "Jtngor.xml",
            "Kemang.xml",
            "KptLCD.xml",
            "Kupang.xml",
            "LombokC.xml",
            "Pnrogo.xml",
            "Purind.xml",
            "SlgLCD.xml",
            "TanpiC.xml",
            "Tnbang.xml",
    ],
    
    "active":False,
    "installable":True
}
