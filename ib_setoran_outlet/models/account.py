#-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
from openerp.osv import fields, osv, expression
import logging

_logger = logging.getLogger(__name__)

class account_journal(osv.osv):
    _inherit = "account.journal"

    _columns = {
        'related_journal_id': fields.many2one('account.journal', 'Related Journal'),
    }

