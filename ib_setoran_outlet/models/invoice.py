##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, expression
from openerp.tools.translate import _
import logging
from openerp import api

_logger = logging.getLogger(__name__)


class account_invoice(osv.osv):
    _inherit = "account.invoice"

    def _deposit_residual(self, cr, uid, ids, name, args, context=None):
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = self.check_deposit_residual(cr, uid, [inv.id])
        return res

    def _deposit_outlet(self, cr, uid, ids, name, args, context=None):
        res = {}
        for inv in self.browse(cr, uid, ids, context=context):
            res[inv.id] = self.check_balance_setoran(cr, uid, [inv.id])
        return res

    def _get_setoran_list(self, cr, uid, ids, context=None):
        result = {}
        for setoran in self.pool.get('setoran.outlet').browse(cr, uid, ids, context=context):
            result[setoran.invoice_id.id] = True
        return result.keys()

    def _get_invoice_from_line(self, cr, uid, ids, context=None):
        move = {}
        for line in self.pool.get('account.move.line').browse(cr, uid, ids, context=context):
            if line.reconcile_partial_id:
                for line2 in line.reconcile_partial_id.line_partial_ids:
                    move[line2.move_id.id] = True
            if line.reconcile_id:
                for line2 in line.reconcile_id.line_id:
                    move[line2.move_id.id] = True
        invoice_ids = []
        if move:
            invoice_ids = self.pool.get('account.invoice').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return invoice_ids

    def _get_invoice_from_reconcile(self, cr, uid, ids, context=None):
        move = {}
        for r in self.pool.get('account.move.reconcile').browse(cr, uid, ids, context=context):
            for line in r.line_partial_ids:
                move[line.move_id.id] = True
            for line in r.line_id:
                move[line.move_id.id] = True

        invoice_ids = []
        if move:
            invoice_ids = self.pool.get('account.invoice').search(cr, uid, [('move_id','in',move.keys())], context=context)
        return invoice_ids

    _columns = {
        'deposit_residual': fields.function(_deposit_residual, digits_compute=dp.get_precision('Account'),
                type='float', string='Balance',
                store={
                    'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['move_id', 'payment_ids'], 50),
                    'setoran.outlet': (_get_setoran_list, ['amount', 'invoice_id'], 50),
                    'account.move.line': (_get_invoice_from_line, None, 50),
                    'account.move.reconcile': (_get_invoice_from_reconcile, None, 50),
                }), #store=True
        'deposit_outlet': fields.function(_deposit_outlet, string='Completed', type='boolean',
                store={
                    'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['move_id', 'payment_ids'], 50),
                    'setoran.outlet': (_get_setoran_list, ['amount', 'invoice_id'], 50),
                    'account.move.line': (_get_invoice_from_line, None, 50),
                    'account.move.reconcile': (_get_invoice_from_reconcile, None, 50),
                },
                help="Tanda(check list) ini berarti Setoran Penjualan dari Outlet ke Kantor Pusat atas sales invoice ini sudah disetorkan semua."),
        'setoran_ids': fields.many2many('setoran.outlet', 'account_invoice_setoran_outlet_rel',
                'invoice_id', 'setoran_id', 'Setoran Outlet'),
    }

    def button_calculate_balance(self, cr, uid, ids, context=None):
        return True

    def check_balance_setoran(self, cr, uid, ids, *args):
        payment_amount = self.get_payment_amount(cr, uid, ids)
        deposit_amount = self.get_deposit_amount(cr, uid, ids)
        return deposit_amount == payment_amount

    def check_deposit_residual(self, cr, uid, ids, *args):
        payment_amount = self.get_payment_amount(cr, uid, ids)
        deposit_amount = self.get_deposit_amount(cr, uid, ids)
        return payment_amount - deposit_amount

    def get_deposit_amount(self, cr, uid, ids, *args):
        cr.execute("SELECT SUM(amount+amount_provisi) AS amount FROM setoran_outlet WHERE invoice_id IN %s AND state!=%s",
                   (tuple(ids), 'cancel'))
        return cr.fetchone()[0] or 0.0

    def get_payment_amount(self, cr, uid, ids, *args):
        res = []
        invoices = self.read(cr, uid, ids, ['payment_ids'])
        for i in invoices:
            if i['payment_ids']:
                for pay_move_line in self.pool.get('account.move.line').browse(cr, uid, i['payment_ids']):
                    res.append(pay_move_line.id)
        if not res:
            return False
        cr.execute('''SELECT SUM(l.credit) AS amount FROM account_move_line AS l 
                LEFT JOIN account_journal AS j ON l.journal_id=j.id
                WHERE l.reconcile_id IS NOT NULL AND l.id IN %s  AND 
                    (j.code IN ('BNK1','CARD','DELV') OR j.name IN ('CASH','CARD','DELIVERY'))''', (tuple(res),))
        return cr.fetchone()[0] or 0.0

    def setoran_outlet_ke_pusat(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ib_setoran_outlet', 'wizard_setoran_outlet_view')

        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name':_("Daily Cash Deposit [Setoran Tunai Harian Outlet]"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'wizard.setoran.outlet',  #'setoran.outlet',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'default_currency_id': inv.currency_id and inv.currency_id.id,
                'default_warehouse_id': inv.warehouse_id and inv.warehouse_id.id,
                'default_amount': inv.type=='out_invoice' and (not inv.deposit_outlet) and (inv.deposit_residual*-1 if inv.deposit_residual<0.0 else inv.deposit_residual) or 0.0,
                # 'default_name': inv.name,
                'invoice_type': inv.type,
                'invoice_id': inv.id
            }
        }

    @api.multi
    def move_line_id_payment_get(self):
        if not self.id:
            return []
        query = """ SELECT l.id FROM account_move_line l, account_invoice i
                WHERE i.id = %s AND l.move_id = i.move_id AND l.account_id = i.account_id """
        self._cr.execute(query, (self.id,))
        return [row[0] for row in self._cr.fetchall()]


