import ast
import time
from openerp import netsvc
import urllib2
from itertools import groupby
from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, orm
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp import tools, SUPERUSER_ID


class stock_inventory_line(osv.osv):
    _inherit = "stock.inventory.line"
    _columns = {
                'inventory_value': fields.float('Harga', digits_compute=dp.get_precision('Product Price')),
    }   

class pos_config(osv.osv):
    _inherit = 'pos.config'
    _columns = {
                'kode_cabang' : fields.char('Kode Cabang', size=128),
    }

class account_cashbox_line(osv.osv):
    _inherit = "account.cashbox.line"
    _columns = {
        'number_opening' : fields.float('Number of Units', help='Opening Unit Numbers'),
        'number_closing' : fields.float('Number of Units', help='Closing Unit Numbers')
    }
    

class product_product(osv.osv):
    _inherit = "product.product"
    _columns = {
        'tipe': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Type'),
        'pos_old_id' : fields.integer('ID POS Lama', size=7),
    }
    
    # _defaults = {
    #     'tipe': 'log'
    # }
    

class pos_order_line(osv.osv):
    _inherit = "pos.order.line"
    _defaults = {
        'name': '/',
    }
         
    def create(self, cr, uid, vals, context=None):
        vals['name'] = self.pool.get('pos.order').browse(cr, uid, vals['order_id']).name
        return super(pos_order_line, self).create(cr, uid, vals, context=context)


class import_pos(osv.osv_memory):
    _name = "import.pos"
            
    def cron_pos(self, cr, uid, ids=False, context=None):        
        obj_mrp = self.pool.get('mrp.production')
        obj_config = self.pool.get('pos.config')
        obj_bom = self.pool.get('mrp.bom')
        obj_product = self.pool.get('product.product')
        obj_uom = self.pool.get('product.uom')
        obj_loc = self.pool.get('stock.location')
        obj_wh = self.pool.get('stock.warehouse')
        obj_picking = self.pool.get('stock.picking')
        obj_move = self.pool.get('stock.move')
        obj_entri = self.pool.get('account.move')
        obj_entri_line = self.pool.get('account.move.line')
 
        d = date.today()- timedelta(int(ids))
        dte = d.strftime("%Y-%m-%d")
        tgl = d.strftime("%d-%m-%Y")
        
        jurnal = {1:1, 8:9, 9:17, 5:25, 3:33, 6:41, 4:49, 7:57, 12:1271} 
        piutang = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
        dcc = {1:2731, 8:601, 9:896, 5:1191, 3:1489, 6:1784, 4:2079, 7:2374, 12:4571}
        compliment = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691}
        voucher = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
        prepaid = {1:2824, 8:2835, 9:2836, 5:2840, 3:2828, 6:2842, 4:2844, 7:2830, 12:4574} 
        discount = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
        ongkir = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
        lain = {1:577, 8:873, 9:1168, 5:1463, 3:1761, 6:2056, 4:2351, 7:2646, 12:4853} 

        pendapatan = {1:415, 8:711, 9:1006, 5:1301, 3:1599, 6:1894, 4:2189, 7:2484, 12:4690} 
        pajak = {1:375, 8:671, 9:966, 5:1261, 3:1559, 6:1854, 4:2149, 7:2444, 12:4650} 
        
        print 'Cron POS ======================>', date.today(), tgl
          
        exis = obj_picking.search(cr, uid, [('date', '=', dte), ('owner_id', '=', 3), ('origin', '=', 'Menu Tarikan POS')])
        if exis :
            raise osv.except_osv(('Perhatian !'), ("Data penjualan tanggal %s sudah pernah diimport ..." % tgl))
              
        url = "http://203.153.218.222/OPENERP/GetRangkingMenu.asp?TanggalA=%s&TanggalB=%s&KodeCabang=ALL" % (tgl, tgl)
        url_bayar = "http://203.153.218.222/OPENERP/GetSummaryHarianGabungan.asp?TanggalA=%s&TanggalB=%s&KodeCabang=ALL" % (tgl, tgl)
           
        try:
            usock = urllib2.urlopen(url)
            usock_bayar = urllib2.urlopen(url_bayar)
            webpage = usock.read()
            webpage_bayar = usock_bayar.read()
            usock.close()
            usock_bayar.close()
            soup = BeautifulSoup(webpage)
            soup_bayar = BeautifulSoup(webpage_bayar)
        except  Exception, e :
            raise osv.except_osv(('Perhatian !'), ("Koneksi API Gagal ..."))
                
        web = ast.literal_eval(soup.text)['DATA RANGKING MENU']
        web_bayar = ast.literal_eval(soup_bayar.text)['DATA SUMMARY HARIAN GABUNGAN']
         
        data = {}; cabang = {}
        for key, group in groupby(web, lambda item: item["KodeCabang"]):
            cid = obj_config.search(cr, uid, [('kode_cabang', '=', key)])     
            if cid:
                cabang[key] = cid[0]
                data[key] = [item for item in group]
         
        data_bayar = {}; cabang_bayar = {}
        for keyb, groupb in groupby(web_bayar, lambda item: item["KodeCabang"]):
            cid = obj_config.search(cr, uid, [('kode_cabang', '=', keyb)])     
            if cid:
                cabang_bayar[keyb] = cid[0]
                data_bayar[keyb] = [item for item in groupb]
  
        for x in cabang.keys():
            print 'CABANG', x, datetime.now()
             
            config = obj_config.browse(cr, uid, cabang[x])
            wh = obj_loc.get_warehouse(cr, uid, config.stock_location_id, context=context)       
            gudang = obj_wh.browse(cr, uid, wh)
            user = self.pool.get('res.users').search(cr, uid, [('warehouse_id', '=', wh)])[0]
              
            salid = obj_picking.create(cr,uid, {
                                                        'origin': 'Menu Tarikan POS',
                                                        'picking_type_id': gudang.out_type_id.id,
                                                        'partner_id': gudang.partner_id.id,
                                                        'date': dte,
                                                        'owner_id': 3,
                                                        'move_type': 'one',
                                                        'company_id': gudang.company_id.id,
                                                    })
             
             
            total = 0
            nama = x + ' POS ' +  tgl
            period = self.pool.get('account.period').find(cr, uid, context={})
            movid = obj_entri.create(cr,uid, {'ref' : nama, 'period_id': period[0], 'journal_id': jurnal[gudang.company_id.id], 'company_id': gudang.company_id.id, 'partner_id': gudang.partner_id.id})
                  
                                      
            for i in data[x]:
                pad = obj_product.search(cr, uid, [('pos_old_id', '=', i['IDmenu'])])
                if pad :
                    if i['Jumlah']:
                        pid = pad[0]
                        total += i['Jumlah'] * i['Harga'] 
                        prod = obj_product.browse(cr, uid, pid)
                            
                        obj_move.create(cr, uid, {
                                                        'picking_id': salid,
                                                        'name': prod.partner_ref,
                                                        'date': dte,
                                                        'product_id': pid,
                                                        'product_uom': prod.uom_id.id,
                                                        'location_id': gudang.lot_stock_id.id,
                                                        'location_dest_id': 9,
                                                        'product_uom_qty': i['Jumlah'],
                                                        'origin': 'Menu Tarikan POS',
                                                        'company_id': gudang.company_id.id
                        })
    
                        if prod.bom_ids:
                            obj_mrp.create(cr, uid, {
                                                     'product_id': pid,
                                                     'date_planned': dte,
                                                     'user_id': user,
                                                     'origin': nama, 
                                                     'product_qty': i['Jumlah'],
                                                     'product_uom': prod.uom_id.id,
                                                     'bom_id': prod.bom_ids[0].id,
                                                     'location_src_id': gudang.lot_stock_id.id,
                                                     'location_dest_id': gudang.lot_stock_id.id,
                                                     'company_id': gudang.company_id.id
                            })

                            
                            
                            
                            
            if data_bayar.has_key(x):
                try:
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Pendapatan Penjualan',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': pendapatan[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': total})
                       
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Hutang PB 1',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': pajak[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': total*0.1})
                       
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Penerimaan Tunai',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': piutang[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCash'],
                                            'credit': 0})
           
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Compliment',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': compliment[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCompliment'],
                                            'credit': 0})  
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Prepaid',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': prepaid[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalPrepaid'],
                                            'credit': 0})  
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Voucher',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': voucher[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalVoucher'],
                                            'credit': 0})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Discount',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': discount[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalDiscount'],
                                            'credit': 0})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Ongkos Kirim',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': ongkir[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalOngkir'],
                                            'credit': 0})
     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Pendapatan Lain-lain',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': lain[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': data_bayar[x][0]['TotalOngkir']})
       
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Penerimaan Kartu Credit',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': dcc[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCard'],
                                            'credit': 0})
 
 
                except Exception, e :
                    print e
                    
                    

class tarik_penjualan(osv.osv_memory):
    _name = "tarik.penjualan"
    _columns = {
                'name': fields.char('File Name', 16),
                'date_start': fields.date('Date', required=True),
                'pos_id': fields.many2one('pos.config', 'Outlet', required=True),
    }    
    
    _defaults = {
                 'date_start': lambda *a: time.strftime('%Y-%m-%d'),
    }

            
    def tarik_penjualan(self, cr, uid, ids, context=None):                
        val = self.browse(cr, uid, ids)[0]
 
        obj_mrp = self.pool.get('mrp.production')
        obj_config = self.pool.get('pos.config')
        obj_bom = self.pool.get('mrp.bom')
        obj_product = self.pool.get('product.product')
        obj_uom = self.pool.get('product.uom')
        obj_loc = self.pool.get('stock.location')
        obj_wh = self.pool.get('stock.warehouse')
        obj_picking = self.pool.get('stock.picking')
        obj_move = self.pool.get('stock.move')
        obj_entri = self.pool.get('account.move')
        obj_entri_line = self.pool.get('account.move.line')
  
        dte = val.date_start
        tgla = dte.split('-')
        tgl = tgla[2]+'-'+tgla[1]+'-'+tgla[0]
         
        jurnal = {1:1, 8:9, 9:17, 5:25, 3:33, 6:41, 4:49, 7:57, 12:1271} 
        piutang = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
        dcc = {1:2731, 8:601, 9:896, 5:1191, 3:1489, 6:1784, 4:2079, 7:2374, 12:4571}
        compliment = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691}
        voucher = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
        prepaid = {1:2824, 8:2835, 9:2836, 5:2840, 3:2828, 6:2842, 4:2844, 7:2830, 12:4574} 
        discount = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
        ongkir = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
        lain = {1:577, 8:873, 9:1168, 5:1463, 3:1761, 6:2056, 4:2351, 7:2646, 12:4853} 
 
        pendapatan = {1:415, 8:711, 9:1006, 5:1301, 3:1599, 6:1894, 4:2189, 7:2484, 12:4690} 
        pajak = {1:375, 8:671, 9:966, 5:1261, 3:1559, 6:1854, 4:2149, 7:2444, 12:4650} 
           
        print 'Import POS ======================>', tgl
           
        exis = obj_picking.search(cr, uid, [('date', '=', dte), ('origin', '=', 'Menu Tarikan POS'), ('partner_id', '=', val.pos_id.picking_type_id.warehouse_id.partner_id.id)])
        if exis :
            raise osv.except_osv(('Perhatian !'), ("Data penjualan tanggal %s sudah pernah diimport ..." % tgl))
               
        url = "http://203.153.218.222/OPENERP/GetRangkingMenu.asp?TanggalA=%s&TanggalB=%s&KodeCabang=%s" % (tgl, tgl, val.pos_id.kode_cabang)
        url_bayar = "http://203.153.218.222/OPENERP/GetSummaryHarianGabungan.asp?TanggalA=%s&TanggalB=%s&KodeCabang=%s" % (tgl, tgl, val.pos_id.kode_cabang)
            
        try:
            usock = urllib2.urlopen(url)
            usock_bayar = urllib2.urlopen(url_bayar)
            webpage = usock.read()
            webpage_bayar = usock_bayar.read()
            usock.close()
            usock_bayar.close()
            soup = BeautifulSoup(webpage)
            soup_bayar = BeautifulSoup(webpage_bayar)
        except  Exception, e :
            raise osv.except_osv(('Perhatian !'), ("Koneksi API Gagal ..."))
                 
        web = ast.literal_eval(soup.text)['DATA RANGKING MENU']
        web_bayar = ast.literal_eval(soup_bayar.text)['DATA SUMMARY HARIAN GABUNGAN']
          
        data = {}; cabang = {}
        for key, group in groupby(web, lambda item: item["KodeCabang"]):
            cid = obj_config.search(cr, uid, [('kode_cabang', '=', key)])     
            if cid:
                cabang[key] = cid[0]
                data[key] = [item for item in group]
          
        data_bayar = {}; cabang_bayar = {}
        for keyb, groupb in groupby(web_bayar, lambda item: item["KodeCabang"]):
            cid = obj_config.search(cr, uid, [('kode_cabang', '=', keyb)])     
            if cid:
                cabang_bayar[keyb] = cid[0]
                data_bayar[keyb] = [item for item in groupb]
       
        for x in cabang.keys():
            print 'CABANG', x, datetime.now()
              
            config = obj_config.browse(cr, uid, cabang[x])
            wh = obj_loc.get_warehouse(cr, uid, config.stock_location_id, context=context)       
            gudang = obj_wh.browse(cr, uid, wh)
            user = self.pool.get('res.users').search(cr, uid, [('warehouse_id', '=', wh)])[0]
                
            salid = obj_picking.create(cr,uid, {
                                                        'origin': 'Menu Tarikan POS',
                                                        'picking_type_id': gudang.out_type_id.id,
                                                        'partner_id': gudang.partner_id.id,
                                                        'date': dte,
                                                        'owner_id': 3,
                                                        'move_type': 'one',
                                                        'company_id': gudang.company_id.id,
                                                    })
               
               
            total = 0; baid = {}
            nama = x + ' POS ' +  tgl
            period = self.pool.get('account.period').find(cr, uid, context={})
            movid = obj_entri.create(cr,uid, {'ref' : nama, 'period_id': period[0], 'journal_id': jurnal[gudang.company_id.id], 'company_id': gudang.company_id.id, 'partner_id': gudang.partner_id.id})
                         
            for i in data[x]:
                pad = obj_product.search(cr, uid, [('pos_old_id', '=', i['IDmenu'])])
                if pad :
                    if i['Jumlah']:
                        pid = pad[0]
                        total += i['Jumlah'] * i['Harga'] 
                        prod = obj_product.browse(cr, uid, pid)
                             
                        obj_move.create(cr, uid, {
                                                        'picking_id': salid,
                                                        'name': prod.partner_ref,
                                                        'date': dte,
                                                        'product_id': pid,
                                                        'product_uom': prod.uom_id.id,
                                                        'location_id': gudang.lot_stock_id.id,
                                                        'location_dest_id': 9,
                                                        'product_uom_qty': i['Jumlah'],
                                                        'origin': 'Menu Tarikan POS',
                                                        'company_id': gudang.company_id.id
                        })
    
                        if prod.bom_ids:
                            obj_mrp.create(cr, uid, {
                                                     'product_id': pid,
                                                     'date_planned': dte,
                                                     'user_id': user,
                                                     'origin': nama, 
                                                     'product_qty': i['Jumlah'],
                                                     'product_uom': prod.uom_id.id,
                                                     'bom_id': prod.bom_ids[0].id,
                                                     'location_src_id': gudang.lot_stock_id.id,
                                                     'location_dest_id': gudang.lot_stock_id.id,
                                                     'company_id': gudang.company_id.id
                            })

             
            if data_bayar.has_key(x):                
                try:
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Pendapatan Penjualan',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': pendapatan[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': total})
                       
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Hutang PB 1',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': pajak[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': total*0.1})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Penerimaan Tunai',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': piutang[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCash'],
                                            'credit': 0})
           
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Compliment',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': compliment[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCompliment'],
                                            'credit': 0})  
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Prepaid',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': prepaid[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalPrepaid'],
                                            'credit': 0})  
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Voucher',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': voucher[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalVoucher'],
                                            'credit': 0})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Discount',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': discount[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalDiscount'],
                                            'credit': 0})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Ongkos Kirim',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': ongkir[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalOngkir'],
                                            'credit': 0})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Pendapatan Lain-lain',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': lain[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': 0,
                                            'credit': data_bayar[x][0]['TotalOngkir']})
                     
                    obj_entri_line.create(cr,uid, {
                                            'name': 'Penerimaan Kartu Credit',
                                            'partner_id': gudang.partner_id.id,
                                            'account_id': dcc[gudang.company_id.id],
                                            'date_maturity': dte,
                                            'move_id': movid,
                                            'debit': data_bayar[x][0]['TotalCard'],
                                            'credit': 0})
 
                except Exception, e :
                    pass
                                
        return {}
            

class mrp_bom_line(osv.osv):
    _inherit = 'mrp.bom.line'
    _columns = {
        'product_rounding': fields.float('Product Rounding', digits_compute=dp.get_precision('Product Unit of Measure'), help="Rounding applied on the product quantity."),   
    }



class mrp_product_produce_line(osv.osv_memory):
    _inherit = "mrp.product.produce.line"
    _columns = {
        'product_qty': fields.float('Quantity (in default UoM)', digits_compute=dp.get_precision('Product Unit of Measure')),
    }


class stock_move(osv.osv):
    _inherit = 'stock.move'
    _columns = {
        'stock_line_id': fields.many2one('stock.order.line', 'Stock Order Line'),
    }

    def _store_average_cost_price(self, cr, uid, move, context=None):
        product_obj = self.pool.get('product.product')
        move.refresh()
        if any([q.qty <= 0 for q in move.quant_ids]):
            return
        average_valuation_price = 0.0
        for q in move.quant_ids:
            average_valuation_price += q.qty * q.cost
        
        if move.product_qty == 0.0:
            average_valuation_price = 0.0
        else :
            average_valuation_price = average_valuation_price / move.product_qty
        product_obj.write(cr, SUPERUSER_ID, [move.product_id.id], {'standard_price': average_valuation_price}, context=context)
        self.write(cr, uid, [move.id], {'price_unit': average_valuation_price}, context=context)


    def action_consume(self, cr, uid, ids, product_qty, location_id=False, restrict_lot_id=False, restrict_partner_id=False, consumed_for=False, context=None):
        if context is None:
            context = {}
        res = []
        production_obj = self.pool.get('mrp.production')
        uom_obj = self.pool.get('product.uom')

        if product_qty <= 0:
            product_qty = 0.1 #raise osv.except_osv(_('Warning!'), _('Please provide proper quantity.'))
        ids2 = []
        for move in self.browse(cr, uid, ids, context=context):
            if move.state == 'draft':
                ids2.extend(self.action_confirm(cr, uid, [move.id], context=context))
            else:
                ids2.append(move.id)

        for move in self.browse(cr, uid, ids2, context=context):
            move_qty = move.product_qty
            uom_qty = uom_obj._compute_qty(cr, uid, move.product_id.uom_id.id, product_qty, move.product_uom.id)
            if move_qty <= 0:
                move_qty = 0.1 #raise osv.except_osv(_('Error!'), _('Cannot consume a move with negative or zero quantity.'))
            quantity_rest = move.product_qty - uom_qty
            if quantity_rest > 0:
                ctx = context.copy()
                if location_id:
                    ctx['source_location_id'] = location_id
                new_mov = self.split(cr, uid, move, move_qty - quantity_rest, restrict_lot_id=restrict_lot_id, restrict_partner_id=restrict_partner_id, context=ctx)
                self.write(cr, uid, new_mov, {'consumed_for': consumed_for}, context=context)
                res.append(new_mov)
            else:
                res.append(move.id)
                if location_id:
                    self.write(cr, uid, [move.id], {'location_id': location_id, 'restrict_lot_id': restrict_lot_id, 'restrict_partner_id': restrict_partner_id, 'consumed_for': consumed_for}, context=context)
            self.action_done(cr, uid, res, context=context)
            production_ids = production_obj.search(cr, uid, [('move_lines', 'in', [move.id])])
            production_obj.signal_workflow(cr, uid, production_ids, 'button_produce')
            for new_move in res:
                if new_move != move.id:
                    production_obj.write(cr, uid, production_ids, {'move_lines': [(4, new_move)]})
        return res




class res_users(osv.osv):
    _inherit = "res.users"
    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
        'kelompok_ids': fields.one2many('kelompok.user', 'res_user_id', 'Group User'),
    }
        

class kelompok_user(osv.osv):
    _name = "kelompok.user"
    _columns = {
        'res_user_id': fields.many2one('res.users', 'RES USERS', required=True, ondelete='cascade'),
        'name': fields.many2one('res.users', 'User'),
    }
    


class accountaccount(osv.osv):
    _inherit = "account.account"

    def _check_allow_code_change(self, cr, uid, ids, context=None):
        line_obj = self.pool.get('account.move.line')
        for account in self.browse(cr, uid, ids, context=context):
            account_ids = self.search(cr, uid, [('id', 'child_of', [account.id])], context=context)
            if line_obj.search(cr, uid, [('account_id', 'in', account_ids)], context=context):
                pass #raise osv.except_osv(_('Warning !'), _("You cannot change the code of account which contains journal items!"))
        return True




#                 print  '###########################################', total, data_bayar.has_key(x)
#                 print data_bayar
#                 print 'pendapatan', pendapatan[gudang.company_id.id]
#                 print 'pajak', pajak[gudang.company_id.id]
#                 print 'piutang', piutang[gudang.company_id.id], data_bayar[x][0]['TotalCash']
#                 print 'compliment', compliment[gudang.company_id.id], data_bayar[x][0]['TotalCompliment']
#                 print 'prepaid', prepaid[gudang.company_id.id], data_bayar[x][0]['TotalPrepaid']
#                 print 'voucher', voucher[gudang.company_id.id], data_bayar[x][0]['TotalVoucher']
#                 print 'discount', discount[gudang.company_id.id], data_bayar[x][0]['TotalDiscount']
#                 print 'ongkir', ongkir[gudang.company_id.id], data_bayar[x][0]['TotalOngkir']
#                 print 'lain', ongkir[gudang.company_id.id], data_bayar[x][0]['TotalOngkir']
#                 print 'card', dcc[gudang.company_id.id], data_bayar[x][0]['TotalCard']

            
#     def tarik_penjualan(self, cr, uid, ids, context=None):                
#         val = self.browse(cr, uid, ids)[0]
# 
#         obj_config = self.pool.get('pos.config')
#         obj_bom = self.pool.get('mrp.bom')
#         obj_product = self.pool.get('product.product')
#         obj_uom = self.pool.get('product.uom')
#         obj_loc = self.pool.get('stock.location')
#         obj_wh = self.pool.get('stock.warehouse')
#         obj_picking = self.pool.get('stock.picking')
#         obj_move = self.pool.get('stock.move')
#         obj_entri = self.pool.get('account.move')
#         obj_entri_line = self.pool.get('account.move.line')
#  
#         dte = val.date_start
#         tgla = dte.split('-')
#         tgl = tgla[2]+'-'+tgla[1]+'-'+tgla[0]
#         
#         jurnal = {1:1, 8:9, 9:17, 5:25, 3:33, 6:41, 4:49, 7:57, 12:1271} 
#         piutang = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
#         dcc = {1:2731, 8:601, 9:896, 5:1191, 3:1489, 6:1784, 4:2079, 7:2374, 12:4571}
#         compliment = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691}
#         voucher = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
#         prepaid = {1:2824, 8:2835, 9:2836, 5:2840, 3:2828, 6:2842, 4:2844, 7:2830, 12:4574} 
#         discount = {1:416, 8:712, 9:1007, 5:1302, 3:1600, 6:1895, 4:2190, 7:2485, 12:4691} 
#         ongkir = {1:304, 8:602, 9:897, 5:1192, 3:1490, 6:1785, 4:2080, 7:2375, 12:4576} 
#         lain = {1:577, 8:873, 9:1168, 5:1463, 3:1761, 6:2056, 4:2351, 7:2646, 12:4853} 
# 
#         pendapatan = {1:415, 8:711, 9:1006, 5:1301, 3:1599, 6:1894, 4:2189, 7:2484, 12:4853} 
#         pajak = {1:375, 8:671, 9:966, 5:1261, 3:1559, 6:1854, 4:2149, 7:2444, 12:4650} 
#           
#         print 'Import POS ======================>', tgl
#           
#         exis = obj_picking.search(cr, uid, [('date', '=', dte), ('origin', '=', 'Menu Tarikan POS'), ('partner_id', '=', val.pos_id.picking_type_id.warehouse_id.partner_id.id)])
#         if exis :
#             raise osv.except_osv(('Perhatian !'), ("Data penjualan tanggal %s sudah pernah diimport ..." % tgl))
#               
#         url = "http://203.153.218.222/OPENERP/GetRangkingMenu.asp?TanggalA=%s&TanggalB=%s&KodeCabang=%s" % (tgl, tgl, val.pos_id.kode_cabang)
#         url_bayar = "http://203.153.218.222/OPENERP/GetSummaryHarianGabungan.asp?TanggalA=%s&TanggalB=%s&KodeCabang=%s" % (tgl, tgl, val.pos_id.kode_cabang)
#            
#         try:
#             usock = urllib2.urlopen(url)
#             usock_bayar = urllib2.urlopen(url_bayar)
#             webpage = usock.read()
#             webpage_bayar = usock_bayar.read()
#             usock.close()
#             usock_bayar.close()
#             soup = BeautifulSoup(webpage)
#             soup_bayar = BeautifulSoup(webpage_bayar)
#         except  Exception, e :
#             raise osv.except_osv(('Perhatian !'), ("Koneksi API Gagal ..."))
#                 
#         web = ast.literal_eval(soup.text)['DATA RANGKING MENU']
#         web_bayar = ast.literal_eval(soup_bayar.text)['DATA SUMMARY HARIAN GABUNGAN']
#          
#         data = {}; cabang = {}
#         for key, group in groupby(web, lambda item: item["KodeCabang"]):
#             cid = obj_config.search(cr, uid, [('kode_cabang', '=', key)])     
#             if cid:
#                 cabang[key] = cid[0]
#                 data[key] = [item for item in group]
#          
#         data_bayar = {}; cabang_bayar = {}
#         for keyb, groupb in groupby(web_bayar, lambda item: item["KodeCabang"]):
#             cid = obj_config.search(cr, uid, [('kode_cabang', '=', keyb)])     
#             if cid:
#                 cabang_bayar[keyb] = cid[0]
#                 data_bayar[keyb] = [item for item in groupb]
#       
#         for x in cabang.keys():
#             print 'CABANG', x, datetime.now()
#              
#             config = obj_config.browse(cr, uid, cabang[x])
#             wh = obj_loc.get_warehouse(cr, uid, config.stock_location_id, context=context)       
#             gudang = obj_wh.browse(cr, uid, wh)
#                
#             salid = obj_picking.create(cr,uid, {
#                                                         'origin': 'Menu Tarikan POS',
#                                                         'picking_type_id': gudang.out_type_id.id,
#                                                         'partner_id': gudang.partner_id.id,
#                                                         'date': dte,
#                                                         'owner_id': 3,
#                                                         'move_type': 'one',
#                                                         'company_id': gudang.company_id.id,
#                                                     })
#               
#               
#             total = 0; bahan = []; baid = {}
#             nama = x + ' POS ' +  tgl
#             period = self.pool.get('account.period').find(cr, uid, context={})
#             movid = obj_entri.create(cr,uid, {'ref' : nama, 'period_id': period[0], 'journal_id': jurnal[gudang.company_id.id], 'company_id': gudang.company_id.id, 'partner_id': gudang.partner_id.id})
#             
#             goods_id = obj_picking.create(cr,uid, {
#                                         'origin': 'BoM Tarikan POS',
#                                         'type_wo': 'fg',
#                                         'picking_type_id': gudang.int_type_id.id,
#                                         'move_type': 'one',
#                                         'date': dte,
#                                         'company_id': gudang.company_id.id,
#                                     })
#                                
#             material_id = obj_picking.create(cr,uid, {
#                                         'origin': 'BoM Tarikan POS',
#                                         'type_wo': 'rm',
#                                         'picking_type_id': gudang.int_type_id.id,
#                                         'move_type': 'one',
#                                         'date': dte,
#                                         'company_id': gudang.company_id.id,
#                                     })
#  
#              
#             for i in data[x]:
#                 pad = obj_product.search(cr, uid, [('pos_old_id', '=', i['IDmenu'])])
#                 if pad :
#                     if i['Jumlah']:
#                         pid = pad[0]
#                         total += i['Jumlah'] * i['Harga'] 
#                         prod = obj_product.browse(cr, uid, pid)
#  
#                         idg = obj_move.create(cr,uid, {
#                                                     'name': prod.partner_ref,
#                                                     'date': dte,
#                                                     'product_id': pid,
#                                                     'product_uom': prod.uom_id.id,
#                                                     'product_uom_qty': i['Jumlah'],
#                                                     'location_id': 7,
#                                                     'location_dest_id': gudang.lot_stock_id.id,
#                                                     'picking_id': goods_id,
#                                                     'origin': 'BoM Tarikan POS',
#                                                     'company_id': gudang.company_id.id})
#                             
#                         obj_move.create(cr, uid, {
#                                                         'picking_id': salid,
#                                                         'name': prod.partner_ref,
#                                                         'date': dte,
#                                                         'product_id': pid,
#                                                         'product_uom': prod.uom_id.id,
#                                                         'location_id': gudang.lot_stock_id.id,
#                                                         'location_dest_id': 9,
#                                                         'product_uom_qty': i['Jumlah'],
#                                                         'origin': 'Menu Tarikan POS',
#                                                         'company_id': gudang.company_id.id
#                         })
#    
#                         if prod.bom_ids:
#                             bom_point = obj_bom.browse(cr, uid, prod.bom_ids[0].id)
#                             factor = obj_uom._compute_qty(cr, uid, prod.uom_id.id, i['Jumlah'], bom_point.product_uom.id)
#                             material = obj_bom._bom_explode(cr, uid, bom_point, prod, factor/bom_point.product_qty, properties=[], routing_id=False, context={})[0]
#                                                  
#                             for m in material:
#                                 baid[m['product_id']] = []
#                                 bahan.append({
#                                               'name': m['name'],
#                                               'date': dte,
#                                               'product_id': m['product_id'],
#                                               'product_uom': m['product_uom'],
#                                               'product_uom_qty': m['product_qty'],
#                                               'location_id': gudang.lot_stock_id.id, 
#                                               'location_dest_id': 7,
#                                               'picking_id': material_id,
#                                               'origin': 'BoM Tarikan POS',
#                                               'company_id': gudang.company_id.id})
#              
#              
#             for l in bahan:
#                 baid[l['product_id']].append(l)
#                  
#             for b in baid:
#                 obj_move.create(cr,uid, {
#                                          'name': baid[b][0]['name'],
#                                          'date': dte,
#                                          'product_id': b,
#                                          'product_uom': baid[b][0]['product_uom'],
#                                          'product_uom_qty': sum(u['product_uom_qty'] for u in baid[b]),
#                                          'location_id': gudang.lot_stock_id.id,
#                                          'location_dest_id': 7,
#                                          'picking_id': baid[b][0]['picking_id'],
#                                          'origin': 'BoM Tarikan POS',
#                                          'company_id': gudang.company_id.id})
             













# class pos_session(osv.osv):
#     _inherit = "pos.session"
# 
#     def wkf_action_closing_control(self, cr, uid, ids, context=None):
#         for session in self.browse(cr, uid, ids, context=context):
#             for statement in session.statement_ids:
#                 self.pool.get('account.bank.statement').button_cancel(cr, uid, [statement.id])
#                 self.pool.get('account.bank.statement').unlink(cr, uid, [statement.id])
#         return True 


#                         bid = obj_bom.search(cr, uid, [('product_tmpl_id', '=', prod.product_tmpl_id.id)])
#                         if bid :
#                             hbom = obj_mrp.create(cr, uid, {
#                                                      'product_id': pid[0],
#                                                      'product_qty': i['Jumlah'],
#                                                      'product_uom': prod.uom_id.id,
#                                                      'bom_id': bid[0],
#                                                      'location_src_id': gudang,
#                                                      'location_dest_id': gudang,
#                             })
#                             mrp.append(hbom)
                                 
#             for m in obj_mrp.browse(cr, uid, mrp):
#                 try:
#                     print '>>>', m.name
#                     obj_mrp.signal_workflow(cr, uid, [m.id], 'button_confirm')
#                     obj_mrp.force_production(cr, uid, [m.id])
#                     obj_mrp.action_produce(cr, uid, m.id, m.product_qty, 'consume_produce', wiz=False, context=context)
#                 except  Exception, e :
#                     pass

#     def cron_pos(self, cr, uid, ids=False, context=None):        
#         obj_session = self.pool.get('pos.session')
#         obj_config = self.pool.get('pos.config')
#         obj_order = self.pool.get('pos.order')
#         obj_order_line = self.pool.get('pos.order.line')
#         obj_bom = self.pool.get('mrp.bom')
#         obj_product = self.pool.get('product.product')
#         obj_uom = self.pool.get('product.uom')
#         obj_loc = self.pool.get('stock.location')
#         obj_wh = self.pool.get('stock.warehouse')
#         obj_picking = self.pool.get('stock.picking')
#         obj_move = self.pool.get('stock.move')
#         
#         d = date.today()- timedelta(int(ids))
#         dte = d.strftime("%Y-%m-%d")
#         tgl = d.strftime("%d-%m-%Y") 
#         jurnal = {1:7, 8:15, 9:23, 5:31, 3:39, 6:47, 4:55, 7:63}
#         
#         print 'Cron POS ======================>', date.today(), tgl
#         
#         exis = obj_session.search(cr, uid, [('start_at', '=', dte)])
#         if exis :
#             raise osv.except_osv(('Perhatian !'), ("Data penjualan tanggal %s sudah pernah diimport ..." % tgl))
#             
#         url = "http://203.153.218.222/OPENERP/GetRangkingMenu.asp?TanggalA=%s&TanggalB=%s&KodeCabang=ALL" % (tgl, tgl)
#         url_bayar = "http://203.153.218.222/OPENERP/GetSummaryHarianGabungan.asp?TanggalA=%s&TanggalB=%s&KodeCabang=ALL" % (tgl, tgl)
#           
#         try:
#             usock = urllib2.urlopen(url)
#             usock_bayar = urllib2.urlopen(url_bayar)
#             webpage = usock.read()
#             webpage_bayar = usock_bayar.read()
#             usock.close()
#             usock_bayar.close()
#             soup = BeautifulSoup(webpage)
#             soup_bayar = BeautifulSoup(webpage_bayar)
#         except  Exception, e :
#             raise osv.except_osv(('Perhatian !'), ("Koneksi API Gagal ..."))
#                
#         web = ast.literal_eval(soup.text)['DATA RANGKING MENU']
#         web_bayar = ast.literal_eval(soup_bayar.text)['DATA SUMMARY HARIAN GABUNGAN']
#         
#         data = {}; cabang = {}
#         for key, group in groupby(web, lambda item: item["KodeCabang"]):
#             cid = obj_config.search(cr, uid, [('kode_cabang', '=', key)])     
#             if cid:
#                 cabang[key] = cid[0]
#                 data[key] = [item for item in group]
#         
#         data_bayar = {}; cabang_bayar = {}
#         for keyb, groupb in groupby(web_bayar, lambda item: item["KodeCabang"]):
#             cid = obj_config.search(cr, uid, [('kode_cabang', '=', keyb)])     
#             if cid:
#                 cabang_bayar[keyb] = cid[0]
#                 data_bayar[keyb] = [item for item in groupb]
#                 
#         print '####################>', type(web_bayar), web_bayar
#         print '====================>', type(data_bayar), data_bayar
#            
#         for x in cabang.keys():
#             try:
#                 print 'CABANG', x
#                 posid = obj_session.create(cr, uid, {
#                                                      'config_id': cabang[x],
#                                                      'start_at': dte,
#                                                      'stop_at': dte
#                                                      })  
#      
#                 obj_session.open_cb(cr, uid, posid)
#                 config = obj_config.browse(cr, uid, cabang[x])
#                 wh = obj_loc.get_warehouse(cr, uid, config.stock_location_id, context=context)       
#                 gudang = obj_wh.browse(cr, uid, wh)
#                 posa = obj_session.browse(cr, uid, posid)
#                 orid = obj_order.create(cr, uid, {'session_id': posid, 'partner_id': gudang.partner_id.id})
#                       
#                 for i in data[x]:
#                     pad = obj_product.search(cr, uid, [('pos_old_id', '=', i['IDmenu'])])
#                     if pad :
#                         if i['Jumlah']:
#                             pid = pad[0]
#                             prod = obj_product.browse(cr, uid, pid)
#                             goods_id = obj_picking.create(cr,uid, {
#                                                         'origin': posa.name,
#                                                         'type_wo': 'fg',
#                                                         'picking_type_id': gudang.int_type_id.id,
#                                                         'move_type': 'one',
#                                                         'date': dte,
#                                                         'company_id': gudang.company_id.id,
#                                                     })
#                               
#                             picking_list = [goods_id]                   
#                             idg = obj_move.create(cr,uid, {
#                                                         'name': prod.partner_ref,
#                                                         'date': dte,
#                                                         'product_id': pid,
#                                                         'product_uom': prod.uom_id.id,
#                                                         'product_uom_qty': i['Jumlah'],
#                                                         'location_id': 7,
#                                                         'location_dest_id': gudang.lot_stock_id.id,
#                                                         'picking_id': goods_id,
#                                                         'origin': posa.name,
#                                                         'company_id': gudang.company_id.id})
#                               
#                             bom_id = obj_bom._bom_find(cr, uid, prod.uom_id.id, product_id=pid, properties=[], context=None)
#                             if bom_id:
#                                 bom_point = obj_bom.browse(cr, uid, bom_id)
#                                 factor = obj_uom._compute_qty(cr, uid, prod.uom_id.id, i['Jumlah'], bom_point.product_uom.id)
#                                 material = obj_bom._bom_explode(cr, uid, bom_point, prod, factor/bom_point.product_qty, properties=[], routing_id=False, context={})[0]
#                                 material_id = obj_picking.create(cr,uid, {
#                                                             'origin': posa.name,
#                                                             'type_wo': 'rm',
#                                                             'picking_type_id': gudang.int_type_id.id,
#                                                             'move_type': 'one',
#                                                             'date': dte,
#                                                             'company_id': gudang.company_id.id,
#                                                         })
#                                   
#                                 picking_list.append(material_id)
#                                 sum = []
#                                 for x in material:
#                                     sum.append(obj_move.create(cr,uid, {
#                                                             'name': x['name'],
#                                                             'date': dte,
#                                                             'product_id': x['product_id'],
#                                                             'product_uom': x['product_uom'],
#                                                             'product_uom_qty': x['product_qty'],
#                                                             'location_id': gudang.lot_stock_id.id,
#                                                             'location_dest_id': 7,
#                                                             'picking_id': material_id,
#                                                             'origin': posa.name,
#                                                             'company_id': gudang.company_id.id}))
#                                                           
#                             obj_picking.action_confirm(cr, uid, picking_list, context=context)
#                             obj_picking.force_assign(cr, uid, picking_list, context=context)
#                             obj_picking.action_done(cr, uid, picking_list, context=context)
#                               
#                             cost = 0
#                             for s in obj_move.browse(cr, uid, sum):
#                                 harga = s.price_unit
#                                 if not harga:
#                                     harga = 1000
#                                 cost += harga*s.product_uom_qty
#       
#                             obj_move.write(cr, uid, [idg], {'price_unit': cost/i['Jumlah']})
#                             for m in obj_move.browse(cr, uid, idg).quant_ids:
#                                 self.pool.get('stock.quant').write(cr, uid, [m.id], {'cost': cost/i['Jumlah']})
#                   
#                             obj_order_line.create(cr, uid, {
#                                                             'order_id': orid,
#                                                             'product_id': pid,
#                                                             'qty': i['Jumlah'],
#                                                             'price_unit': i['Harga']
#                             })                    
#       
#                 order = obj_order.browse(cr, uid, orid)
#                 amount = order.amount_total - order.amount_paid
#                   
#                 nilai = {
#                          'payment_date': dte, 'payment_name': False,
#                          'journal': jurnal[gudang.company_id.id], 'amount': amount,
#                          'journal_id': (jurnal[gudang.company_id.id], 'Cash (IDR)'), 
#                 }
#                      
#                 if amount != 0.0:
#                     obj_order.add_payment(cr, uid, orid, nilai)
#                                                
#                 if obj_order.test_paid(cr, uid, [orid]):
#                     obj_order.signal_workflow(cr, uid, [orid], 'paid')
#                          
#                 for o in order.session_id.details_ids:
#                     self.pool.get('account.cashbox.line').write(cr, uid, [o.id], {'number_closing': amount})
#                      
#                 obj_session.write(cr, uid, [posid], {'cash_register_balance_end_real': amount})
#                 obj_session.signal_workflow(cr, uid, [posid], 'cashbox_control')
#                 obj_session.signal_workflow(cr, uid, [posid], 'close')
#                  
#             except Exception, e :
#                 pass
#                        
#         return {}






#                             sum = []
#                             for m in material:
#                                 sum.append(obj_move.create(cr,uid, {
#                                                         'name': m['name'],
#                                                         'date': dte,
#                                                         'product_id': m['product_id'],
#                                                         'product_uom': m['product_uom'],
#                                                         'product_uom_qty': m['product_qty'],
#                                                         'location_id': gudang.lot_stock_id.id,
#                                                         'location_dest_id': 7,
#                                                         'picking_id': material_id,
#                                                         'origin': 'BoM Tarikan POS',
#                                                         'company_id': gudang.company_id.id}))
                  
                        #obj_picking.action_confirm(cr, uid, picking_list, context=context)
                        #obj_picking.force_assign(cr, uid, picking_list, context=context)
                        #obj_picking.action_done(cr, uid, picking_list, context=context)
                  
#                         cost = 0
#                         for s in obj_move.browse(cr, uid, sum):
#                             harga = s.price_unit
#                             if not harga:
#                                 harga = 0
#                             cost += harga*s.product_uom_qty
#       
#                         obj_move.write(cr, uid, [idg], {'price_unit': cost/i['Jumlah']})
#                         for m in obj_move.browse(cr, uid, idg).quant_ids:
#                             self.pool.get('stock.quant').write(cr, uid, [m.id], {'cost': cost/i['Jumlah']})
               
            #obj_picking.action_confirm(cr, uid, [salid], context=context)
            #obj_picking.force_assign(cr, uid, [salid], context=context)
            #obj_picking.action_done(cr, uid, [salid], context=context)
