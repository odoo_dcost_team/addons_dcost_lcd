# -*- coding: utf-8 -*-
from openerp.osv import osv, orm, fields
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm

class ProductTemplate(osv.osv):
    _inherit = "product.template"

    _columns = {
        'name': fields.char('Name', required=True, translate=True, select=True, track_visibility='onchange'),
        'list_price': fields.float('Sale Price', digits_compute=dp.get_precision('Product Price'), track_visibility='onchange',
                                   help="Base price to compute the customer price. Sometimes called the catalog price."),
        'standard_price': fields.property(type='float', digits_compute=dp.get_precision('Product Price'), track_visibility='onchange',
                        help="Cost price of the product template used for standard stock valuation in accounting and used as a base price on purchase orders. Expressed in the default unit of measure of the product.", groups="base.group_user", string="Cost Price"),
        'uom_id': fields.many2one('product.uom', 'Unit of Measure', required=True, track_visibility='onchange',
                                  help="Default Unit of Measure used for all stock operation."),
        'uom_po_id': fields.many2one('product.uom', 'Purchase Unit of Measure', required=True, track_visibility='onchange',
            help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure."),
        'categ_id': fields.many2one('product.category', 'Internal Category', required=True, change_default=True,
                    domain="[('type','=','normal')]", track_visibility='onchange', help="Select category for the current product"),
    }

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        ctx = dict(context or {}, create_product_product=True)
        if self.pool['res.partner'].user_access_validation(cr, uid, [], context=ctx):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa membuat data Produk baru!'))
        return super(ProductTemplate, self).create(cr, uid, vals, context=ctx)

    def write(self, cr, uid, ids, vals, context=None):
        if self.pool['res.partner'].user_access_validation(cr, uid, ids, context=context):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa mengubah/mengedit data Produk ini!'))
        return super(ProductTemplate, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        ctx = dict(context or {}, active_test=False)
        if self.pool['res.partner'].user_access_validation(cr, uid, [], context=ctx):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa menghapus data Produk ini!'))
        return super(ProductTemplate, self).unlink(cr, uid, ids, context=context)

    # def unlink(self, cr, uid, ids, context=None):
    #     for tmpl in self.browse(cr, uid, ids, context=context):
    #         if cr.dbname in ['PB1', 'PB', 'OUTLET', 'PEBO']:
    #             if tmpl.purchase_ok and (tmpl.product_tmpl_src_id  or tmpl.groceries) and (uid != 1):
    #                 raise except_orm(_('Perhatian !!!'),
    #                                  _("Anda tidak bisa menghapus data produk ini karena tersinkronisasi dengan DB LOGISTIK,\n "
    #                                    " silahkan logout dan login ke DB LOGISTIK untuk menghapus produk ini."))
    #     return super(ProductTemplate, self).unlink(cr, uid, ids, context=context)



# class ProductProduct(osv.osv):
#     _inherit = "product.product"



