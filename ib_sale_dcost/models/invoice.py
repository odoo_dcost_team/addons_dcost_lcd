# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
###########################################################################################################
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import except_orm

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_invoice_tax = self.env['account.invoice.tax']
        account_move = self.env['account.move']
        sequence_obj = self.env['ir.sequence']
        user_obj = self.env['res.users']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise except_orm(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line:
                raise except_orm(_('No Invoice Lines!'), _('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            company_currency = inv.company_id.currency_id
            if not inv.date_invoice:
                # FORWARD-PORT UP TO SAAS-6
                if inv.currency_id != company_currency and inv.tax_line:
                    raise except_orm(
                        _('Warning!'),
                        _('No invoice date!'
                          '\nThe invoice currency is not the same than the company currency.'
                          ' An invoice date is required to determine the exchange rate to apply. Do not forget to update the taxes!'
                          )
                    )
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice

            # create the analytical lines, one move line per invoice line
            iml = inv._get_analytic_lines()
            # check if taxes are all computed
            compute_taxes = account_invoice_tax.compute(inv.with_context(lang=inv.partner_id.lang))
            inv.check_tax_lines(compute_taxes)

            # I disabled the check_total feature
            if self.env.user.has_group('account.group_supplier_inv_check_total'):
                if inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (
                            inv.currency_id.rounding / 2.0):
                    raise except_orm(_('Bad Total!'), _(
                        'Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))

            if inv.payment_term:
                total_fixed = total_percent = 0
                for line in inv.payment_term.line_ids:
                    if line.value == 'fixed':
                        total_fixed += line.value_amount
                    if line.value == 'procent':
                        total_percent += line.value_amount
                total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
                if (total_fixed + total_percent) > 100:
                    raise except_orm(_('Error!'), _(
                        "Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))

            # Force recomputation of tax_amount, since the rate potentially changed between creation
            # and validation of the invoice
            inv._recompute_tax_amount()
            # one move line per tax line
            iml += account_invoice_tax.move_line_get(inv.id)
            # insert potongan penjualan / diskon data pos
            # if inv.discount_method in ('percent','fixed') and inv.discount>0.0:
            #     iml += self.insert_discount_move_lines(inv.id)

            nomor_inv = False
            if inv.type in ('in_invoice', 'in_refund'):
                ref = inv.reference
                if inv.warehouse_id:
                    wh_code = inv.warehouse_id.code
                elif inv.user_id:
                    usr = user_obj.browse(inv.user_id.id)
                    wh_code = usr.warehouse_id and usr.warehouse_id.code
                else:
                    user = user_obj.browse(self._uid)
                    wh_code = user.warehouse_id and user.warehouse_id.code or 'XXX'
                inv_sequences = sequence_obj.get('supplier.invoice.dcost')
                supp_inv = list(str(inv_sequences))
                supp_inv.insert(4, wh_code + "/")
                nomor_inv = ''.join(supp_inv)
            else:
                ref = inv.number

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, ref, iml)

            name = inv.supplier_invoice_number or inv.name or '/'
            totlines = []
            if inv.payment_term:
                totlines = inv.with_context(ctx).payment_term.compute(total, date_invoice)[0]
            if totlines:
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'ref': ref,
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'ref': ref
                })

            date = date_invoice

            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)

            line = [(0, 0, self.line_get_convert(l, part.id, date)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            if journal.centralisation:
                raise except_orm(_('User Error!'),
                                 _(
                                     'You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))

            line = inv.finalize_invoice_move_lines(line)

            move_vals = {
                'ref': inv.reference or inv.name,
                'line_id': line,
                'journal_id': journal.id,
                'date': inv.date_invoice,
                'narration': inv.comment,
                'company_id': inv.company_id.id,
            }
            if nomor_inv:
                move_vals.update({'name': nomor_inv})
            ctx['company_id'] = inv.company_id.id
            period = inv.period_id
            if not period:
                period = period.with_context(ctx).find(date_invoice)[:1]
            if period:
                move_vals['period_id'] = period.id
                for i in line:
                    i[2]['period_id'] = period.id

            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.sudo(inv.user_id.id).with_context(ctx_nolang).create(move_vals)

            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'period_id': period.id,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
            move.post()
        self._log_event()
        return True

    @api.model
    def insert_discount_move_lines(self, invoice_id):
        user_obj = self.env['res.users']
        analytic_cbg_obj = self.env['analytic.cabang']
        inv = self.env['account.invoice'].browse(invoice_id)
        account_id = self.env['account.account'].search(['|',('code', 'like', '%240000.00.07%'),
                    ('name', 'like', '%Potongan Penjualan%')], limit=1)
        if inv.warehouse_id:
            wh_id = inv.warehouse_id.id
        elif inv.user_id:
            usr = user_obj.browse(inv.user_id.id)
            wh_id = usr.warehouse_id and usr.warehouse_id.id
        else:
            user = user_obj.browse(self._uid)
            wh_id = user.warehouse_id and user.warehouse_id.id or False
        analytic_cbg_id = analytic_cbg_obj.search([('name','=',wh_id),('account_id','=',account_id[-1].id)], limit=1)
        analytic_cbg = analytic_cbg_obj.browse(analytic_cbg_id[-1].id)
        return [{
            #'type': 'dest',
            'name': '%s' % ('[DISC] Potongan Penjualan'),
            'account_id': account_id[-1].id,
            'price_unit': (inv.discount * -1) or 0.0,
            'quantity': 1,
            'account_analytic_id': analytic_cbg.analytic_id and analytic_cbg.analytic_id.id,
            'currency_id': inv.currency_id.id,
            'price': (inv.discount * -1) or 0.0,
        }]



