#!/usr/bin/python
##########   IBROHIM BILADIN | ibradiiin@gmail.com | 083871909782   ########## #-*- coding: utf-8 -*-
from openerp.osv import osv, orm, fields
from openerp.tools.translate import _

import csv
import sys
import psycopg2
from datetime import datetime

QUERY = """SELECT * FROM sales_pos_order 
	WHERE source_id=CAST(%s AS numeric) OR 
	(branch_id=CAST(%s AS int) AND date=to_date(%s, 'DD-MM-YYYY'))"""


class middleware_sql(osv.osv):
    _name = "middleware.sql"
    _description = "Auto-Scheduler for Omzet and Menu Ranking"

    def run_scheduler_middleware_pb(self, cr, uid, ids, context=None):
        con = psycopg2.connect("dbname='PB1' user='odoo' password='Mys3@f00d' host='192.168.19.6'")
        pebo = con.cursor()

        pebo.execute("select outlet_id from stock_warehouse where active=true")
        pebo_outlet_code = [x[0] for x in pebo.fetchall()]

        with open("ResultOmzet.csv") as csvfile:
            r = csv.reader(csvfile, delimiter=';')
            print r
            for rows in r:
                params = (rows[0], rows[1], rows[2])
                pebo.execute(QUERY, params)  # check existing data
                res_pb = pebo.fetchone()  # cek data di db pebo
                if len(rows) > 1:  # len(rows)>3
                    id = rows[0]
                    idcabang = rows[1]
                    tanggal = datetime.strptime(rows[2], "%d-%m-%Y")
                    namacabang = str("DCOST ") + str(rows[3]).strip().title()
                    if rows[3] == 'BnB Hotel':
                        namacabang = str("DCOST ") + str(rows[3]).strip().upper()
                    elif rows[3] == 'Blok M':
                        namacabang = str("DCOST Blok M Square")
                    elif rows[3] == 'AirPort':
                        namacabang = str("DCOST Airport Hub")
                    elif rows[3][0:4] == 'DVIP':
                        namacabang = str(rows[3])
                        if rows[3] == 'DVIP BALI' or str(rows[3][4:]).strip() == 'BALI':
                            namacabang = str("DVIP Bali Sunset Road")
                    elif rows[3][0:3] in ('SGC', 'PGC', 'ITC', 'BSD'):
                        namacabang = str("DCOST ") + str(rows[3]).strip()
                    totalcash = (rows[4])
                    totalcard = (rows[5])
                    totalprepaid = (rows[6])
                    totaldiscount = (rows[7])
                    pb1 = (rows[8])
                    if (idcabang in pebo_outlet_code) and (not res_pb):
                        # print rows[0],rows[2]
                        pebo.execute('''SELECT id FROM stock_warehouse WHERE (outlet_id like %s OR outlet like %s) 
                                        AND active=TRUE LIMIT 1''',
                                     (rows[1], str("%DCOST ") + str(rows[3]) + str("%")))
                        res_wh = pebo.fetchone()
                        idwarehouse = res_wh[0] or False
                        pebo.execute("insert into sales_pos_order(date,prepaid_amount,cash_amount,discount,card_amount,\
        					branch_id,branch_name,amount_tax,src_mysql_id,invoiced,state,name,create_uid,write_date) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,False,%s,%s,%s,current_date)", \
                                     (tanggal, totalprepaid, totalcash, totaldiscount, totalcard, idcabang, namacabang,
                                      pb1, id, 'draft', idwarehouse, 1))
                        con.commit()
            pebo.close()
            con.close()

        return True

    def run_scheduler_middleware_lcd(self, cr, uid, ids, context=None):
        connect = psycopg2.connect("dbname='LCD' user='odoo8' password='Mys3@f00d' host='192.168.19.7'")
        lcd = connect.cursor()

        lcd.execute("select outlet_id from stock_warehouse where active=true")
        lcd_outlet_code = [y[0] for y in lcd.fetchall()]

        with open("ResultOmzet.csv") as csvfile:
            r = csv.reader(csvfile, delimiter=';')
            print r
            for rows in r:
                params = (rows[0], rows[1], rows[2])
                lcd.execute(QUERY, params)  # check existing data
                res_lcd = lcd.fetchone()  # cek data di db LCD
                if len(rows) > 1:  # len(rows)>3
                    id = rows[0]
                    idcabang = rows[1]
                    tanggal = datetime.strptime(rows[2], "%d-%m-%Y")
                    namacabang = str("DCOST ") + str(rows[3]).title()
                    if rows[3] == 'BnB Hotel':
                        namacabang = str("DCOST ") + str(rows[3]).upper()
                    elif rows[3] == 'Blok M':
                        namacabang = str("DCOST Blok M Square")
                    elif rows[3] == 'Armada Town Square':
                        namacabang = str("DCOST Magelang")
                    totalcash = (rows[4])
                    totalcard = (rows[5])
                    totalprepaid = (rows[6])
                    totaldiscount = (rows[7])
                    pb1 = (rows[8])
                    if (rows[1] in lcd_outlet_code) and (not res_lcd):
                        # print rows[0],rows[2]
                        lcd.execute('''SELECT id FROM stock_warehouse 
        					WHERE (outlet_id like %s OR outlet like %s) AND active=TRUE LIMIT 1''',
                                    (rows[1], str("%DCOST ") + str(rows[3]) + str("%")))
                        res_wh = lcd.fetchone()
                        idwarehouse = res_wh[0] or False
                        # print res_wh
                        lcd.execute("insert into sales_pos_order(date,prepaid_amount,cash_amount,discount,card_amount,\
        						branch_id,branch_name,amount_tax,src_mysql_id,invoiced,state,name,create_uid,write_date) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,False,%s,%s,%s,current_date)", \
                                    (tanggal, totalprepaid, totalcash, totaldiscount, totalcard, idcabang, namacabang,
                                     pb1, id, 'draft', idwarehouse, 1))
                        connect.commit()

            lcd.close()
            connect.close()

        return True