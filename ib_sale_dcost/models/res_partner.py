##-*- coding: utf-8 -*-
##########   IBRAHIM BILADIN | ibradiiin@gmail.com | 083871909782   ##########
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _
    
class res_partner(osv.Model):
    _inherit = "res.partner"

    _columns = {
        'kelurahan_id': fields.many2one('res.partner.kelurahan', 'Kelurahan'),
        'kecamatan_id': fields.many2one('res.partner.kecamatan', 'Kecamatan'),
        'kota_id': fields.many2one('res.partner.kota', 'Kota/Kabupaten'),
        'code': fields.char('Partner Code', size=64),
    }

    _sql_constraints = [
        ('customer_code_uniq', 'unique (code,company_id)', 'The code of the partner must be unique per company !')
    ]
    
    def create(self, cr, uid, vals, context=None):
        account_obj = self.pool.get('account.account')
        if context is None:
            context = {}

        if self.user_access_validation(cr, uid, [], context=None):
            #uid not in (1, 206, 207, 347, 353, 375, 376, 371, 394, 408)
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa membuat Customer/Supplier baru!'))

        ar_account_id = ap_account_id = False
        if 'supplier' in vals and vals['supplier']:
            ap_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%121210.00.02%'),
                                         ('name', 'like', '%Hutang Usaha ke Supplier Sudah Ditagih%')], limit=1)[0]
        elif 'customer' in vals and vals['customer']:
            ar_account_id = account_obj.search(cr, uid, ['&', '|', ('active', '=', True), ('code', 'like', '%111140.00.01%'),
                                         ('name', 'like', '%Piutang Usaha Penjualan Harian%')], limit=1)[0]
        if ar_account_id:
            vals['property_account_receivable'] = ar_account_id
        if ap_account_id:
            vals['property_account_payable'] = ap_account_id
        return super(res_partner, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if self.user_access_validation(cr, uid, ids, context=None):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa mengubah/mengedit data Customer/Supplier ini!'))
        return super(res_partner, self).write(cr, uid, ids, vals, context=context)

    def unlink(self, cr, uid, ids, context=None):
        if self.user_access_validation(cr, uid, ids, context=None):
            raise osv.except_osv(_('Perhatian !!!'),
                                 _('Akses anda terbatas. Anda tidak bisa menghapus data Customer/Supplier ini!'))
        return super(res_partner, self).unlink(cr, uid, ids, context=context)

    def user_access_validation(self, cr, uid, ids, context=None):
        groups_obj = self.pool.get('res.groups')
        allow_group_ids, allowed_users = [], []
        categ_module_ids = self.pool.get('ir.module.category').search(cr, SUPERUSER_ID,
                                        [('name', '=', 'Administrasi Setting')], limit=1) or []
        if categ_module_ids:
            allow_group_ids = groups_obj.search(cr, SUPERUSER_ID,
                                                ['&', ('category_id', '=', categ_module_ids[0]), '|',
                                                 ('name','in',('Admin Accounting/Finance','Admin Operational')),
                                                 ('comment','in',('ADMIN - OPS','ADMIN-ACC'))], context=context)
        if allow_group_ids:
            for group in groups_obj.browse(cr, uid, allow_group_ids):
                for user in group.users:
                    allowed_users.append(user.id)
        if (uid not in allowed_users):
            return True
        else:
            return False

    def onchange_partner_name(self, cr, uid, ids, partner_name, context=None):
        if not partner_name:
            return {'value': {'name': "%s" % (''),}}

        if partner_name:
            partners = self.pool.get('res.partner').search(cr, uid, ['|','|',('name','like',partner_name),('name','like','%'+ partner_name),('name','like',partner_name +'%')], context=context)
            if partners:
                raise osv.except_osv(_('Warning !!!'), _('Nama Customer/Supplier sudah ada di Database,\n Silahkan input yang lainnya.'))
        return {'value': {'name': partner_name}}


class res_partner_kelurahan(orm.Model):
    _name = "res.partner.kelurahan"
    _columns = {
        'name': fields.char('Kelurahan', required=True),
        ##'zip': fields.integer('Kode POS'),
        'kecamatan_id': fields.many2one('res.partner.kecamatan', 'Kecamatan'),
    }


class res_partner_kecamatan(orm.Model):
    _name = "res.partner.kecamatan"
    _columns = {
        'name': fields.char('Kecamatan', required=True),
        'kota_id': fields.many2one('res.partner.kota', 'Kota/Kabupaten'),
    }


class res_partner_kota(orm.Model):
    _name = "res.partner.kota"
    _columns = {
        'name': fields.char('Kota/Kabupaten', required=True),
        'state_id': fields.many2one('res.country.state', 'State'),
        'type': fields.selection([('kota', 'Kota'), ('kab', 'Kabupaten')], 'Jenis'),
        'jabodetabek': fields.boolean(string='JABODETABEK', help="Label untuk wilayah JABODETABEK"),
    }
    _defaults = {
        'jabodetabek': False,        
    }

