# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

class posting_journal_atonce(osv.osv_memory):
    _name = "posting.jornal.at.once"
    _description = "Posting the jornal at once"

    def _check_acc_move_list(self, cr, uid, context):
        if context.get('active_model', '') == 'account.move':
            ids = context['active_ids']
            move_obj = self.pool.get('account.move')
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            if ids or (len(ids) > 1):
                moves = move_obj.read(cr, uid, ids, ['state','company_id',])
                for m in moves:
                    if m['state'] <> 'draft':
                        raise orm.except_orm(_('Warning'), _('At least one of the selected journal entries is %s! \nJournal Entries status allowed is Draft.') % m['state'])
                    if m['company_id'][0] <> user.company_id.id:
                        raise orm.except_orm(_('Warning'), _('Not all Journal Entries are in the same company as the user!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(posting_journal_atonce, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_acc_move_list(cr, uid, context)
        return res


    def validate_journal(self, cr, uid, ids, context=None):
        move_obj = self.pool.get('account.move')
        move_ids = context.get('active_ids', [])
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        #data_wzd = self.browse(cr, uid, ids[0], context=context)
        if move_ids and (len(move_ids) > 0):
            cr.execute('''SELECT uid AS usr_id FROM res_groups_users_rel 
                    WHERE gid IN (SELECT id FROM res_groups 
                    WHERE name like 'Invoicing%' or name like '%Payments%' 
                        or name like '%Financial Manager%')''')
            allow_users = []
            for dt in cr.dictfetchall():
                if dt['usr_id'] not in allow_users:
                    allow_users.append(dt['usr_id'])

            if (context.get('active_model', '')=='account.move'):
                if user.id not in allow_users:
                    raise osv.except_osv(_('Error!'), _('You include users who are not allowed to post journals.'))
                else:
                    move_obj.button_validate(cr, uid, move_ids, context=context)

        return {'type': 'ir.actions.act_window_close'}


