# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782
from openerp import models, fields, api, _
import openerp.addons.decimal_precision as dp

import logging
_logger = logging.getLogger(__name__)


class MenuRankingInformation(models.TransientModel):
    _name = "sale.menu_rank_info"
    _description = "Data POS Menu Information"

    work_order_id = fields.Many2one('work.order', string="Work Order", readonly=True, default=lambda self: self._context.get('active_id', False))
    name = fields.Char(string='Menu Name', readonly=True)
    menu = fields.Char(string='Menu ID', readonly=True)
    src_mysql_id = fields.Char(string='Source Database ID', readonly=True)
    branch_name = fields.Char(string='Branch Name', readonly=True)
    warehouse_id = fields.Many2one('stock.warehouse', string='Outlet [WH]', readonly=True)
    qty = fields.Float(string='Qty [Porsi]', digits=dp.get_precision('Stock Weight'), readonly=True, default=0.0)
    amount_total = fields.Float(string='Total', digits=dp.get_precision('Account'), readonly=True, default=0.0)
    unit_price = fields.Float(string='Unit Price', digits=dp.get_precision('Product Price'), readonly=True, default=0.0)
    date = fields.Date(string='Date', readonly=True)
    branch_id = fields.Integer(string='Sequence', readonly=True)

    @api.onchange('work_order_id')
    def onchange_work_order_id(self):
        menu_pos = self.env['mrp.pos.order'].search([('work_order_id', '=', self.work_order_id.id)], limit=1)
        if menu_pos:
            mrp_pos = self.env['mrp.pos.order'].browse(menu_pos.id)
            return {'value': {
                'date': mrp_pos.date,
                'name': mrp_pos.name,
                'menu': mrp_pos.menu,
                'warehouse_id': mrp_pos.warehouse_id and mrp_pos.warehouse_id.id,
                'qty': mrp_pos.qty,
                'unit_price': mrp_pos.unit_price,
                'amount_total': mrp_pos.amount_total,
                'branch_id': mrp_pos.branch_id,
                'branch_name': mrp_pos.branch_name,
                'src_mysql_id': mrp_pos.src_mysql_id,
            }}

        return {'value': {}}


