# -*- coding: utf-8 -*-
##############################################################################
import logging
import threading
from openerp import SUPERUSER_ID
from openerp import tools

from openerp.osv import fields,osv
from openerp.api import Environment

_logger = logging.getLogger(__name__)

class pos_omzet_compute_all(osv.osv_memory):
    _name = 'sales.pos.order.compute.all'
    _description = 'Compute all scheduler for POS Omzet'

    _columns = {
        'period_from': fields.many2one('account.period', 'Start Period', required=True),
        'period_to': fields.many2one('account.period', 'End Period', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
    }

    def _pos_omzet_calculation_all(self, cr, uid, ids, start_date=False, end_date=False, warehouse_id=False, context=None):
        with Environment.manage():
            omzet_obj = self.pool.get('sales.pos.order')
            #As this function is in a new thread, i need to open a new cursor, because the old one may be closed

            new_cr = self.pool.cursor()
            scheduler_cron_id = self.pool['ir.model.data'].get_object_reference(new_cr, SUPERUSER_ID, 'ib_sale_dcost', 'ir_cron_convert_pos_to_sales_action')[1]
            # Avoid to run the scheduler multiple times in the same time
            try:
                with tools.mute_logger('openerp.sql_db'):
                    new_cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron_id,))
            except Exception:
                _logger.info('Attempt to run pos omzet scheduler aborted, as already running')
                new_cr.rollback()
                new_cr.close()
                return {}
            user = self.pool.get('res.users').browse(new_cr, uid, uid, context=context)
            #comps = [x.id for x in user.company_ids]
            #for comp in comps:
            omzet_obj.run_scheduler(new_cr, uid, use_new_cursor=new_cr.dbname, date_start=start_date, date_stop=end_date, warehouse_id=warehouse_id, context=context)
            new_cr.close()
            return {}

    def poz_omzet_calculation(self, cr, uid, ids, context=None):
        data_wzd = self.browse(cr, uid, ids[0], context=context)
        omzet_ids = context.get('active_ids', [])
        start_date = data_wzd.period_from and data_wzd.period_from.date_start
        end_date = data_wzd.period_to and data_wzd.period_to.date_stop
        warehouse_id = data_wzd.warehouse_id and data_wzd.warehouse_id.id
        self._pos_omzet_calculation_all(cr, uid, omzet_ids, start_date, end_date, warehouse_id, context)
        #threaded_calculation = threading.Thread(target=self._pos_omzet_calculation_all, args=(cr, uid, ids, start_date, end_date, context))
        #threaded_calculation.start()
        return {'type': 'ir.actions.act_window_close'}



class stock_move_autocreate_entry(osv.osv_memory):
    _name = 'stock.move.autocreate.journal'
    _description = 'AutoCreate and post account move from stock move'

    _columns = {
        'date_start': fields.datetime('Starting Date', select=True),
        'date_end': fields.datetime('Ending Date', select=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
    }

    def _stock_move_calculate_entry(self, cr, uid, ids, start_date=False, end_date=False, warehouse_id=False, context=None):
        with Environment.manage():
            move_obj = self.pool.get('stock.move')
            new_cr = self.pool.cursor()
            scheduler_cron_id = self.pool['ir.model.data'].get_object_reference(new_cr, SUPERUSER_ID,
                                                                'ib_sale_dcost', 'ir_cron_autocreate_post_journal_act')[1]
            # Avoid to run the scheduler multiple times in the same time
            try:
                with tools.mute_logger('openerp.sql_db'):
                    new_cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron_id,))
            except Exception:
                _logger.info('Attempt to run stock move scheduler aborted, as already running')
                new_cr.rollback()
                new_cr.close()
                return {}
            #user = self.pool.get('res.users').browse(new_cr, uid, uid, context=context)
            move_obj.run_autocreate_posting_journal(new_cr, uid, use_new_cursor=new_cr.dbname,
                        date_start=start_date, date_stop=end_date, warehouse_id=warehouse_id, context=context)
            new_cr.close()
            return {}

    def move_calculate_entry(self, cr, uid, ids, context=None):
        wzd = self.browse(cr, uid, ids[0], context=context)
        move_ids = context.get('active_ids', [])
        warehouse_id = wzd.warehouse_id and wzd.warehouse_id.id
        self._stock_move_calculate_entry(cr, uid, move_ids, wzd.date_start, wzd.date_end, warehouse_id, context)
        return {'type': 'ir.actions.act_window_close'}


class pos_menu_ranking_compute_all(osv.osv_memory):
    _name = 'menu.ranking.compute.all'

    _columns = {
        'period_from': fields.many2one('account.period', 'Start Period', required=True),
        'period_to': fields.many2one('account.period', 'End Period', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
    }

    def _menu_ranking_execute_all(self, cr, uid, ids, start_date=False, end_date=False, warehouse_id=False, context=None):
        with Environment.manage():
            menu_pos_obj = self.pool.get('mrp.pos.order')
            # As this function is in a new thread, i need to open a new cursor, because the old one may be closed

            new_cr = self.pool.cursor()
            scheduler_cron_id = self.pool['ir.model.data'].get_object_reference(new_cr, SUPERUSER_ID, 'ib_sale_dcost',
                                                                                'ir_cron_pos_menu_ranking_action')[
                1]
            # Avoid to run the scheduler multiple times in the same time
            try:
                with tools.mute_logger('openerp.sql_db'):
                    new_cr.execute("SELECT id FROM ir_cron WHERE id = %s FOR UPDATE NOWAIT", (scheduler_cron_id,))
            except Exception:
                _logger.info('Attempt to run pos menu ranking scheduler aborted, as already running')
                new_cr.rollback()
                new_cr.close()
                return {}
            user = self.pool.get('res.users').browse(new_cr, uid, uid, context=context)
            menu_pos_obj.run_scheduler(new_cr, uid, use_new_cursor=new_cr.dbname, date_start=start_date,
                                    date_stop=end_date, warehouse_id=warehouse_id, context=context)
            new_cr.close()
            return {}

    def menu_ranking_calculation(self, cr, uid, ids, context=None):
        data_wzd = self.browse(cr, uid, ids[0], context=context)
        menu_ranking_ids = context.get('active_ids', [])
        start_date = data_wzd.period_from and data_wzd.period_from.date_start
        end_date = data_wzd.period_to and data_wzd.period_to.date_stop
        warehouse_id = data_wzd.warehouse_id and data_wzd.warehouse_id.id
        self._menu_ranking_execute_all(cr, uid, menu_ranking_ids, start_date, end_date, warehouse_id, context)
        return {'type': 'ir.actions.act_window_close'}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

