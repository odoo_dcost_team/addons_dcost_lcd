### -*- coding: utf-8 -*-
##############################################################################
#####   IBRAHIM BILADIN   #####
##############################################################################
import time
from openerp.osv import osv, fields, orm
from openerp.tools.translate import _
        

class pos_make_to_work_order(osv.osv_memory):
    _name = "pos.make.work.order"
    _description = "Data POS Make to Work Order"
    
    def _dirty_pos_line_check(self, cr, uid, context):
        if context.get('active_model', '') == 'pos.manufacture':
            ids = context['active_ids']
            if len(ids) > 1:
                pos = self.pool.get('pos.manufacture').read(cr, uid, ids, ['quantity','branch_id','product_id','menu'])
                for d in pos:
                    if not d['branch_id']:
                        raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang belum ada Outlet ID / Warehouse ID nya, silahkan cek kembali...\n[Menu: %s].') % (d['menu'],))
                    if not d['product_id']:
                        raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang belum ada Produk ID nya, silahkan cek kembali...\n[Menu: %s].') % (d['menu'],))
                    else:
                        prod = self.pool.get('product.product').browse(cr, uid, d['product_id'][0], context=None)
                        bom_id = self.pool.get('mrp.bom')._bom_find(cr, uid, product_tmpl_id=prod.product_tmpl_id.id, product_id=prod.id, properties=[], context=None)
                        if not bom_id:
                            raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang belum ada ID Susunan Material nya (Bill of Material ID), silahkan cek kembali...\n[Menu: %s].') % (d['menu'],))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(pos_make_to_work_order, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._dirty_pos_line_check(cr, uid, context)
        return res
    
    def _create_wo_from_pos(self, cr, uid, pos_ids, context=None):
        if context is None:
            context = {}
        bom_obj = self.pool.get('mrp.bom')
        wo_obj = self.pool.get('work.order')
        product_obj = self.pool.get('product.product')
        uom_obj = self.pool.get('product.uom')
        pos_data = self.pool.get('pos.manufacture')
        manufaktur_obj = self.pool.get('x.manufacturing') 
        wo_ids, material, product_ids = [], [], []
        for rec in pos_data.browse(cr, uid, pos_ids, context=context):
            if not rec.branch_id:
                raise osv.except_osv(_('Warning!'), _('Outlet ID kosong!\nLengkapi Data POS dengan Outlet ID masing-masing produk.'))
            prod = product_obj.browse(cr, uid, rec.product_id.id, context=context)
            bom_id = bom_obj._bom_find(cr, uid, product_tmpl_id=prod.product_tmpl_id.id, product_id=prod.id, properties=[], context=context) or False
            if not prod.id:
                raise osv.except_osv(_('Warning!'), _('Product/Menu ID kosong!\nSilahkan lengkapi Data POS dengan Product/Menu ID masing-masing.'))
            if not bom_id:
                raise osv.except_osv(_('Warning!'), _('BoM ID kosong!\nSilahkan lengkapi Data POS dengan BoM (Material) ID masing-masing.'))
            
            wo_id = self.pool.get('work.order').create(cr, uid, {
                    'name': self.pool.get('ir.sequence').get(cr, uid, 'work.order') or '/',
                    'date' : rec.date or time.strftime('%Y-%m-%d'), 'bom_id': bom_id,
                    'product_id': prod.id, 'product_qty': float(rec.quantity) or 1.0,
                    'product_uom': rec.product_uom and rec.product_uom.id or prod.uom_id.id ,
                    'warehouse_id': rec.branch_id and rec.branch_id.id,}, context=context)
            
            if bom_id:
                bom_point = bom_obj.browse(cr, uid, bom_id)
                factor = uom_obj._compute_qty(cr, uid, prod.uom_id.id, float(rec.quantity), bom_point.product_uom.id)
                material = bom_obj._bom_explode(cr, uid, bom_point, prod, factor/bom_point.product_qty, properties=[], 
                        routing_id=False, context={})[0]
                for line in material:
                    self.pool.get('raw.material.line').create(cr, uid, {'name': line['name'], 'work_id': wo_id,
                            'product_id': line['product_id'], 'product_qty': line['product_qty'],
                            'product_uom': line['product_uom'],}, context=context)
                    #if rec.quantity and float(rec.quantity) > 0:
                        #line['product_qty'] = (line['product_qty'] * float(rec.quantity))
                    
            if wo_id:
                if wo_id not in wo_ids:
                    wo_ids.append(wo_id)
                manufaktur_obj.write(cr, uid, [rec.id], {'work_order_id': wo_id, 'work_order': True}, context=context)
                self.pool.get('work.order.pos.line').create(cr, uid, {
                        'work_order_id': wo_id,
                        'pos_id': rec.id,
                        'date': rec.date,
                        'menu': rec.menu,
                        'product_id': rec.product_id and rec.product_id.id,
                        'product_uom': rec.product_uom and rec.product_uom.id,
                        'product_qty': float(rec.quantity),
                        'unit_price': float(rec.unit_price),
                        'amount_total': float(rec.amount_total),
                        'branch_id': rec.branch_id and rec.branch_id.id}, context=context)
                cr.execute('''SELECT * FROM pos_work_order_rel WHERE wo_id=%s AND pos_id=%s''',(wo_id, rec.id,))
                if not cr.fetchone():
                    cr.execute('INSERT INTO pos_work_order_rel (wo_id,pos_id) values (%s,%s)', (wo_id, rec.id,))
                    
        if wo_ids:
            wo_obj.work_confirm(cr, uid, wo_ids, context=context)
            wo_obj.work_validate(cr, uid, wo_ids, context=context)
        
        return wo_ids

    def create_work_order(self, cr, uid, ids, context=None):
        active_ids = context and context.get('active_ids', [])
        res = self._create_wo_from_pos(cr, uid, active_ids, context) or False

        if not res:
            raise osv.except_osv(_('Warning!'), _('Work Order cannot be created for this data (POS) because the data has been done.'))
        if context.get('open_work_orders', False):
            return self.open_work_orders(cr, uid, ids, res, context=context)
        return {'type': 'ir.actions.act_window_close'}

    def open_work_orders(self, cr, uid, ids, wo_ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'siu_work_order', 'action_work_order_form',)  # 'view_work_order_tree'
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        #choose the view_mode accordingly
        if len(wo_ids)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, wo_ids))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'siu_work_order', 'view_work_order_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = wo_ids and wo_ids[0] or False
        return result
    

pos_make_to_work_order()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
