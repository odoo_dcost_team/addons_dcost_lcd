### -*- coding: utf-8 -*-
##############################################################################
#####   IBRAHIM BILADIN   #####
##############################################################################
import time
from openerp.osv import osv, fields, orm
from openerp.tools.translate import _
from openerp.osv.orm import browse_record, browse_null
from lxml import etree
from functools import update_wrapper


#===============================================================================
# class PowerDict(dict):
#     # http://stackoverflow.com/a/3405143/190597 (gnibbler)
#     def __init__(self, parent=None, key=None):
#         self.parent = parent
#         self.key = key
# 
#     def __missing__(self, key):
#         self[key] = PowerDict(self, key)
#         return self[key]
# 
#     def append(self, item):
#         self.parent[self.key] = [item]
# 
#     def __setitem__(self, key, val):
#         dict.__setitem__(self, key, val)
#         try:
#             val.parent = self
#             val.key = key
#         except AttributeError:
#             pass
#===============================================================================
        

class pos_line_make_order(osv.osv_memory):
    _name = "pos.make.order"
    _description = "Data POS Make to Sale Order"
    
    def _dirty_pos_line_check(self, cr, uid, context):
        if context.get('active_model', '') == 'pos.sales':
            ids = context['active_ids']
            if len(ids) > 1:
                pos = self.pool.get('pos.sales').read(cr, uid, ids, ['id','ordered','sale_id','date','branch_id'])
                for d in pos:
                    tanggal = cabang = ''
                    if d['date']:
                        exp = str(d['date']).split("-")
                        tanggal = exp[2] +"-"+ exp[1] +"-"+ exp[0]
                    if d['branch_id']:
                        cabang = self.pool.get('stock.warehouse').browse(cr, uid, d['branch_id'][0], context=context).name
                         
                    if not d['date']:
                        raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang tidak ada tanggal transaksinya. Silahkan cek kembali ID %s - cabang %s.') % (d['id'],cabang,))
                    if not d['branch_id']:
                        raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang tidak ada Cabang / Outlet nya. Silahkan cek kembali ID %s - tanggal %s.') % (d['id'],tanggal,))
                    if d['ordered'] == True or d['sale_id']:
                        raise orm.except_orm(_('Warning'), _('Ada salah satu Data POS yang sudah dijadikan SO yaitu tanggal %s cabang %s.\nSales Order dapat dibuat jika data POS belum di order (Ordered is False).') % (tanggal, cabang,))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(pos_line_make_order, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._dirty_pos_line_check(cr, uid, context)
        return res
    
    _columns = {
        'order_method':fields.selection(
            [('new', 'Create a new sale order'), ('merge', 'Merge with another sale order')],
            'Create a new sale order or Merge with another sale order?', required=True),
        'order_id': fields.many2one('sale.order', 'Sales Order (Existing - State:Draft)'),
    }
    
    def onchange_method(self, cr, uid, ids, method, context=None):
        return {'value': {'order_id': False}}
    
    def _create_so(self, cr, uid, pid, pos_data, context=None):  #pos, date, wh_id, discount,
        res_partner = self.pool.get('res.partner')
        order_obj = self.pool.get('sale.order')
        curr_obj = self.pool.get('product.pricelist')
        
        company_id = self.pool.get('res.users').browse(cr,uid,uid,context=context).company_id.id
        customer = res_partner.browse(cr, uid, pid, context=context)
        pricelist = customer.property_product_pricelist.id
        currency = curr_obj.browse(cr, uid, pricelist, context=context).currency_id.id
        payment_term = customer.property_payment_term and customer.property_payment_term.id or False
        fpos = customer.property_account_position and customer.property_account_position.id or False
        values = {
            'name': self.pool.get('ir.sequence').get(cr, uid, 'sale.order', context=context) or '/',
            'order_policy': 'manual',
            'partner_id': customer.id,
            'partner_shipping_id': customer.id,
            'partner_invoice_id': customer.id,
            'pricelist_id': pricelist,
            'currency_id': currency,
            'warehouse_id': pos_data.branch_id and pos_data.branch_id.id,
            'payment_term': payment_term,
            'fiscal_position': fpos,
            'date_order': time.strftime('%Y-%m-%d'), #date or time.strftime('%Y-%m-%d'), 
            'state': 'draft',
            'company_id': company_id,
            'user_id': uid,
        }
        return order_obj.create(cr, uid, values, context=context)

    def _create_so_line(self, cr, uid, data, context=None):
        product_uom = False
        cr.execute("SELECT id FROM product_uom WHERE name like %s or name like %s ORDER BY id ASC LIMIT 1", ('%day%','%Day%'))
        result = cr.fetchall()
        if result:
            for i in result:
                product_uom = i[0]
        else:
            cr.execute("SELECT id FROM product_uom WHERE name like %s or name like %s ORDER BY id ASC LIMIT 1", ('%lot%','%Lot%'))
            result = cr.fetchall()
            for i in result:
                product_uom = i[0]
        val = {
            'order_id': data['order_id'],
            'name': data['name'],
            'product_uom_qty': 1.0,
            'product_uom': product_uom,
            'price_unit': data['amount'] or 0.0,
        }
        return self.pool.get('sale.order.line').create(cr, uid, val, context=context)
       
    def _create_orders(self, cr, uid, pos_ids, oid, context=None):
        if context is None:
            context = {}
        pos_data = self.pool.get('pos.sales')
        order_obj = self.pool.get('sale.order')
        order_line_obj = self.pool.get('sale.order.line')
        res_partner = self.pool.get('res.partner')
        omzet_obj = self.pool.get('x.omzet')
        order_ids, pos_data_ids = [], []
        warehouses, discounts, olines = {}, {}, {}
        sale_id = False
        part_ids = res_partner.search(cr, uid, [('name', 'like', '%All Customer%')], context=context)
        partner_id = res_partner.browse(cr, uid, part_ids[0] or 9732, context=context).id
        
        for pos in pos_data.browse(cr, uid, pos_ids, context=context):
            if not pos.branch_id:
                raise osv.except_osv(_('Error!'), _('Salah satu data POS tidak ada ID Cabang/Outlet nya, Silahkan cek kembali data POS...'))
            if (not pos.cash_amount) and (not pos.card_amount) and (not pos.prepaid_amount):
                raise osv.except_osv(_('Error!'), _('Salah satu data POS tidak ada jumlah transaksinya (Cash,Card ataupun Prepaid), Silahkan cek kembali...'))
            
            if not oid:
                if pos.branch_id.id not in warehouses.keys():
                    sale_id = self._create_so(cr, uid, partner_id, pos, context=context)
                    warehouses.update({pos.branch_id.id: sale_id})
                    if pos.discount and float(pos.discount) > 0.0:
                        discounts.update({sale_id: float(pos.discount)})
                    olines[sale_id] = {}
                    if pos.cash_amount and float(pos.cash_amount) > 0.0:
                        olines[sale_id]['Cash'] = float(pos.cash_amount)
                    if pos.card_amount and float(pos.card_amount) > 0.0:
                        olines[sale_id]['Card'] = float(pos.card_amount)
                    if pos.prepaid_amount and float(pos.prepaid_amount) > 0.0:
                        olines[sale_id]['Prepaid'] = float(pos.prepaid_amount)
                else:
                    sale_id = warehouses[pos.branch_id.id]
                    if float(pos.discount) > 0.0:
                        if sale_id in discounts.keys():
                            discounts.update({sale_id: discounts[sale_id] + float(pos.discount)})
                        else:
                            discounts.update({sale_id: float(pos.discount)})
                    if sale_id not in olines.keys():
                        olines[sale_id] = {}
                        if pos.cash_amount and float(pos.cash_amount) > 0.0:
                            olines[sale_id]['Cash'] = float(pos.cash_amount)
                        if pos.card_amount and float(pos.card_amount) > 0.0:
                            olines[sale_id]['Card'] = float(pos.card_amount)
                        if pos.prepaid_amount and float(pos.prepaid_amount) > 0.0:
                            olines[sale_id]['Prepaid'] = float(pos.prepaid_amount)
                    else: 
                        if ('Cash' in olines[sale_id]) and float(pos.cash_amount) > 0.0:
                            olines[sale_id]['Cash'] = olines[sale_id]['Cash'] + float(pos.cash_amount)
                        else:
                            if float(pos.cash_amount) > 0.0:
                                olines[sale_id]['Cash'] = float(pos.cash_amount)
                        if ('Card' in olines[sale_id]) and float(pos.card_amount) > 0.0:
                            olines[sale_id]['Card'] = olines[sale_id]['Card'] + float(pos.card_amount)
                        else:
                            if float(pos.card_amount) > 0.0:
                                olines[sale_id]['Card'] = float(pos.card_amount)
                        if ('Prepaid' in olines[sale_id]) and float(pos.prepaid_amount) > 0.0:
                            olines[sale_id]['Prepaid'] = olines[sale_id]['Prepaid'] + float(pos.prepaid_amount)
                        else:
                            if float(pos.prepaid_amount) > 0.0:
                                olines[sale_id]['Prepaid'] = float(pos.prepaid_amount)
            else:
                sale_id = oid
                oline_descs = []
                sale_orders = order_obj.read(cr, uid, [sale_id], ['state','discount','warehouse_id'], context=context)
                if sale_id not in olines.keys():
                    olines[sale_id] = {}
                for s in sale_orders:
                    if s['state'] <> 'draft':
                        raise osv.except_osv(_('Error!'), 
                            _('Sales Order tidak valid karena berstatus %s. Data POS hanya dapat digabungkan ke dalam SO yang mempunyai status Draft.') % (s['state'],))
                    if pos.branch_id.id <> s['warehouse_id'][0]:
                        raise osv.except_osv(_('Error!'), _('ID Cabang / Outlet data POS tidak sama dengan Sales Order (%s),\nSilahkan cek kembali...\n[ID: %s; Outlet: %s; Date: %s]') % (s['warehouse_id'][1],pos.id,pos.branch_id.name,pos.date,))
                    if pos.discount and float(pos.discount) > 0.0:
                        if s['discount']:
                            discounts.update({sale_id: s['discount'] + float(pos.discount)})
                        else:
                            if sale_id in discounts.keys():
                                discounts.update({sale_id: discounts[sale_id] + float(pos.discount)})
                            else: 
                                discounts.update({sale_id: float(pos.discount)})
                olines_ids = order_line_obj.search(cr, uid, [('order_id','=',sale_id)], context=context)
                for line in order_line_obj.browse(cr, uid, olines_ids, context=context):
                    if line.name=='Cash' and float(pos.cash_amount) > 0.0:
                        order_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + float(pos.cash_amount))}, context=context)
                    if line.name=='Card' and float(pos.card_amount) > 0.0:
                        order_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + float(pos.card_amount))}, context=context)
                    if line.name=='Prepaid' and float(pos.prepaid_amount) > 0.0:
                        order_line_obj.write(cr, uid, [line.id], {'price_unit': (line.price_unit + float(pos.prepaid_amount))}, context=context)
                    oline_descs.append(line.name)
                
                if ('Cash' in olines[sale_id].keys()):
                    olines[sale_id]['Cash'] = olines[sale_id]['Cash'] + float(pos.cash_amount)
                else:
                    if ('Cash' not in oline_descs) and float(pos.cash_amount) > 0.0:
                        olines[sale_id]['Cash'] = float(pos.cash_amount)
                if ('Card' in olines[sale_id].keys()):
                    olines[sale_id]['Card'] = olines[sale_id]['Card'] + float(pos.card_amount)
                else:
                    if ('Card' not in oline_descs) and float(pos.card_amount) > 0.0:
                        olines[sale_id]['Card'] = float(pos.card_amount)
                if ('Prepaid' in olines[sale_id].keys()):
                    olines[sale_id]['Prepaid'] = olines[sale_id]['Prepaid'] + float(pos.prepaid_amount)
                else:
                    if ('Prepaid' not in oline_descs) and float(pos.prepaid_amount) > 0.0:
                        olines[sale_id]['Prepaid'] = float(pos.prepaid_amount)
                        
                if pos.branch_id.id not in warehouses.keys():
                    warehouses.update({pos.branch_id.id: sale_id})
                
            if sale_id not in order_ids:
                order_ids.append(sale_id)
            if pos.id not in pos_data_ids:
                pos_data_ids.append(pos.id)
            if sale_id:
                omzet_obj.write(cr, uid, [pos.id], {'sale_id': sale_id, 'ordered': True}, context=context)
                self.pool.get('sale.pos.line').create(cr, uid, {'order_id': sale_id, 'pos_id': str(pos.id), 'date': pos.date,  
                                                'branch_id': pos.branch_id.id, 'discount': float(pos.discount), 
                                                'cash_amount': float(pos.cash_amount), 'card_amount': float(pos.card_amount),
                                                'prepaid_amount': float(pos.prepaid_amount)}, context=context)

        if order_ids:
            for so in order_ids:
                if (so in olines.keys()): #and (not olines[so])
                    order_lines = olines[so]
                    for desc in order_lines.keys():
                        self._create_so_line(cr, uid, {'order_id': so, 'name': desc, 'amount': order_lines[desc]}, context=None)
                if so in discounts.keys():
                    order_obj.write(cr, uid, [so], {'discount_method': 'fixed', 'discount': discounts[so]}, context=context)
                order_obj.button_dummy(cr, uid, [so], context=context)
                order_obj.signal_workflow(cr, uid, [so], 'order_confirm')
            if oid:
                order_obj.message_post(cr, uid, order_ids, body=_("Quotes have been modified"), context=context)
        
        return order_ids

    def create_order(self, cr, uid, ids, context=None):
        active_ids = context and context.get('active_ids', [])
        data = self.browse(cr, uid, ids, context=context)[0]
        res = False
        
        if data.order_method=='merge' and data.order_id:
            res = self._create_orders(cr, uid, active_ids, data.order_id.id, context)
        else:
            res = self._create_orders(cr, uid, active_ids, None, context)

        if not res:
            raise osv.except_osv(_('Warning!'), _('Sales Order cannot be created for this data (POS) due to one of the following reasons The Data POS is Ordered!'))
        if context.get('open_orders', False):
            return self.open_orders(cr, uid, ids, res, context=context)
        return {'type': 'ir.actions.act_window_close'}

    def open_orders(self, cr, uid, ids, order_ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')

        result = mod_obj.get_object_reference(cr, uid, 'sale', 'action_orders')   #'view_order_tree'
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        #choose the view_mode accordingly
        if len(order_ids)>1:
            result['domain'] = "[('id','in',["+','.join(map(str, order_ids))+"])]"
        else:
            res = mod_obj.get_object_reference(cr, uid, 'sale', 'view_order_form')
            result['views'] = [(res and res[1] or False, 'form')]
            result['res_id'] = order_ids and order_ids[0] or False
        return result

pos_line_make_order()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
