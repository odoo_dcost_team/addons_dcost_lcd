import csv
import time
import base64
import tempfile
from datetime import date, datetime, timedelta
import cStringIO
from dateutil import parser
from openerp.osv import fields, osv




class account_analytic_account(osv.osv):
    _inherit = 'account.analytic.account'
    _columns = {
                'active': fields.boolean('Active'),
    }
    
    _defaults = {'active': 1}


class update_analytic(osv.osv_memory):
    _name = "update.analytic"
    _columns = {
                'code': fields.char('New Code', 6),
                'name' : fields.many2one('res.partner', 'Partner', domain=[('customer','=',True)]),
                'analytic_id' : fields.many2one('account.analytic.account', 'CoAA', domain=[('parent_id','=',False)], required=True),
    }


    
    def revisi_code(self, cr, uid, ids, context=None):       
        val = self.browse(cr, uid, ids)[0]
        
        analytic_obj = self.pool.get('account.analytic.account')
        analytic_ids = self._get_children_and_consol(cr, uid, [val.analytic_id.id])
        if val.code:
            data = analytic_obj.browse(cr, uid, analytic_ids)
            for x in data:
                a = x.code.split('-')
                analytic_obj.write(cr, uid, x.id, {'code': val.code +'-'+ a[1]})
        
            
        return True

    
    def revisi(self, cr, uid, ids, context=None):       
        val = self.browse(cr, uid, ids)[0]
        
        analytic_obj = self.pool.get('account.analytic.account')
        analytic_ids = self._get_children_and_consol(cr, uid, [val.analytic_id.id])
        if val.name:
            analytic_obj.write(cr, uid, analytic_ids, {'partner_id': val.name.id})
        else:
            analytic_obj.write(cr, uid, analytic_ids, {'active': False})
            
        return True

    def _get_children_and_consol(self, cr, uid, ids, context=None):
        analytic_obj = self.pool.get('account.analytic.account')
        ids2 = analytic_obj.search(cr, uid, [('parent_id', 'child_of', ids)], context=context)
        ids3 = []
        for rec in analytic_obj.browse(cr, uid, ids2, context=context):
            for child in rec.child_ids:
                ids3.append(child.id)
             
        return ids2
  



class EksportImport(osv.osv_memory):
    _name = "eksport.import"
    _columns = {
                'type': fields.selection((('eks','Export'), ('imp','Import')), 'Type'),
                'name': fields.char('File Name', 16),
                'tabel' : fields.many2one('ir.model', 'Object Model', required=True),
                'data_file': fields.binary('File'),
    }
       
    _defaults = {'type' :'eks'}
    
    
    def eksport_excel(self, cr, uid, ids, context=None):
                    
        val = self.browse(cr, uid, ids)[0]
           
        idd = self.pool.get('product.product').search(cr, uid, [])
        data = self.pool.get('product.product').browse(cr, uid, idd)
        
#         result = ';'.join(data[0].keys())   
#         value = [d.values() for d in data]
        
        result = 'id;code;name;uom;category;price;cost method'
        
        for x in data :
            result += '\n' + ';'.join([str(x.id), str(x.code), str(x.name), str(x.uom_id.name), str(x.categ_id.name), str(x.standard_price), x.cost_method]) 
                                  
#         for v in value:
#             for x in v:
#                 if isinstance(x, tuple):
#                     v[v.index(x)] = x[0]
                              
#         for row in value:
#             result += '\n' + ';'.join([str(v) for v in row]) 
                                  
        out = base64.encodestring(result)
        self.write(cr, uid, ids, {'data_file':out, 'name':'eksport.csv'}, context=context)
                              
        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_eksport_import', 'view_wizard_eksport_import')
        view_id = view_rec[1] or False
                          
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'eksport.import',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

             
    def import_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        if not val.data_file:
            raise osv.except_osv(_('Error'), _("Silahkan memilih file yang akan diimport !"))
        
        filename = val.name
        filedata = base64.b64decode(val.data_file)
        input = cStringIO.StringIO(filedata)
        input.seek(0)
 
        (fileno, fp_name) = tempfile.mkstemp('.csv', 'openerp_')
        file = open(fp_name, "w")
        file.write(filedata)
        file.close()
         
        crd = csv.reader(open(fp_name,"rb"))
        head = crd.next()[0].split(';')

        if val.tabel.model == 'mrp.bom':
            self.create_bom(cr, uid, crd)
            return {}
        if val.tabel.model == 'stock.inventory':
            self.opname(cr, uid, crd)
            return {}
        if val.tabel.model == 'stock.warehouse.orderpoint':
            self.buffer(cr, uid, crd)
            return {}

        for row in crd:
            res = {}
            for x in range (0, len(row[0].split(';'))):
                r = row[0].split(';')[x]
                if r.upper() == 'FALSE':
                    r = False
                elif r.upper() == 'TRUE':
                    r = True
                else:
                    try:
                        r = float(r)
                    except:
                        pass
                res[head[x]] = r
     
            self.pool.get(str(val.tabel.model)).create(cr, uid, res)


#             userid = self.pool.get(str(val.tabel.model)).create(cr, uid, res)
#             if str(val.tabel.model) == 'res.users':
#                 for u in self.pool.get('res.users').search(cr, uid, []) :
#                     self.pool.get('kelompok.user').create(cr, uid, {'name': u, 'res_user_id': userid})
 
        return {}                


    def buffer(self, cr, uid, data):
        val = []
        for row in data:
            lis = row[0].split(';')
            val.append(lis)
        
        for r in val:
            u = self.pool.get('stock.warehouse').browse(cr, uid, int(r[0]))
            self.pool.get("stock.warehouse.orderpoint").create(cr, int(r[5]), {
                                                                       'warehouse_id': u.id,
                                                                       'location_id': u.lot_stock_id.id,
                                                                       'product_id': int(r[1]),
                                                                       'qty_multiple': float(1.0),
                                                                       'product_min_qty': float(r[3]),
                                                                       'product_max_qty': float(r[4]), 
                                                               })

    def opname(self, cr, uid, data):
        product_obj = self.pool.get('product.product')
        inv = self.pool.get("stock.inventory").create(cr, uid, {'name': 'Opname Stock Inventory ' + time.strftime('%d-%m-%Y'), 'date': time.strftime('%Y-%m-%d')})
        for row in data:
            lis = row[0].split(';')
            product = product_obj.browse(cr, uid, int(lis[0]))
            self.pool.get("stock.inventory.line").create(cr, uid, {
                                                                   'inventory_id': inv,
                                                                   'product_id': int(lis[0]),
                                                                   'product_qty': float(lis[1]),
                                                                   'location_id': int(lis[2]),
                                                                   'product_uom': int(lis[3]), 
                                                                   #'prod_lot_id': int(lis[4]),
                                                                   'price_unit': product.standard_price,
                                                                   })
     
    def create_bom(self, cr, uid, data):
        no = []
        res = {}
        bom_obj = self.pool.get('mrp.bom')
        bom_line_obj = self.pool.get('mrp.bom.line')
        product_obj = self.pool.get('product.product') 
        for row in data:
            lis = row[0].split(';')
            
            if lis[1]:
                
                if float(lis[3]) < 0 :
                    raise osv.except_osv(('Perhatian'), ("Quantity product 0 %s !") % str(lis[0]))
                
                bom_id = bom_obj.create(cr, uid, {
                                                  'name': lis[1],
                                                  'product_tmpl_id': int(lis[5]), 
                                                  'product_qty': float(lis[3]),
                                                  'product_uom': int(lis[4])})
                res[lis[0]] = bom_id
                
            elif lis[2]:
                
                print lis 
                
                if float(lis[3]) < 0 :
                    raise osv.except_osv(('Perhatian'), ("Quantity product 0 %s !") % str(lis[0]))
                
                v = bom_obj.browse(cr, uid, res[lis[0]])
                if v.product_tmpl_id.id == int(lis[5]) :
                    raise osv.except_osv(('Perhatian'), ("Product sama %s !") % str(lis[0]))
                
                bom_line_obj.create(cr, uid, {
                                          'bom_id': res[lis[0]],
                                          'product_id': int(lis[5]), 
                                          'product_qty': float(lis[3]),
                                          'product_uom': int(lis[4])})




#             self.pool.get(val.tabel.model).write(cr, uid, [int(res['id'])], {'pos_old_id': int(res['pos_old_id'])})


#         idd = self.pool.get(val.tabel.model).search(cr, uid, [])
#         self.pool.get(val.tabel.model).write(cr, uid, idd, {'company_id': False}, context=context)



#         for x in obj_users.search(cr, uid, []) :
#             self.pool.get('kelompok.user').create(cr, uid, {'name': x, 'res_user_id': x})
#               
#         for x in range(92, 179):
#             self.pool.get('kelompok.user').create(cr, uid, {'name': x-87, 'res_user_id': x})
#   
#         for u in range(179,238):
#             for e in range(5, 179):
#                 self.pool.get('kelompok.user').create(cr, uid, {'name': e, 'res_user_id': u})
#      
#         idu = obj_users.search(cr, uid, [])
#         obj_users.write(cr, uid, idu, {'in_group_15': True}) 
#             
#         idd = obj_journal.search(cr, uid, [('type','=', 'cash')])
#         obj_journal.write(cr, uid, idd, {'journal_user': True})
#         idj = obj_journal.search(cr, uid, [])
#         obj_journal.write(cr, uid, idj, {'update_posted': True, 'with_last_closing_balance': False})
#         cid = obj_cashbox.search(cr, uid, [])
#         obj_cashbox.unlink(cr, uid, cid)   
#     
#         idc = obj_partner.search(cr, uid, [])
#         for x in obj_partner.browse(cr, uid, idc) :
#             if x.phone:
#                 if x.phone.find('.') and x.phone.find('.')  > 0:
#                     obj_partner.write(cr, uid, [x.id], {'phone': x.phone[:-2]})
#             if x.mobile:
#                 if x.mobile.find('.') and x.mobile.find('.')  > 0:
#                     obj_partner.write(cr, uid, [x.id], {'mobile': x.mobile[:-2]})
#             if x.fax:
#                 if x.fax.find('.') and x.fax.find('.')  > 0:
#                     obj_partner.write(cr, uid, [x.id], {'fax': x.fax[:-2]})
#                         
#         pid = obj_product.search(cr, uid, [])
#         for x in obj_product.browse(cr, uid, pid) :
#             if x.default_code.find('.') and x.default_code.find('.')  > 0:
#                 obj_product.write(cr, uid, [x.id], {'default_code': x.default_code[:-2]})
#    
#         posid = obj_pos.search(cr, uid, [])
#         for x in obj_pos.browse(cr, uid, posid) : 
#             obj_pos.write(cr, uid, [x.id], {'journal_ids': [(6,0, [7])]})  



### UNTUK CEK BOM ###


#         idd = self.pool.get(val.tabel.model).search(cr, uid, [])
#         data = self.pool.get(val.tabel.model).browse(cr, uid, idd)
#         
#         mrp = []
#         obj_mrp = self.pool.get('mrp.production')
#         for x in data:
#             hbom = obj_mrp.create(cr, uid, {
#                                             'product_id': x.product_tmpl_id.id,
#                                             'product_qty': x.product_qty,
#                                             'product_uom': x.product_uom.id,
#                                             'bom_id': x.id,
#                                             'location_src_id': 12,
#                                             'location_dest_id': 12,
#             })
#             mrp.append(hbom)
#                             
# 
#         for m in obj_mrp.browse(cr, uid, mrp):
#             try:
#                 obj_mrp.signal_workflow(cr, uid, [m.id], 'button_confirm')
#                 obj_mrp.force_production(cr, uid, [m.id])
#                 obj_mrp.action_produce(cr, uid, m.id, m.product_qty, 'consume_produce', wiz=False, context=context)
#             except  Exception, e :
#                 print '>>>', m.name, m.product_id.partner_ref
#                 pass




### TIDAK DIPAKE ###


#         for row in crd:
#             res = {}
#             for x in range (0, len(row[0].split(';'))):
#                 r = row[0].split(';')[x]
#                 res[head[x]] = r       
#             self.pool.get('pos.config').write(cr, uid, [int(res['id'])], {'kode_cabang': res['kode_cabang']}) 

#         for row in crd:
#             res = {}
#             for x in range (0, len(row[0].split(';'))):
#                 r = row[0].split(';')[x]
#                 res[head[x]] = r       
#             obj_product.write(cr, uid, [int(res['id'])], {'pos_old_id': int(res['pos_old_id'])})        



#         idd = self.pool.get('account.journal').search(cr, uid, [])
#         data = self.pool.get('account.journal').browse(cr, uid, idd)
#         
#         for x in data :
#             self.pool.get('account.journal').write(cr, uid, [x.id], {'name': x.name + ' ' + x.company_id.company_registry}, context=context)