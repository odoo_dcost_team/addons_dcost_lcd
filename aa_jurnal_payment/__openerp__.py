{
    "name":"Jurnal Payment",
    "version":"0.1",
    "author":"Muhammad Aziz - 087881071515",
    "category":"Custom Modules",
    "description": """
        To handle payment outlet.
    """,
    "depends":["base", "account", "account_voucher"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":["payment_view.xml"],
    "active":False,
    "installable":True
}
