import csv
import time
import base64
import tempfile
from datetime import date, datetime, timedelta
import cStringIO
from dateutil import parser
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp


class account_journal(osv.osv):
    _inherit = 'account.journal'
    _columns = {
        'account_perantara_id': fields.many2one('account.account', 'Account Perantara', domain=[('type', 'not in', ('payable', 'receivable', 'view'))]),
        'account_perantara_id2': fields.many2one('account.account', 'Account Pendapatan Cash', domain=[('type', 'not in', ('payable', 'receivable', 'view'))]),
        'analytic_perantara_id': fields.many2one('account.analytic.account', 'Analytic Perantara', domain=[('type', '=', 'normal')]),
    }


class account_voucher(osv.osv):
    _inherit = 'account.voucher'


    def action_move_line_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        move_pool = self.pool.get('account.move')
        move_line_pool = self.pool.get('account.move.line')
        for voucher in self.browse(cr, uid, ids, context=context):
            local_context = dict(context, force_company=voucher.journal_id.company_id.id)
            if voucher.move_id:
                continue
            company_currency = self._get_company_currency(cr, uid, voucher.id, context)
            current_currency = self._get_current_currency(cr, uid, voucher.id, context)
            context = self._sel_context(cr, uid, voucher.id, context)
            ctx = context.copy()
            ctx.update({'date': voucher.date})

            move_id = move_pool.create(cr, uid, self.account_move_get(cr, uid, voucher.id, context=context), context=context)
            name = move_pool.browse(cr, uid, move_id, context=context).name

            move_line_id = move_line_pool.create(cr, uid, self.first_move_line_get(cr,uid,voucher.id, move_id, company_currency, current_currency, local_context), local_context)
            move_line_brw = move_line_pool.browse(cr, uid, move_line_id, context=context)

            ###### ADD ACC PERANTARA ######

            if voucher.journal_id.account_perantara_id:
                analytic = False
                if voucher.journal_id.analytic_perantara_id :
                    analytic = voucher.journal_id.analytic_perantara_id.id
                import pdb;pdb.set_trace()
                debid = move_line_pool.create(cr,uid, {
                                                'name': '/',
                                                'partner_id': move_line_brw.partner_id.id,
                                                'account_id': voucher.journal_id.account_perantara_id.id,
                                                'date_maturity': move_line_brw.date_maturity,
                                                'move_id': move_id,
                                                'debit': move_line_brw.debit,
                                                'credit': 0})

                move_line_pool.create(cr,uid, {
                                                'name': '/',
                                                'partner_id': move_line_brw.partner_id.id,
                                                'account_id': voucher.journal_id.account_perantara_id2.id,
                                                'date_maturity': move_line_brw.date_maturity,
                                                'move_id': move_id,
                                                'debit': 0,
                                                'credit': move_line_brw.debit})

                #move_line_pool.write(cr, uid, [move_line_brw.id], {'analytic_account_id': analytic})
                move_line_pool.write(cr, uid, [debid], {'analytic_account_id': analytic})

            #################################

            line_total = move_line_brw.debit - move_line_brw.credit
            rec_list_ids = []

            if voucher.type == 'sale':
                line_total = line_total - self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)
            elif voucher.type == 'purchase':
                line_total = line_total + self._convert_amount(cr, uid, voucher.tax_amount, voucher.id, context=ctx)

            line_total, rec_list_ids = self.voucher_move_line_create(cr, uid, voucher.id, line_total, move_id, company_currency, current_currency, context)

            # Create the writeoff line if needed
            ml_writeoff = self.writeoff_move_line_get(cr, uid, voucher.id, line_total, move_id, name, company_currency, current_currency, local_context)
            if ml_writeoff:
                move_line_pool.create(cr, uid, ml_writeoff, local_context)
            # We post the voucher.
            self.write(cr, uid, [voucher.id], {
                'move_id': move_id,
                'state': 'posted',
                'number': name,
            })
            if voucher.journal_id.entry_posted:
                move_pool.post(cr, uid, [move_id], context={})
            # We automatically reconcile the account move lines.
            reconcile = False
            for rec_ids in rec_list_ids:
                if len(rec_ids) >= 2:
                    reconcile = move_line_pool.reconcile_partial(cr, uid, rec_ids, writeoff_acc_id=voucher.writeoff_acc_id.id, writeoff_period_id=voucher.period_id.id, writeoff_journal_id=voucher.journal_id.id)
        return True


