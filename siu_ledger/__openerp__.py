{
    "name":"Accounting Report",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "category":"Custom Modules",
    "description": """
        To provide accounting legal report.
    """,
    "depends":["base", "account", "analytic"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":["ledger_view.xml"],#"cron_view.xml"],
    "active":False,
    "installable":True
}
