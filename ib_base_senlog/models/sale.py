# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
import time
import openerp
import xmlrpclib
import threading
from openerp import models, api, fields as Fields

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp import pooler, SUPERUSER_ID
from openerp.exceptions import except_orm, Warning
from psycopg2 import OperationalError

db_groceries = ['LOGISTIK','SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT']
db_outlets = ['PB1', 'PB', 'OUTLET', 'PEBO']

class sale_order(osv.osv):
    _inherit = "sale.order"

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        company_id = usr_obj._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)], context=context)
        user = usr_obj.browse(cr, uid, uid, context=context)
        warehouse_id = False
        if user.warehouse_id:
            warehouse_id = user.warehouse_id.id
        elif warehouse_ids:
            warehouse_id = warehouse_ids[0]
        return warehouse_id

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse', required=True, track_visibility='onchange'),
        'state': fields.selection([
            ('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),
            ('cancel', 'Cancelled'),
            ('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),
            ('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),
            ('done', 'Done'),
        ], 'Status', readonly=True, copy=False, help="Gives the status of the quotation or sales order.\
                  \nThe exception status is automatically set when a cancel operation occurs \
                  in the invoice validation (Invoice Exception) or in the picking list process (Shipping Exception).\nThe 'Waiting Schedule' status is set when the invoice is confirmed\
                   but waiting for the scheduler to run on the order date.", select=True, track_visibility='always'),
        'validity_date': fields.date('Delivery Date', track_visibility='onchange'),
    }

    _defaults = {
        'warehouse_id': _get_warehouse,
    }

    def onchange_wh_id(self, cr, uid, ids, warehouse_id, context=None):
        val = {}
        if warehouse_id:
            user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
            warehouse = self.pool.get('stock.warehouse').browse(cr, uid, warehouse_id, context=context)
            if warehouse.company_id:
                val['company_id'] = warehouse.company_id.id
            if user.warehouse_id and (warehouse.id != user.warehouse_id.id):
                val['warehouse_id'] = user.warehouse_id.id

        return {'value': val}

    def action_sent_to_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'})

    def button_set_to_draft(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.signal_workflow(cr, uid, ids, 'quo_sent_to_draft')
        return True

    def run_duplicate(self, cr, uid, ids, autocommit=False, context=None):
        order_ids = []
        for order in self.browse(cr, uid, ids, context=context):
            # user = self.pool.get('res.users').search(cr, uid, [('login', 'like', 'adm%'), ('login', 'not like', '%admin%'),
            #         ('warehouse_id', '=', omzet.name and omzet.name.id)], limit=1, order='login')[0] or SUPERUSER_ID
            if (not order.purchase_order) and (not order.related_docs): #Create new invoice
                if (not order.order_line):
                    raise except_orm(_('Peringatan !!!'),
                                     _("Tidak ada produk didalam Sales Order Logistik. Silahkan cek kembali..."))
                else:
                    order_ids.append(order.id)
        if order_ids:
            try:
                self.pool.get('duplicate.sale.order').duplication_sale_to_purchase(cr, uid, order_ids)
                # if result_ids:
                #     self.write(cr, SUPERUSER_ID, [order.id], {'invoiced': True}, context=context)
            except OperationalError:
                if autocommit:
                    cr.rollback()
                    # continue
                else:
                    raise
        return True

    ### Run Scheduler - Duplicate SO to PO ###
    def run_scheduler_duplicate(self, cr, uid, use_new_cursor=False, date_start=False, date_stop=False, context=None):
        if context is None:
            context = {}
        try:
            if use_new_cursor:
                cr = openerp.registry(cr.dbname).cursor()
            # Run SO confirmed dan belum di duplikasi ke PB1 (outlet)
            dom = [('state','not in',('draft','cancel','sent')),('purchase_order','=',False)]
            if date_start and date_stop:
                dom += [('date_order','>=',date_start),('date_order','<=',date_stop)]
            else:
                dom += [('date_order','>=',time.strftime('%Y-01-01 01:00:00'))]  #'%Y-%m-%d'  #time.strftime('%Y-01-01')
                #dom += [('date', '>=',time.strftime('%Y-01-01')),('date', '<=',time.strftime('%Y-10-31'))]
            prev_ids = []
            while True:
                ids = self.search(cr, SUPERUSER_ID, dom, context=context)
                if not ids or prev_ids == ids:
                    break
                else:
                    prev_ids = ids
                self.run_duplicate(cr, SUPERUSER_ID, ids, autocommit=use_new_cursor, context=context)
                if use_new_cursor:
                    cr.commit()
        finally:
            if use_new_cursor:
                try:
                    cr.close()
                except Exception:
                    pass
        return {}




class sale_order_line(osv.osv):
    _inherit = "sale.order.line"

    def product_id_change(self, cr, uid, ids, pricelist, product, qty=0,
            uom=False, qty_uos=0, uos=False, name='', partner_id=False,
            lang=False, update_tax=True, date_order=False, packaging=False, fiscal_position=False, flag=False, context=None):
        context = context or {}
        lang = lang or context.get('lang', False)
        if not partner_id:
            raise osv.except_osv(_('No Customer Defined!'), _('Before choosing a product,\n select a customer in the sales form.'))
        warning = False
        product_uom_obj = self.pool.get('product.uom')
        partner_obj = self.pool.get('res.partner')
        product_obj = self.pool.get('product.product')
        partner = partner_obj.browse(cr, uid, partner_id)
        lang = partner.lang
        context_partner = context.copy()
        context_partner.update({'lang': lang, 'partner_id': partner_id})

        if not product:
            return {'value': {'th_weight': 0,
                'product_uos_qty': qty}, 'domain': {'product_uom': [],
                   'product_uos': []}}
        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        result = {}
        warning_msgs = ''
        product_obj = product_obj.browse(cr, uid, product, context=context_partner)

        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(cr, uid, uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(cr, uid, uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False

        fpos = False
        if not fiscal_position:
            fpos = partner.property_account_position or False
        else:
            fpos = self.pool.get('account.fiscal.position').browse(cr, uid, fiscal_position)

        if uid == SUPERUSER_ID and context.get('company_id'):
            taxes = product_obj.taxes_id.filtered(lambda r: r.company_id.id == context['company_id'])
        else:
            taxes = product_obj.taxes_id
        result['tax_id'] = self.pool.get('account.fiscal.position').map_tax(cr, uid, fpos, taxes, context=context)

        if not flag:
            result['name'] = self.pool.get('product.product').name_get(cr, uid, [product_obj.id], context=context_partner)[0][1]
            if product_obj.description_sale:
                result['name'] += '\n'+product_obj.description_sale
        domain = {}
        if (not uom) and (not uos):
            result['product_uom'] = product_obj.uom_id.id
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
                uos_category_id = product_obj.uos_id.category_id.id
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
                uos_category_id = False
            result['th_weight'] = qty * product_obj.weight
            domain = {'product_uom':
                        [('category_id', '=', product_obj.uom_id.category_id.id)],
                        'product_uos':
                        [('category_id', '=', uos_category_id)]}
        elif uos and not uom: # only happens if uom is False
            result['product_uom'] = product_obj.uom_id and product_obj.uom_id.id
            result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
            result['th_weight'] = result['product_uom_qty'] * product_obj.weight
        elif uom: # whether uos is set or not
            default_uom = product_obj.uom_id and product_obj.uom_id.id
            q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
            result['th_weight'] = q * product_obj.weight        # Round the quantity up

        if not uom2:
            uom2 = product_obj.uom_id
        if product_obj.requirement_type:
            result['requirement_type'] = product_obj.requirement_type

        if not pricelist:
            warn_msg = _('You have to select a pricelist or a customer in the sales form !\n'
                    'Please set one before choosing a product.')
            warning_msgs += _("No Pricelist ! : ") + warn_msg +"\n\n"
        else:
            ctx = dict(
                context,
                uom=uom or result.get('product_uom'),
                date=date_order,
            )
            price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                    product, qty or 1.0, partner_id, ctx)[pricelist]
            if price is False:
                warn_msg = _("Cannot find a pricelist line matching this product and quantity.\n"
                        "You have to change either the product, the quantity or the pricelist.")

                warning_msgs += _("No valid pricelist line found ! :") + warn_msg +"\n\n"
            else:
                price = self.pool['account.tax']._fix_tax_included_price(cr, uid, price, taxes, result['tax_id'])
                result.update({'price_unit': price})
                if context.get('uom_qty_change', False):
                    values = {'price_unit': price}
                    if result.get('product_uos_qty'):
                        values['product_uos_qty'] = result['product_uos_qty']
                    return {'value': values, 'domain': {}, 'warning': False}
        if warning_msgs:
            warning = {
                       'title': _('Configuration Error!'),
                       'message' : warning_msgs
                    }
        return {'value': result, 'domain': domain, 'warning': warning}



class account_invoice(osv.osv):
    _inherit = "account.invoice"

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        company_id = usr_obj._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)], context=context)
        user = usr_obj.browse(cr, uid, uid, context=context)
        warehouse_id = False
        if user.warehouse_id:
            warehouse_id = user.warehouse_id.id
        elif warehouse_ids:
            warehouse_id = warehouse_ids[0]
        return warehouse_id

    _columns = {
        'warehouse_id': fields.many2one('stock.warehouse', 'Warehouse'),
    }

    _defaults = {
        'warehouse_id': _get_warehouse,
    }




class RPCProxyOne(object):
    def __init__(self, server, ressource, user_id):
        self.server = server
        self.user_id = user_id
        local_url = 'http://%s:%d/xmlrpc/common' % (server.server_url,
                                                    server.server_port)
        rpc = xmlrpclib.ServerProxy(local_url)
        self.uid = rpc.login(server.server_db, server.login, server.password)
        local_url = 'http://%s:%d/xmlrpc/object' % (server.server_url,
                                                    server.server_port)
        self.rpc = xmlrpclib.ServerProxy(local_url)
        self.ressource = ressource

    def __getattr__(self, name):
        RPCProxy(self.server, self.user_id)
        server_db = self.server.server_db
        server_pass = self.server.password
        resource = self.ressource
        return lambda cr, uid, *args, **kwargs: self.rpc.execute(server_db,
                                                                 self.user_id or self.uid,
                                                                 server_pass,
                                                                 resource,
                                                                 name, *args)


class RPCProxy(object):
    def __init__(self, server, user):
        self.server = server
        self.user = user

    def get(self, ressource):
        return RPCProxyOne(self.server, ressource, self.user)



class DuplicateSalesOrder(models.Model):
    _name = 'duplicate.sale.order'

    report = []
    report_total = 0
    report_create = 0
    report_write = 0

    @api.model
    def input(self, ids, value):
        return value

    @api.model
    def duplication_sale_to_purchase(self, order_ids):
        server_id = self.env['duplicate.config.server'].search([('server_db', 'in', tuple(db_outlets))], limit=1).id
        server = self.env['duplicate.config.server'].browse(server_id)
        context = dict(self._context or {})
        #active_ids = self._context.get('active_ids', [])
        pool = pooler.get_pool(self.env.cr.dbname)
        uid = SUPERUSER_ID  #self.user_id.id or

        # self.meta = {}
        purchases = {}
        pool_src = pool
        pool_dest = RPCProxy(server, SUPERUSER_ID)

        sale_obj = pool_src.get('sale.order')
        users_obj = pool_dest.get('res.users')
        # sales = sale_obj.browse(self._cr, SUPERUSER_ID, order_ids or [])[0]  # active_ids
        #pool_dest = RPCProxy(server, real_uid)

        purchase_obj = pool_dest.get('purchase.order')
        purchase_line_obj = pool_dest.get('purchase.order.line')
        history_src_obj = pool_src.get('duplicate.history')
        partner_obj = pool_dest.get('res.partner')
        warehouse_obj = pool_dest.get('stock.warehouse')
        sequence_obj = pool_dest.get('ir.sequence')
        pick_type_obj = pool_dest.get('stock.picking.type')
        history_obj = pool_dest.get('duplicate.history')
        product_obj = pool_dest.get('product.product')
        so_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
        so_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])
        po_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order')])
        po_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])

        for sale in sale_obj.browse(self._cr, SUPERUSER_ID, order_ids or []): #active_ids
            if sale.partner_id and sale.partner_id.code:
                key = str('adm') + str(sale.partner_id.code.strip().lower())
            elif self.env.user.login:
                key = str(self.env.user.login).lower()
            else:
                key = 'admin'
            real_uid = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)[0] or uid
            exist_oid = self.get_id(so_model_ids and so_model_ids[0], sale.id) #check from history of duplication
            if exist_oid:
                purchases = purchase_obj.read(self._cr, real_uid, [exist_oid], [])[0]
                if (exist_oid and purchases['state'] not in ('draft')):  #sale.purchase_order or ,'sent','bid'
                    raise except_orm(_('Warning!!!'),
                                     _("Purchase Order (Outlet) berstatus '%s' dan tidak bisa diduplikasi. Status yang diperbolehkan yaitu draft, sent, bid.") % purchases['state'])
            partner_ids = partner_obj.search(self._cr, real_uid, ['&','|',('supplier', '=', True),
                                ('name','like','%'+str(sale.warehouse_id.partner_id.name).strip().replace('DCOST ','')),
                                ('name','like','%'+str(sale.warehouse_id.name).strip().replace('PB',''))])
            if not partner_ids:
                raise except_orm(_('Warning!'), _("Partner name (Supplier) tidak tersedia di Database PB1."))
            else:
                wh_ids = warehouse_obj.search(self._cr, real_uid, [('code','!=','')])
                warehouses = warehouse_obj.read(self._cr, real_uid, wh_ids, context=context)  #[0]
                outlet_codes = [w['code'] for w in warehouses]
                if sale.partner_id.code and (sale.partner_id.code in outlet_codes):
                    # if (not sale.partner_id.code) or (sale.partner_id.code not in outlet_codes):
                    #     raise except_orm(_('Warning!'), _("Partner code [Kode Customer SO] are not available in DB PB1 [Warehouse Code]. - result: %s") % (sale.partner_id.code,))
                    warehouse_ids = warehouse_obj.search(self._cr, real_uid,
                                    [('code', 'like', '%' + sale.partner_id.code + '%')], limit=1) or []
                    pick_type_ids = pick_type_obj.search(self._cr, real_uid,
                                    [('warehouse_id', '=', warehouse_ids and warehouse_ids[0] or False)], limit=1)
                    picking_type = pick_type_obj.read(self._cr, real_uid, pick_type_ids, context=context)[0]
                    supplier = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
                    values = self._prepare_purchase_order(sale, supplier, pick_type_ids and pick_type_ids[0] or False,
                                                          picking_type['default_location_dest_id'], real_uid)

                    if exist_oid:
                        purchase_id = exist_oid
                        po_number = purchases['name']
                        if purchases['state'] == 'draft':
                            sol_ids = []
                            purchase_obj.write(self._cr, real_uid, [purchase_id], values)
                            for line in sale.order_line:
                                product_id = self.get_product_details(pool_dest, line, 'product_id')
                                product_description = self.get_product_details(pool_dest, line, 'description')
                                uom_id = self.get_uom_id(pool_dest, line)
                                exist_lid = self.get_id(so_line_model_ids and so_line_model_ids[0], line.id)
                                #product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
                                account_id = self.get_product_details(pool_dest, line, 'account_id')
                                analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
                                line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id, product_description, analytic_id)
                                if not uom_id:
                                    raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1."))
                                if exist_lid and product_id and uom_id:
                                    dest_oline = purchase_line_obj.read(self._cr, uid, [exist_lid], [])[0]
                                    if dest_oline['state'] == 'draft':
                                        purchase_line_obj.write(self._cr, real_uid, [exist_lid], line_vals)
                                else:
                                    add_pur_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
                                    self.create_duplicate_history(pool_src, pool_dest, so_line_model_ids and so_line_model_ids[0]
                                        or False, line.id or False, po_line_model_ids and po_line_model_ids[0] or False,
                                        add_pur_line_id or False, uid, real_uid)
                                if line.id not in sol_ids:
                                    sol_ids.append(line.id)

                            if sol_ids:
                                pol_ids = purchase_line_obj.search(self._cr, real_uid, [('order_id', '=', purchase_id)])
                                history_dst_ids = history_obj.search(self._cr, real_uid,
                                            [('source_id','in',tuple(pol_ids)),('destination_id','not in',tuple(sol_ids))])
                                history_src_ids = history_src_obj.search(self._cr, uid,
                                            [('source_id', 'not in', tuple(sol_ids)),('destination_id', 'in', tuple(pol_ids))])
                                if history_dst_ids:
                                    for res in history_obj.read(self._cr, real_uid, history_dst_ids, ['source_id']):
                                        purchase_line_obj.unlink(self._cr, real_uid, [res['source_id']])
                                if history_src_ids or history_dst_ids:
                                    self.remove_duplicate_history(pool_dest, pool_src, history_dst_ids, history_src_ids, real_uid, uid)
                            purchase_obj.message_post(self._cr, real_uid, [purchase_id], body=_("Purchase Order has been changed"))
                            self.report_total += 1
                            self.report_write += 1
                        else:
                            raise except_orm(_('Warning!'), _("Data can not be changed or deleted because it is not a draft."))
                    else: #create: purchase_order outlet
                        purchase_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'purchase.order.dcost', context=context)
                        nomor_po = list(str(purchase_order_number))  #PO/12345 -> PO/sunter/12345
                        nomor_po.insert(3, str(sale.partner_id.code)+ "/")
                        po_number = ''.join(nomor_po)
                        values.update({'name': str(po_number)})
                        print values
                        purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
                        # new_lines = []
                        for line in sale.order_line:
                            product_id = self.get_product_details(pool_dest, line, 'product_id')
                            product_description = self.get_product_details(pool_dest, line, 'description')
                            uom_id = self.get_uom_id(pool_dest, line)
                            # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
                            account_id = self.get_product_details(pool_dest, line, 'account_id')
                            analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
                            if not uom_id:
                                raise except_orm(_('Warning!'), _("Product UoM are not available in the PB1 Database."))
                            # if not product_id:
                            #     raise except_orm(_('Warning!'), _("Product (ID) are not available in the PB1 Database."))
                            if product_id and uom_id:
                                line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id, product_description, analytic_id)
                                line_vals.update({'price_unit': line.price_unit})
                                print line_vals
                                purchases_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
                                # new_lines.append((0, 0, line_vals))
                                self.create_duplicate_history(pool_src, pool_dest, so_line_model_ids and so_line_model_ids[0]
                                        or False, line.id or False, po_line_model_ids and po_line_model_ids[0] or False,
                                        purchases_line_id or False, uid, real_uid)
                        # if len(new_lines) == len(sale.order_line):
                        #     values.update({'order_line': new_lines})
                        #     purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
                        purchase_obj.message_post(self._cr, real_uid, [purchase_id],
                                body=_("Purchase Order created"), context={'mail_create_nolog': True})
                        self.create_duplicate_history(pool_src, pool_dest, so_model_ids and so_model_ids[0] or False,
                                sale.id or False, po_model_ids and po_model_ids[0] or False, purchase_id or False, uid, real_uid)
                        self.report_total += 1
                        self.report_create += 1

                    purchase_obj.button_dummy(self._cr, uid, [purchase_id])
                    sale_obj.write(self._cr, uid, [sale.id], {'purchase_order': True, 'related_docs': po_number or ''})
                    # self.meta = {}
        return True

    # @api.model
    # def duplication_sale_to_purchase(self, order_ids):
    #     server_id = self.env['duplicate.config.server'].search([('server_db', 'in', tuple(db_outlets))], limit=1).id
    #     server = self.env['duplicate.config.server'].browse(server_id)
    #     context = dict(self._context or {})
    #     #active_ids = self._context.get('active_ids', [])
    #     pool = pooler.get_pool(self.env.cr.dbname)
    #     uid = SUPERUSER_ID  #self.user_id.id or
    #
    #     self.meta = {}
    #     purchases = {}
    #     pool_src = pool
    #     pool_dest = RPCProxy(server, SUPERUSER_ID)
    #     purchase_order = {}
    #
    #     sale_obj = pool_src.get('sale.order')
    #     users_obj = pool_dest.get('res.users')
    #     sales = sale_obj.browse(self._cr, SUPERUSER_ID, order_ids or [])[0] #active_ids
    #     if sales.partner_id and sales.partner_id.code:
    #         key = str('adm') + str(sales.partner_id.code.strip().lower())
    #     elif self.env.user.login:
    #         key = str(self.env.user.login).lower()
    #     else:
    #         key = 'admin'
    #     user_ids = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)
    #     real_uid = users_obj.read(self._cr, SUPERUSER_ID, user_ids, context=context)[0]['id'] or uid
    #     #pool_dest = RPCProxy(server, real_uid)
    #
    #     purchase_obj = pool_dest.get('purchase.order')
    #     purchase_line_obj = pool_dest.get('purchase.order.line')
    #     history_src_obj = pool_src.get('duplicate.history')
    #     partner_obj = pool_dest.get('res.partner')
    #     warehouse_obj = pool_dest.get('stock.warehouse')
    #     sequence_obj = pool_dest.get('ir.sequence')
    #     pick_type_obj = pool_dest.get('stock.picking.type')
    #     history_obj = pool_dest.get('duplicate.history')
    #     product_obj = pool_dest.get('product.product')
    #     so_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
    #     so_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])
    #     po_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order')])
    #     po_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])
    #
    #     for sale in sale_obj.browse(self._cr, SUPERUSER_ID, order_ids or []): #active_ids
    #         # exist_oid = self.get_id(so_model_ids and so_model_ids[0], sale.id) #check from history of duplication
    #         exist_oid = purchase_obj.search(self._cr, real_uid, [('sale_id','=',sale.id),('state','!=','cancel')])
    #         if exist_oid:
    #             purchases = purchase_obj.read(self._cr, real_uid, exist_oid, [])[0]
    #             if (exist_oid and purchases['state'] not in ('draft')):  #sale.purchase_order or ,'sent','bid'
    #                 raise except_orm(_('Warning!!!'),
    #                                  _("Purchase Order (Outlet) berstatus '%s' dan tidak bisa diduplikasi. Status yang diperbolehkan yaitu draft, sent, bid.") % purchases['state'])
    #         partner_ids = partner_obj.search(self._cr, real_uid, ['&','|',('supplier', '=', True),
    #                             ('name','like','%'+str(sale.warehouse_id.partner_id.name).strip().replace('DCOST ','')),
    #                             ('name','like','%'+str(sale.warehouse_id.name).strip().replace('PB',''))])
    #         if not partner_ids:
    #             raise except_orm(_('Warning!'), _("Partner name (Supplier) tidak tersedia di Database PB1."))
    #         else:
    #             wh_ids = warehouse_obj.search(self._cr, real_uid, [('code','!=',''),('active','=',True)])
    #             warehouses = warehouse_obj.read(self._cr, real_uid, wh_ids, context=context)  #[0]
    #             outlet_codes = [w['code'] for w in warehouses]
    #             if (not sale.partner_id.code) or (sale.partner_id.code not in outlet_codes):
    #                 raise except_orm(_('Warning!'), _("Partner code [Kode Customer SO] are not available in DB PB1 [Warehouse Code]. \nresult: %s") % (sale.partner_id.code,))
    #             warehouse_ids = warehouse_obj.search(self._cr, real_uid,
    #                             [('code', 'like', '%' + sale.partner_id.code + '%')], limit=1) or []
    #             pick_type_ids = pick_type_obj.search(self._cr, real_uid,
    #                             [('warehouse_id', '=', warehouse_ids and warehouse_ids[0] or False)], limit=1)
    #             picking_type = pick_type_obj.read(self._cr, real_uid, pick_type_ids, context=context)[0]
    #             supplier = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
    #             values = self._prepare_purchase_order(sale, supplier, pick_type_ids and pick_type_ids[0] or False,
    #                                                   picking_type['default_location_dest_id'], real_uid)
    #             if exist_oid:
    #                 purchase_id = exist_oid and exist_oid[0] or purchases['id']
    #                 if purchases['state'] in ('draft', 'sent', 'bid'):
    #                     purchase_obj.write(self._cr, real_uid, [purchase_id], values)
    #                     for line in sale.order_line:
    #                         product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                         product_description = self.get_product_details(pool_dest, line, 'description')
    #                         uom_id = self.get_uom_id(pool_dest, line)
    #                         exist_pol_id = purchase_line_obj.search(self._cr, real_uid, [('sale_line_id', '=', line.id), ('state', '!=', 'cancel')])
    #                         # exist_lid = self.get_id(so_line_model_ids and so_line_model_ids[0], line.id)
    #                         account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                         analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
    #                         line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id, product_description, analytic_id)
    #                         if not product_id:
    #                             raise except_orm(_('Warning!'), _("Produk tidak tersedia di Database PB1."))
    #                         if not uom_id:
    #                             raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1."))
    #                         if exist_pol_id and product_id and uom_id and (line.write_date > line.create_date):
    #                             dest_poline = purchase_line_obj.read(self._cr, uid, exist_pol_id, [])[0]
    #                             if dest_poline['state'] in ('draft', 'sent'):
    #                                 purchase_line_obj.write(self._cr, real_uid, exist_pol_id, line_vals)
    #                         else:
    #                             if product_id and uom_id:
    #                                 purchase_line_obj.create(self._cr, real_uid, line_vals)
    #                 else:
    #                     raise except_orm(_('Warning!'), _("Data can not be changed or deleted because it is not a draft."))
    #             else: #create: purchase_order outlet
    #                 purchase_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'purchase.order.dcost', context=context)
    #                 nomor_po = list(str(purchase_order_number))  #PO/12345 -> PO/sunter/12345
    #                 nomor_po.insert(3, str(sale.partner_id.code)+ "/")
    #                 po_number = ''.join(nomor_po)
    #                 values.update({'name': str(po_number), 'sale_id': sale.id})
    #                 #purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
    #                 new_lines = []
    #                 for line in sale.order_line:
    #                     product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                     product_description = self.get_product_details(pool_dest, line, 'description')
    #                     uom_id = self.get_uom_id(pool_dest, line)
    #                     # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
    #                     account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                     analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
    #                     if not product_id:
    #                         raise except_orm(_('Warning!'), _("Produk tidak tersedia di Database PB1."))
    #                     if not uom_id:
    #                         raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1."))
    #                     if product_id and uom_id:
    #                         line_vals = self._prepare_purchase_order_line(line, False, product_id, uom_id, product_description, analytic_id)
    #                         new_lines.append((0, 0, line_vals))
    #
    #                 values.update({'order_line': new_lines})
    #                 if len(new_lines) == len(sale.order_line):
    #                     purchase_order[sale.id] = values
    #
    #     if purchase_order:
    #         for sale in sale_obj.browse(self._cr, SUPERUSER_ID, order_ids or []):
    #             if sale.id in purchase_order.keys():
    #                 purchase_id = purchase_obj.create(self._cr, real_uid, purchase_order[sale.id],
    #                                                   context={'mail_create_nolog': True})
    #                 purchase_obj.button_dummy(self._cr, real_uid, [purchase_id])
    #                 sale_obj.write(self._cr, uid, [sale.id], {'purchase_order': True, 'purchase_id': purchase_id,
    #                                                           'related_docs': purchase_order[sale.id]['name'] or ''})
    #                 # purchase_data = purchase_obj.read(self._cr, real_uid, [purchase_id], context=context)[0]
    #                 # user = users_obj.read(self._cr, real_uid, [purchase_data['user_id']], context=context)[0]
    #                 # if str(user.login)[3:] != str(sales.partner_id.code.strip().lower()):
    #                 #     purchase_obj.write(self._cr, real_uid, [purchase_id], {'user_id': real_uid})
    #         self.meta = {}
    #
    #     return True

    @api.model
    def get_product_details(self, pool_dest, line, var=''):
        product_obj = pool_dest.get('product.product')
        product_categ_obj = pool_dest.get('product.category')
        #('name_template', 'like', str(line.product_id.name_template).split(" ")[0] + '%')
        product_ids = product_obj.search(self._cr, SUPERUSER_ID,
                ['&',('name_template', 'like', '%' + str(line.product_id.name_template) + '%'),
                    ('default_code', 'like', str(line.product_id.default_code))],
                    limit=1)
        result = False
        if product_ids:
            product = product_obj.read(self._cr, SUPERUSER_ID, product_ids, ['product_tmpl_id','name_template','type','description_sale','property_account_expense','categ_id'])[0]
            if product['type'] == 'product':
                if var=='product_id':
                    result = product_ids and product_ids[0] or False    ##line.product_id.id
                elif var == 'description':
                    result = product['name_template'] or product['description_sale']
                elif var == 'account_id':
                    if product['property_account_expense']:
                        result = product['property_account_expense'][0]
                    elif product['categ_id']:
                        product_categ = product_categ_obj.read(self._cr, SUPERUSER_ID, [product['categ_id'][0]], [])[0]
                        if product_categ['property_account_expense_categ']:
                            result = product_categ['property_account_expense_categ'][0]
                        else:
                            result = False
                    else:
                        result = False
            else:
                result = False if var=='product_id' else ''
        else:
            result = False if var == 'product_id' else ''
        return result

    @api.model
    def get_uom_id(self, pool_dest, line):
        uom_obj = pool_dest.get('product.uom')
        uom_ids = uom_obj.search(self._cr, SUPERUSER_ID,
                [('name', 'like', '%' + str(line.product_uom.name) + '%'),
                 ('name', 'like', str(line.product_uom.name).split(" ")[0] + '%')],limit=1)
        if not uom_ids:
            raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1.\n'%s'") % (str(line.product_uom.name),))
        return uom_ids and uom_ids[0] or line.product_uom.id or False

    @api.model
    def get_id(self, object_id, rec_id):
        pool = pooler.get_pool(self.env.cr.dbname)
        history_pool = pool.get('duplicate.history')  #self.user_id.id
        rid = history_pool.search(self._cr, SUPERUSER_ID,
                               [('src_model_id', '=', object_id),
                                ('source_id', '=', rec_id)])
        result = False
        if rid:
            result = history_pool.read(self._cr, SUPERUSER_ID, rid,
                                    ['destination_id'])[0]['destination_id']  #self.user_id.id
        return result

    @api.model
    def remove_duplicate_history(self, pool_dest, pool_src, history_dst_ids, history_src_ids, real_uid, uid):
        pool_dest.get('duplicate.history').unlink(self._cr, real_uid, history_dst_ids)
        pool_src.get('duplicate.history').unlink(self._cr, uid, history_src_ids)
        return True

    @api.model
    def create_duplicate_history(self, pool_src, pool_dest, src_mdl_id, src_id, dest_mdl_id, dest_id, uid, real_uid):
        pool_src.get('duplicate.history').create(self._cr, uid, {
            'src_model_id': src_mdl_id,
            'source_id': src_id,
            'dest_model_id': dest_mdl_id,
            'destination_id': dest_id,
            'user_id': uid,
        })
        pool_dest.get('duplicate.history').create(self._cr, real_uid, {
            'src_model_id': dest_mdl_id,
            'source_id': dest_id,
            'dest_model_id': src_mdl_id,
            'destination_id': src_id,
            'user_id': real_uid,
        })
        return True

    def _prepare_purchase_order(self, data, supplier, pick_type_id, loc_dest_id, user_id):
        supplier_pricelist = supplier['property_product_pricelist_purchase']
        currency_id = data.currency_id and data.currency_id.id or data.company_id.currency_id.id
        return {
            #'origin': data.name,
            'date_order': data.validity_date or data.date_order or fields.datetime.now(),
            'partner_id': supplier['id'],
            'pricelist_id': supplier_pricelist[0] or 1,
            'currency_id': currency_id or 13,
            'location_id': loc_dest_id[0] or False,
            'company_id': data.company_id.id or 1,
            'fiscal_position': supplier['property_account_position'] or False,
            'related_docs': data.name,
            'notes': data.note,
            'picking_type_id': pick_type_id,
            'create_uid': user_id,
            'write_uid': user_id,
            'sales_order': True,
            'user_id': user_id,
            # 'order_line': [(0, 0, {})],
        }

    def _prepare_purchase_order_line(self, line, purchase_id, product_id, uom_id, description, analytic_id):
        return {
            'order_id': purchase_id,
            'name': description or line.name,
            'product_id': product_id or False,
            'product_qty': line.product_uom_qty,
            'product_uom': uom_id,
            'price_unit': line.price_unit or 1.0,
            'state': 'draft',
            'date_planned': line.order_id.validity_date or time.strftime('%Y-%m-%d, %H:%M:%S') or False,
            'requirement_type': line.requirement_type,
            'account_analytic_id': analytic_id or False,
            'sale_line_id': line.id or False,
        }

    @api.model
    def get_anaytic_id(self, pool_dest, account_id, warehouse_id):
        # analytic_obj = pool_dest.get('account.analytic.account')
        analytic_cbg_obj = pool_dest.get('analytic.cabang')
        analytic_cabang_ids = analytic_cbg_obj.search(self._cr, SUPERUSER_ID,
                                 [('name', '=', warehouse_id), ('account_id', '=', account_id)], limit=1)
        # analytic_cabang = analytic_cbg_obj.browse(self._cr, SUPERUSER_ID, analytic_cabang_ids)[0]
        analytic_cabang = analytic_cbg_obj.read(self._cr, SUPERUSER_ID, analytic_cabang_ids, [])[0]
        if not analytic_cabang_ids:
            raise except_orm(_('Warning!'), _("Mapping analytic cabang tidak tersedia di Database PB1."))
        return analytic_cabang_ids and analytic_cabang['analytic_id'][0] or False

#
# class product_template(osv.osv):
#     _inherit = 'product.template'
#
#     _columns = {
#         'max_stock': fields.float('Maximum Stock'),
#         'min_stock': fields.float('Minimum Stock'),
#     }




class stock_warehouse(osv.osv):
    _inherit = "stock.warehouse"
    _columns = {
        'outlet': fields.char(string='Outlet'),
        'outlet_id': fields.char('Outlet ID'),
    }



