{
    "name":"Mutasi Account Report",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://myabcerp.com",
    "category":"Custom Modules",
    "description": """
        The base module to generate excel report.
    """,
    "depends":["base", "account", "stock"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":["mutasi_view.xml"],
    "active":False,
    "installable":True
}
