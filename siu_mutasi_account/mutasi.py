import csv
import time
import base64
import tempfile
from datetime import date, datetime, timedelta
import cStringIO
from dateutil import parser
from openerp.osv import fields, osv



class mutasi_account(osv.osv):
    _name = "mutasi.account"
    _columns = {
                'name': fields.char('File Name', 16),
                'status': fields.selection([
                    ('all', 'All'),
                    ], 'Status', required=True),
                
                'company_id' : fields.many2one('res.company', 'Company', required=True),
                'fiscalyear_id' : fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
                
                'partner_id' : fields.many2one('res.partner', 'Partner'),
                
                'filter': fields.selection((('period','Period'), ('date','Date')), 'Filter by', required=True),
                
                'period_to' : fields.many2one('account.period', 'Period To'),
                'period_from' : fields.many2one('account.period', 'Period From'),
                
                'date_from' : fields.date('Date From', required=True),
                'date_to' : fields.date('Date To', required=True),
                'account_ids': fields.many2many('account.account','mutasi_rel', 'mutasi_id', 'account_id', 'Account', domain="[('type', '!=', 'view'), ('company_id', '=', company_id)]", required=True),
                'data_file': fields.binary('File'),
    }   
    
    _defaults = {
                 'status': 'all',
                 'fiscalyear_id' : 1,
                 'company_id' : 1,
                 'filter' : 'period',
                 'date_from': time.strftime('%Y-%m-%d'),
                 'date_to': time.strftime('%Y-%m-%d'),
    }
    
    def company_change(self, cr, uid, ids, company_id):
        return  {'value': {'fiscalyear_id': False, 'period_from': False, 'period_to': False}}  

    def eksport_excel(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
        
        hid = []; awal = []

        if val.partner_id:
            if val.filter == 'period':
                hid = obj_move.search(cr, uid, [('partner_id', '=', val.partner_id.id), ('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
                awal = obj_move.search(cr, uid, [('partner_id', '=', val.partner_id.id), ('period_id', '<', val.period_from.id)])
            elif val.filter == 'date':
                hid = obj_move.search(cr, uid, [('partner_id', '=', val.partner_id.id), ('date', '>=', val.date_from), ('date', '<=', val.date_to)])
                awal = obj_move.search(cr, uid, [('partner_id', '=', val.partner_id.id), ('date', '<', val.date_from)])
        else:
            if val.filter == 'period':
                hid = obj_move.search(cr, uid, [('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
                awal = obj_move.search(cr, uid, [('period_id', '<', val.period_from.id)])
            elif val.filter == 'date':
                hid = obj_move.search(cr, uid, [('date', '>=', val.date_from), ('date', '<=', val.date_to)])
                awal = obj_move.search(cr, uid, [('date', '<', val.date_from)])
        

        if not hid:
            raise osv.except_osv(('Perhatian !'), ('TIdak ada mutasi account pada tanggal / period tersebut'))


        data = 'sep=;\nName;Date;Period;Journal;Reference;Description;Account;Analytic;Destination Analytic;Debit;Credit;Balance;Amount Currency;Partner;Invoice;Status'
        
        
        for a in val.account_ids:
            mid = obj_move_line.search(cr, uid, [('move_id', 'in', hid), ('account_id', '=', a.id)])
            mad = obj_move_line.browse(cr, uid, mid)

            wil = obj_move_line.search(cr, uid, [('move_id', 'in', awal), ('account_id', '=', a.id)])
            wal = obj_move_line.browse(cr, uid, wil)
            
            cre = [i.credit for i in wal]
            dbt = [i.debit for i in wal]
            amt = [i.amount_currency for i in wal]
            
            mut_cre = 0
            mut_dbt = 0
            mut_amt = 0
            
            data += '\n' + ';'.join(['-', '-', '-', '-', '-', '-', 'OPENING BALANCE', '-', '-', str(sum(dbt)), str(sum(cre)), str(sum(dbt)-sum(cre)), str(sum(amt)), '-', '-', '-'])
            data += '\n' + ';'.join(['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'])
            
            for x in mad:
                mut_cre += x.credit
                mut_dbt += x.debit
                mut_amt += x.amount_currency
                
                tujuan = '-'
                tmid = obj_move_line.search(cr, uid, [('move_id', '=', x.move_id.id)])
                if tmid:
                    tad = obj_move_line.browse(cr, uid, tmid)
                    tujuan = '[' + str(tad[0].analytic_account_id.code) + '] ' + str(tad[0].analytic_account_id.name)
                

                d = [x.move_id.name, 
                     x.move_id.date, 
                     x.move_id.period_id.name,
                     x.move_id.journal_id.name, 
                     str(x.move_id.ref), 
                     x.name,
                     '[' + x.account_id.code + '] ' + x.account_id.name,
                     '[' + str(x.analytic_account_id.code) + '] ' + str(x.analytic_account_id.name),
                     str(tujuan),
                     str(x.debit),
                     str(x.credit),
                     str(x.debit-x.credit),
                     str(x.amount_currency),
                     str(x.partner_id.name),
                     str(x.invoice.name),
                     x.move_id.state]
                data += '\n' + ';'.join(d)
            
            data += '\n' + ';'.join(['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'])
            data += '\n' + ';'.join(['-', '-', '-', '-', '-', '-', 'CLOSING BALANCE', '-', '-', str(sum(dbt)+mut_dbt), str(sum(cre)+mut_cre), str((sum(dbt)+mut_dbt)-(sum(cre)+mut_cre)), str(sum(amt)+mut_amt), '-', '-', '-'])
                          
        out = base64.encodestring(data.encode('utf-8')) #base64.encodestring(data)
        self.write(cr, uid, ids, {'data_file':out, 'name': 'ledger.xls'}, context=context)
                  
        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'siu_mutasi_account', 'view_wizard_mutasi_account')
        view_id = view_rec[1] or False
              
        return {
            'view_type': 'form',
            'view_id' : [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'mutasi.account',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
             


#         if val.status == 'all':
#         elif val.status == 'posted':
#             if val.filter == 'period':
#                 hid = obj_move.search(cr, uid, [('status', '=', 'posted'), ('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
#                 awal = obj_move.search(cr, uid, [('status', '=', 'posted'), ('period_id', '<', val.period_from.id)])
#             elif val.filter == 'date':
#                 hid = obj_move.search(cr, uid, [('status', '=', 'posted'), ('date', '>=', val.date_from), ('date', '<=', val.date_to)])
#                 awal = obj_move.search(cr, uid, [('status', '=', 'posted'), ('date', '<', val.date_from)])
#         elif val.status == 'draft':
#             if val.filter == 'period':
#                 hid = obj_move.search(cr, uid, [('status', '=', 'draft'), ('period_id', '>=', val.period_from.id), ('period_id', '<=', val.period_to.id)])
#             elif val.filter == 'date':
#                 hid = obj_move.search(cr, uid, [('status', '=', 'draft'), ('date', '>=', val.date_from), ('date', '<=', val.date_to)])
        