# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

class purchase_line_invoice(osv.osv_memory):
    _inherit = 'purchase.order.line_invoice'

    def _check_po_lines(self, cr, uid, context):
        if context.get('active_model', '') == 'purchase.order.line':
            ids = context['active_ids']
            if len(ids) > 1:
                oline_obj = self.pool.get('purchase.order.line')
                po_lines = oline_obj.read(cr, uid, ids, ['state', 'partner_id', 'company_id'])
                for pol in po_lines:
                    if pol['state'] not in ('confirmed','done'):
                        raise orm.except_orm(_('Warning'), _('At least one of the selected po lines is %s! \nOrder lines status allowed is confirmed or done.') % pol['state'])
                    if (pol['partner_id'] != po_lines[0]['partner_id']):
                        raise orm.except_orm(_('Warning'), _('Not all Purchase Order Lines are for the same supplier! \nSupplier ID must be the same.'))
                    if (pol['company_id'] != po_lines[0]['company_id']):
                        raise orm.except_orm(_('Warning'), _('Not all Purchase Order Lines are at the same company!'))
        return {}

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(purchase_line_invoice, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_po_lines(cr, uid, context)
        return res




