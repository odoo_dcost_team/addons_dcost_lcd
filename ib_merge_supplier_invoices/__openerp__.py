# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
{
    "name": "Merge Supplier Invoices",
    "version": "0.1",
    "category": "Custom Module",
    "depends": [
        "base",
        "account",
        "purchase",
        "stock",
        "stock_account",
        "ib_sequence_number",
    ],
    "author":"Ibrohim Binladin | +6283871909782 | ibradiiin@gmail.com",
    "website": "http://ibrohimbinladin.wordpress.com",
    "description": """
        Create Merge Supplier Invoice From :\n
        1. Picking IN (Incoming Shipment) - Invoicing Control=Base on Incoming Shipment ('picking')\n
        2. Purchase Order Lines - Invoicing Control=Base on Purchase Order Lines ('manual')\n
    """,
    "data": [
        "views/purchase_advance_merge_inv.xml",
        "views/stock_invoice_onshipping_view.xml",
        # "workflow.xml",
        "views/view.xml",
        # "views/purchase_line_invoice_view.xml",
    ],
    "installable": True,
    "auto_install": False,
}
