{
    "name":"Material Requirement Planning",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://openerp.com",
    "category":"Custom Modules/Manufacture",
    "description": """
        Import POS dengan API.
    """,
    "depends":["base", "stock", "account", "mrp", "siu_dds", "sale", "purchase_requisition", "siu_work_order"],
    "init_xml":[],
    "demo_xml":[],
    ##"update_xml":["mrp_view.xml",],
    "data": [
        #"mrp_view.xml",
    ],
    "active":False,
    "installable":True
}
