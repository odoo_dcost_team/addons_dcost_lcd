{
    "name":"Custom Report",
    "version":"0.1",
    "author":"SUMBER INTEGRASI UTAMA",
    "website":"http://openerp.co.id",
    "category":"Custom Modules",
    "description": """
        The base module to generate custom report.
    """,
    "depends": ["base", "stock"],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": ["report_dcost_view.xml"],
    "active": False,
    "installable": True
}
