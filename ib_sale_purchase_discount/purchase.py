# -*- coding: utf-8 -*-
##############################################################################
#####   IBRAHIM BILADIN   #####
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class purchase_order_line(osv.osv):
    _inherit = 'purchase.order.line'
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        context = context or {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.discount_method=="fixed":
                price = line.price_unit
            taxes = self.pool.get('account.tax').compute_all(cr, uid, line.taxes_id, price, line.product_qty, line.product_id, line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            val = taxes['total']
            if line.discount_method=="fixed":
                val = (taxes['total'] - line.discount)
            res[line.id] = self.pool.get('res.currency').round(cr, uid, cur, val)
        return res
    
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method'),
        'discount': fields.float('Discount', digits_compute= dp.get_precision('Discount')),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
    }
    
    def onchange_discount(self, cr, uid, ids, method, parent_disc, context=None):
        warning = {}
        val = {'discount_method': method, 'discount': 0.0,}
        if parent_disc in ('percent','fixed'):
            val.update({'discount_method': False, 'discount': 0.0})
            warning = {'title': _('Error!!!'),
                    'message' : _('Sudah ada perhitungan diskon diluar per produk (diskon persentase atau statis), silahkan periksa kembali dokumen ini.')}        
        return {'value': val, 'warning': warning}
    


class purchase_order(osv.osv):
    _inherit = "purchase.order"
    
    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        price_unit = line.price_unit * (1-(line.discount or 0.0)/100.0)
        product_qty = line.product_qty
        if line.discount_method == "fixed":
            price_unit = (line.product_qty * line.price_unit) - line.discount 
            product_qty = 1.0 
        for c in self.pool.get('account.tax').compute_all(cr, uid, line.taxes_id, price_unit, product_qty, line.product_id, line.order_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_discount': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = val3 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            if order.discount_method=='fixed':
                val2 = order.disc_amount
                val = val - ((val / val1) * order.disc_amount)
            if order.discount_method=='percent':
                val2 = val1 * ((order.disc_amount or 0.0)/100.0)
                val = val - (val * ((order.disc_amount or 0.0)/100.0))
                
            res[order.id]['amount_tax'] = self.pool.get('res.currency').round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = self.pool.get('res.currency').round(cr, uid, cur, val1)
            res[order.id]['amount_discount'] = self.pool.get('res.currency').round(cr, uid, cur, val2)
            res[order.id]['amount_total'] = (res[order.id]['amount_untaxed'] - res[order.id]['amount_discount']) + res[order.id]['amount_tax']
        return res
     
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('purchase.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()
    
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'disc_amount': fields.float('Discount Amount', digits_compute= dp.get_precision('Account'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),                
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
            store={
                'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
            },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_discount': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Discount',
            store={
                'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
            },
            multi='sums', help="The discount amount."),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Taxes',
            store={
                'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
            },
            multi='sums', help="The tax amount."),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'purchase.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'purchase.order.line': (_get_order, ['price_unit', 'taxes_id', 'discount', 'product_qty'], 10),
            },
            multi='sums', help="The total amount."),
    }
    _defaults = {
        'disc_amount': 0.0,
    }
    
    def _prepare_inv_line(self, cr, uid, account_id, order_line, context=None):
        """Collects require data from purchase order line that is used to create invoice line
        for that purchase order line
        :param account_id: Expense account of the product of PO line if any.
        :param browse_record order_line: Purchase order line browse record
        :return: Value for fields of invoice lines.
        :rtype: dict
        """
        return {
            'name': order_line.name,
            'account_id': account_id,
            'price_unit': order_line.price_unit or 0.0,
            'quantity': order_line.product_qty,
            'product_id': order_line.product_id.id or False,
            'uos_id': order_line.product_uom.id or False,
            'discount_method': order_line.discount_method,
            'discount': order_line.discount,
            'invoice_line_tax_id': [(6, 0, [x.id for x in order_line.taxes_id])],
            'account_analytic_id': order_line.account_analytic_id.id or False,
        }
    
    def action_invoice_create(self, cr, uid, ids, context=None):
        """Generates invoice for given ids of purchase orders and links that invoice ID to purchase order.
        :param ids: list of ids of purchase orders.
        :return: ID of created invoice.
        :rtype: int
        """
        res = False

        journal_obj = self.pool.get('account.journal')
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')

        for order in self.browse(cr, uid, ids, context=context):
            pay_acc_id = order.partner_id.property_account_payable.id
            journal_ids = journal_obj.search(cr, uid, [('type', '=','purchase'),('company_id', '=', order.company_id.id)], limit=1)
            if not journal_ids:
                raise osv.except_osv(_('Error!'),
                    _('Define purchase journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))

            # generate invoice line correspond to PO line and link that to created invoice (inv_id) and PO line
            inv_lines = []
            for po_line in order.order_line:
                acc_id = self._choose_account_from_po_line(cr, uid, po_line, context=context)
                inv_line_data = self._prepare_inv_line(cr, uid, acc_id, po_line, context=context)
                inv_line_id = inv_line_obj.create(cr, uid, inv_line_data, context=context)
                inv_lines.append(inv_line_id)

                po_line.write({'invoiced':True, 'invoice_lines': [(4, inv_line_id)]}, context=context)

            # get invoice data and create invoice
            inv_data = {
                'name': order.partner_ref or order.name,
                'reference': order.partner_ref or order.name,
                'account_id': pay_acc_id,
                'type': 'in_invoice',
                'partner_id': order.partner_id.id,
                'currency_id': order.pricelist_id.currency_id.id,
                'journal_id': len(journal_ids) and journal_ids[0] or False,
                'invoice_line': [(6, 0, inv_lines)],
                'origin': order.name,
                'fiscal_position': order.fiscal_position.id or False,
                'payment_term': order.payment_term_id.id or False,
                'company_id': order.company_id.id,
                'discount_method': order.discount_method,
                'disc_amount': order.disc_amount,
            }
            inv_id = inv_obj.create(cr, uid, inv_data, context=context)

            # compute the invoice
            inv_obj.button_compute(cr, uid, [inv_id], context=context, set_total=True)

            # Link this new invoice to related purchase order
            order.write({'invoice_ids': [(4, inv_id)]}, context=context)
            res = inv_id
        return res
    
    def onchange_discount_method(self, cr, uid, ids, method, context=None):
        warning = {}
        val = {'discount_method': False, 'disc_amount': 0.0,}
        for order in self.browse(cr, uid, ids, context=context):
            if method in ('percent','fixed'):
                val.update({'discount_method' : method, 'disc_amount': 0.0})
                for line in order.order_line:
                    if line.discount <> 0.0 or line.discount_method in ('percent','fixed'):
                        val.update({'discount_method' : '' or False, 'disc_amount': 0.0})
                        warning = {'title': _('Error!!!'),
                            'message' : _("Salah satu produk/item sudah ditetapkan diskon (persentase atau statis), untuk menggunakan 'metode diskon total (diluar produk)' maka kosongkan semua diskon persentase/fixed didalam produk.")}
        return {'value': val, 'warning': warning}
    
    def button_calculate(self, cr, uid, ids, context=None):
        return True
    
    
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
