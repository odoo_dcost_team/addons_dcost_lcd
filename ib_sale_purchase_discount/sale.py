# -*- coding: utf-8 -*-
##############################################################################
#####   IBRAHIM BILADIN   #####
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class sale_order(osv.osv):
    _inherit = "sale.order"
    
    def _amount_line_tax(self, cr, uid, line, context=None):
        val = 0.0
        price_unit = line.price_unit * (1-(line.discount or 0.0)/100.0)
        product_qty = line.product_uom_qty
        if line.discount_method == "fixed":
            price_unit = (line.product_uom_qty * line.price_unit) - line.discount 
            product_qty = 1.0 
        for c in self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, price_unit, product_qty, line.product_id, line.order_id.partner_id)['taxes']:
            val += c.get('amount', 0.0)
        return val
    
    def _amount_all(self, cr, uid, ids, field_name, arg, context=None):
        cur_obj = self.pool.get('res.currency')
        res = {}
        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {
                'amount_untaxed': 0.0,
                'amount_discount': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = val3 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                val += self._amount_line_tax(cr, uid, line, context=context)
            if order.discount_method=='fixed':
                val2 = order.disc_amount
                val = val - ((val / val1) * order.disc_amount)
            if order.discount_method=='percent':
                val2 = val1 * ((order.disc_amount or 0.0)/100.0)
                val = val - (val * ((order.disc_amount or 0.0)/100.0))
                
            res[order.id]['amount_tax'] = cur_obj.round(cr, uid, cur, val)
            res[order.id]['amount_untaxed'] = cur_obj.round(cr, uid, cur, val1)
            res[order.id]['amount_discount'] = cur_obj.round(cr, uid, cur, val2)
            res[order.id]['amount_total'] = (res[order.id]['amount_untaxed'] - res[order.id]['amount_discount']) + res[order.id]['amount_tax']
        return res
     
    def _get_order(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('sale.order.line').browse(cr, uid, ids, context=context):
            result[line.order_id.id] = True
        return result.keys()
    
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'disc_amount': fields.float('Discount Amount', digits_compute= dp.get_precision('Account'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Untaxed Amount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The amount without tax.", track_visibility='always'),
        'amount_discount': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Discount',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The discount amount.", track_visibility='always'),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Taxes',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The tax amount.", track_visibility='always'),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'sale.order': (lambda self, cr, uid, ids, c={}: ids, ['order_line','discount_method','disc_amount'], 10),
                'sale.order.line': (_get_order, ['price_unit', 'tax_id', 'discount', 'product_uom_qty'], 10),
            },
            multi='sums', help="The total amount.", track_visibility='always'),
    }
    _defaults = {
        'disc_amount': 0.0,
    }
    
    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        """Prepare the dict of values to create the new invoice for a
           sales order. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record order: sale.order record to invoice
           :param list(int) line: list of invoice line IDs that must be
                                  attached to the invoice
           :return: dict of value to create() the invoice
        """
        if context is None:
            context = {}
        journal_ids = self.pool.get('account.journal').search(cr, uid,
            [('type', '=', 'sale'), ('company_id', '=', order.company_id.id)],
            limit=1)
        if not journal_ids:
            raise osv.except_osv(_('Error!'),
                _('Please define sales journal for this company: "%s" (id:%d).') % (order.company_id.name, order.company_id.id))
        invoice_vals = {
            'name': order.client_order_ref or '',
            'origin': order.name,
            'type': 'out_invoice',
            'reference': order.client_order_ref or order.name,
            'account_id': order.partner_id.property_account_receivable.id,
            'partner_id': order.partner_invoice_id.id,
            'journal_id': journal_ids[0],
            'invoice_line': [(6, 0, lines)],
            'currency_id': order.pricelist_id.currency_id.id,
            'comment': order.note,
            'payment_term': order.payment_term and order.payment_term.id or False,
            'fiscal_position': order.fiscal_position.id or order.partner_id.property_account_position.id,
            'date_invoice': context.get('date_invoice', False),
            'company_id': order.company_id.id,
            'user_id': order.user_id and order.user_id.id or False,
            'discount_method': order.discount_method,
            'disc_amount': order.disc_amount,
        }

        # Care for deprecated _inv_get() hook - FIXME: to be removed after 6.1
        invoice_vals.update(self._inv_get(cr, uid, order, context=context))
        return invoice_vals
    
    def onchange_discount_method(self, cr, uid, ids, method, context=None):
        warning = {}
        val = {'discount_method': False, 'disc_amount': 0.0,}
        for order in self.browse(cr, uid, ids, context=context):
            if method in ('percent','fixed'):
                val.update({'discount_method' : method, 'disc_amount': 0.0})
                for line in order.order_line:
                    if line.discount <> 0.0 or line.discount_method in ('percent','fixed'):
                        val.update({'discount_method' : '' or False, 'disc_amount': 0.0})
                        warning = {'title': _('Error!!!'),
                            'message' : _("Salah satu produk/item sudah ditetapkan diskon (persentase atau statis), untuk menggunakan 'metode diskon total (diluar produk)' maka kosongkan semua diskon persentase/fixed didalam produk.")}
        return {'value': val, 'warning': warning}
    
    def button_calculate(self, cr, uid, ids, context=None):
        return True
    


class sale_order_line(osv.osv):
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        context = context or {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.discount_method=="fixed":
                price = line.price_unit
            taxes = self.pool.get('account.tax').compute_all(cr, uid, line.tax_id, price, line.product_uom_qty, line.product_id, line.order_id.partner_id)
            cur = line.order_id.pricelist_id.currency_id
            val = taxes['total']
            if line.discount_method=="fixed":
                val = (taxes['total'] - line.discount)
            res[line.id] = self.pool.get('res.currency').round(cr, uid, cur, val)
        return res
    
    _inherit = 'sale.order.line'
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'discount': fields.float('Discount', digits_compute= dp.get_precision('Discount'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', digits_compute= dp.get_precision('Account')),
    }
    
    def onchange_discount(self, cr, uid, ids, method, parent_disc, context=None):
        warning = {}
        val = {'discount_method': method, 'discount': 0.0,}
        if parent_disc in ('percent','fixed'):
            val.update({'discount_method': False, 'discount': 0.0})
            warning = {'title': _('Error!!!'),
                    'message' : _("Sudah ada perhitungan diskon diluar per produk (diskon persentase atau statis), silahkan periksa kembali dokumen ini.")}        
        return {'value': val, 'warning': warning}
    
    def _prepare_order_line_invoice_line(self, cr, uid, line, account_id=False, context=None):
        """Prepare the dict of values to create the new invoice line for a
           sales order line. This method may be overridden to implement custom
           invoice generation (making sure to call super() to establish
           a clean extension chain).

           :param browse_record line: sale.order.line record to invoice
           :param int account_id: optional ID of a G/L account to force
               (this is used for returning products including service)
           :return: dict of values to create() the invoice line
        """
        res = {}
        if not line.invoiced:
            if not account_id:
                if line.product_id:
                    account_id = line.product_id.property_account_income.id
                    if not account_id:
                        account_id = line.product_id.categ_id.property_account_income_categ.id
                    if not account_id:
                        raise osv.except_osv(_('Error!'),
                                _('Please define income account for this product: "%s" (id:%d).') % \
                                    (line.product_id.name, line.product_id.id,))
                else:
                    prop = self.pool.get('ir.property').get(cr, uid,
                            'property_account_income_categ', 'product.category',
                            context=context)
                    account_id = prop and prop.id or False
            uosqty = self._get_line_qty(cr, uid, line, context=context)
            uos_id = self._get_line_uom(cr, uid, line, context=context)
            pu = 0.0
            if uosqty:
                pu = round(line.price_unit * line.product_uom_qty / uosqty,
                        self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Price'))
            fpos = line.order_id.fiscal_position or False
            account_id = self.pool.get('account.fiscal.position').map_account(cr, uid, fpos, account_id)
            if not account_id:
                raise osv.except_osv(_('Error!'),
                            _('There is no Fiscal Position defined or Income category account defined for default properties of Product categories.'))
            res = {
                'name': line.name,
                'sequence': line.sequence,
                'origin': line.order_id.name,
                'account_id': account_id,
                'price_unit': pu,
                'quantity': uosqty,
                'discount_method': line.discount_method,
                'discount': line.discount,
                'uos_id': uos_id,
                'product_id': line.product_id.id or False,
                'invoice_line_tax_id': [(6, 0, [x.id for x in line.tax_id])],
                'account_analytic_id': line.order_id.project_id and line.order_id.project_id.id or False,
            }

        return res
    
    
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
