# -*- coding: utf-8 -*-
##############################################################################
#####   IBRAHIM BILADIN   #####
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import time

class account_invoice_icwn(osv.osv):
    _inherit = "account.invoice"
    
    def _amount_all(self, cr, uid, ids, name, args, context=None):
        res = {}
        for invoice in self.browse(cr, uid, ids, context=context):
            res[invoice.id] = {
                'amount_untaxed': 0.0,
                'amount_discount': 0.0,
                'amount_tax': 0.0,
                'amount_total': 0.0,
            }
            val = val1 = val2 = 0.0
            cur = invoice.currency_id
            for line in invoice.invoice_line:
                val += line.price_subtotal
            for taxline in invoice.tax_line:
                val2 += taxline.amount
            if invoice.discount_method=='fixed':
                val1 = invoice.disc_amount
                val2 = val2 - ((val2 / val) * invoice.disc_amount)
            if invoice.discount_method=='percent':
                val1 = val * ((invoice.disc_amount or 0.0)/100.0)
                val2 = val2 - (val2 * ((invoice.disc_amount or 0.0)/100.0))

            res[invoice.id]['amount_tax'] = self.pool.get('res.currency').round(cr, uid, cur, val2)
            res[invoice.id]['amount_untaxed'] = self.pool.get('res.currency').round(cr, uid, cur, val)
            res[invoice.id]['amount_discount'] = self.pool.get('res.currency').round(cr, uid, cur, val1)
            res[invoice.id]['amount_total'] = (res[invoice.id]['amount_untaxed'] - res[invoice.id]['amount_discount']) + res[invoice.id]['amount_tax']
        return res
     
    def _get_invoice_line(self, cr, uid, ids, context=None):
        result = {}
        for line in self.pool.get('account.invoice.line').browse(cr, uid, ids, context=context):
            result[line.invoice_id.id] = True
        return result.keys()
 
    def _get_invoice_tax(self, cr, uid, ids, context=None):
        result = {}
        for tax in self.pool.get('account.invoice.tax').browse(cr, uid, ids, context=context):
            result[tax.invoice_id.id] = True
        return result.keys()
    
    _columns = {
        'discount_method': fields.selection([('percent','Percentage'),('fixed','Fixed Amount')], 'Discount Method', 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'disc_amount': fields.float('Discount Amount', digits_compute= dp.get_precision('Account'), 
                readonly=True, states={'draft': [('readonly', False)],'sent': [('readonly', False)]}),
        'amount_discount': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Discount', track_visibility='always',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line','discount_method','disc_amount'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all'),
        'amount_untaxed': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Subtotal', track_visibility='always',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line','discount_method','disc_amount'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all'),
        'amount_tax': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Tax',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line','discount_method','disc_amount'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all'),
        'amount_total': fields.function(_amount_all, digits_compute=dp.get_precision('Account'), string='Total',
            store={
                'account.invoice': (lambda self, cr, uid, ids, c={}: ids, ['invoice_line'], 20),
                'account.invoice.tax': (_get_invoice_tax, None, 20),
                'account.invoice.line': (_get_invoice_line, ['price_unit','invoice_line_tax_id','quantity','discount','invoice_id'], 20),
            },
            multi='all'),
    }
    
    def onchange_discount_method(self, cr, uid, ids, method, context=None):
        warning = {}
        val = {'discount_method': False, 'disc_amount': 0.0,}
        for inv in self.browse(cr, uid, ids, context=context):
            if method in ('percent','fixed'):
                val.update({'discount_method' : method, 'disc_amount': 0.0})
                for line in inv.invoice_line:
                    if line.discount <> 0.0 or line.discount_method in ('percent','fixed'):
                        val.update({'discount_method' : '' or False, 'disc_amount': 0.0})
                        warning = {'title': _('Error!!!'),
                            'message' : _("Salah satu produk/item sudah ditetapkan diskon (persentase atau statis), untuk menggunakan 'metode diskon total (diluar produk)' maka kosongkan semua diskon persentase/fixed didalam produk.")}
        return {'value': val, 'warning': warning}
    
    
    
    
class account_invoice_line_icwn(osv.osv):
    _inherit = "account.invoice.line"
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            if line.discount_method=="fixed":
                price = line.price_unit
            taxes = self.pool.get('account.tax').compute_all(cr, uid, line.invoice_line_tax_id, price, line.quantity, line.product_id, line.invoice_id.partner_id)
            cur = line.invoice_id.currency_id
            val = taxes['total']
            if line.discount_method=="fixed":
                val = (taxes['total'] - line.discount)
            res[line.id] = self.pool.get('res.currency').round(cr, uid, cur, val)
        return res
 
     
    _columns = {
        'discount': fields.float('Discount', digits_compute=dp.get_precision('Percentage')),
        'discount_method' : fields.selection([('percent', 'Percentage'),('fixed', 'Fixed Amount')], 'Discount Method'),
        'price_subtotal': fields.function(_amount_line, string='Amount', digits_compute= dp.get_precision('Account')),
    }
    
    def onchange_discount(self, cr, uid, ids, method, parent_disc, context=None):
        warning = {}
        val = {'discount_method': method, 'discount': 0.0,}
        if parent_disc in ('percent','fixed'):
            val.update({'discount_method': False, 'discount': 0.0})
            warning = {'title': _('Error!!!'),
                    'message' : _('Sudah ada perhitungan diskon diluar per produk (diskon persentase atau statis), silahkan periksa kembali dokumen ini.')}        
        return {'value': val, 'warning': warning}
    


class account_invoice_tax_icwn(osv.osv):
    _inherit = "account.invoice.tax"
    
    def compute(self, cr, uid, invoice_id, context=None):
        tax_grouped = {}
        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        inv = self.pool.get('account.invoice').browse(cr, uid, invoice_id, context=context)
        cur = inv.currency_id
        company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id
        for line in inv.invoice_line:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            product_qty = line.quantity
            if line.discount_method == "fixed":
                price_unit = (line.quantity * line.price_unit) - line.discount
                product_qty = 1.0
            for tax in tax_obj.compute_all(cr, uid, line.invoice_line_tax_id, price_unit, product_qty, line.product_id, inv.partner_id)['taxes']:
                val={}
                val['invoice_id'] = inv.id
                val['name'] = tax['name']
                val['amount'] = tax['amount']
                val['manual'] = False
                val['sequence'] = tax['sequence']
                val['base'] = cur_obj.round(cr, uid, cur, tax['price_unit'] * line['quantity'])

                if inv.type in ('out_invoice','in_invoice'):
                    val['base_code_id'] = tax['base_code_id']
                    val['tax_code_id'] = tax['tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['base'] * tax['base_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['amount'] * tax['tax_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['account_id'] = tax['account_collected_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_collected_id']
                else:
                    val['base_code_id'] = tax['ref_base_code_id']
                    val['tax_code_id'] = tax['ref_tax_code_id']
                    val['base_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['base'] * tax['ref_base_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['tax_amount'] = cur_obj.compute(cr, uid, inv.currency_id.id, company_currency, val['amount'] * tax['ref_tax_sign'], context={'date': inv.date_invoice or time.strftime('%Y-%m-%d')}, round=False)
                    val['account_id'] = tax['account_paid_id'] or line.account_id.id
                    val['account_analytic_id'] = tax['account_analytic_paid_id']

                key = (val['tax_code_id'], val['base_code_id'], val['account_id'], val['account_analytic_id'])
                if not key in tax_grouped:
                    tax_grouped[key] = val
                else:
                    tax_grouped[key]['amount'] += val['amount']
                    tax_grouped[key]['base'] += val['base']
                    tax_grouped[key]['base_amount'] += val['base_amount']
                    tax_grouped[key]['tax_amount'] += val['tax_amount']

        for t in tax_grouped.values():
            t['base'] = cur_obj.round(cr, uid, cur, t['base'])
            t['amount'] = cur_obj.round(cr, uid, cur, t['amount'])
            t['base_amount'] = cur_obj.round(cr, uid, cur, t['base_amount'])
            t['tax_amount'] = cur_obj.round(cr, uid, cur, t['tax_amount'])
        return tax_grouped
    
    
    
    