import ast
import time
from openerp import netsvc
import urllib2
from itertools import groupby
from datetime import date, datetime, timedelta
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv, orm
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import float_compare
from openerp.tools.translate import _
from openerp import tools, SUPERUSER_ID

class ProductionPlan(osv.osv):
    _name = 'production.plan'
    _columns = {
        'name': fields.char('Reference', size=64, required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'date': fields.date('Date', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'type': fields.selection([('log', 'Senlog'), ('kit', 'Senkit')], 'Type', required=True, readonly=True, select=True, states={'draft': [('readonly', False)]}),
        'user_id': fields.many2one('res.users', 'Responsible', states={'draft': [('readonly', False)]}, select=True, track_visibility='onchange'),
        'notes': fields.text('Notes'),
        'mrp': fields.boolean('MRP Done'),
        'state': fields.selection([ ('draft', 'Draft'), ('cancel', 'Cancel'), ('approve', 'Approve'), ('done', 'Done')], 'Plan State', readonly=True, select=True),
        'stock_ids': fields.many2many('stock.order','plan_stock_rel', 'plan_stock_id', 'stock_id', 'Stock Order', domain="[('mps', '=', False), ('state', '=', 'done'), ('type', '=', type)]", readonly=True, states={'draft': [('readonly', False)]}),
        'plan_line': fields.one2many('production.plan.line', 'plan_id', 'Production Plan Lines'),
    }

    _defaults = {
                 'name': lambda self, cr, uid, context={}: self.pool.get('ir.sequence').get(cr, uid, 'production.plan'),
                 'state' : 'draft',
                 'user_id': lambda obj, cr, uid, context: uid,
                 'type': 'log',
                 'date': lambda *a: time.strftime('%Y-%m-%d'), 
    }
    
    _order = 'name desc'


    def user_id_change(self, cr, uid, ids, idp):
        res = {}                       
        res['user_id'] = uid        
        return {'value': res}

    def unlink(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        if val.state != 'draft':
            raise osv.except_osv(('Perhatian !'), ('DDS tidak bisa dihapus pada state \'%s\'!') % (val.state,))
        return super(ProductionPlan, self).unlink(cr, uid, ids, context=context)
         
    def plan_draft(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'draft'})
        return True                               
    
    def plan_cancel(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'cancel'})
        return True                                  
         
    def plan_confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'approve'})
        return True
    
    def plan_validate(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        for x in val.stock_ids:
            self.pool.get('stock.order').write(cr, uid, [x.id], {'mps': True})
        self.write(cr, uid, ids, {'state': 'done'})
        return True 
    
    def compute(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids, context={})[0]
        quant_obj = self.pool.get('stock.quant')
        product_obj = self.pool.get('product.product')
        plan_line_obj = self.pool.get('production.plan.line')
        point_obj = self.pool.get('stock.warehouse.orderpoint')
        
        if val.plan_line:
            for x in val.plan_line:
                plan = x.plan
                if x.real - x.total == 0 :
                    plan = x.max
                elif x.real - x.total <= 0 :
                    plan = abs(x.real - x.total) + x.max
                elif x.real - x.total < x.min:
                    plan = x.max - (x.real - x.total)
                plan_line_obj.write(cr, uid, [x.id], {'plan': plan, 'end': x.real - x.total + plan})
        else:
            product = []
            for x in val.stock_ids:
                for l in x.stock_line:
                    product.append({'product_id': l.product_id.id, 'line': l, 'qty': l.product_qty, 'uom': l.product_uom.id})
                    
            data = {}
            for p in product:
                data[p['product_id']] = {'qty': [], 'uom': p['uom'], 'line': []}
            for p in product:
                data[p['product_id']]['qty'].append(p['qty'])
                data[p['product_id']]['line'].append(p['line'])
                
            for i in data:
                order = sum(data[i]['qty'])
                brg = product_obj.browse(cr, uid, i)
                min = 0; max = 0; plan = order; ada = 0
                
                cari = quant_obj.search(cr, uid, [('product_id', '=', i), ('location_id', '=', val.user_id.warehouse_id.lot_stock_id.id)])
                if cari:
                    ada = sum([x.qty for x in quant_obj.browse(cr, uid, cari)])   
                
                sid = point_obj.search(cr, uid, [('product_id', '=', i), ('location_id', '=', val.user_id.warehouse_id.lot_stock_id.id)])
                if not sid:
                    #raise osv.except_osv(('Perhatian'), ('Product %s belum memiliki stock buffer pada %s !' % (brg.partner_ref, val.user_id.warehouse_id.lot_stock_id.location_id.name)))
                    min = 0
                    max = 0
                else:
                    sto = point_obj.browse(cr, uid, sid)[0]
                    min = sto.product_min_qty
                    max = sto.product_max_qty
                
                plan = 0
                if ada - order == 0 :
                    plan = max
                elif ada - order <= 0 :
                    plan = abs(ada - order) + max
                elif ada - order < min:
                    plan = max - (ada - order)

                plan_line_obj.create(cr, uid, {
                                               'plan_id': val.id,
                                               'product_id': i,
                                               'product_uom': data[i]['uom'],
                                               'total': order,
                                               'name': brg.partner_ref,
                                               'real': ada,
                                               'min': min,
                                               'max': max,
                                               'plan': plan,
                                               'end': ada - order + plan 
                })
                
        return True 

class ProductionPlanLine(osv.osv):
    _name = 'production.plan.line' 
    _columns = {
        'plan_id': fields.many2one('production.plan', 'Plan Reference', required=True, ondelete='cascade', select=True),
        'name': fields.char('Description', size=256, required=True, select=True, readonly=True),
        'product_id': fields.many2one('product.product', 'Product', change_default=True, readonly=True),
        'total': fields.float('Total Order', readonly=True, digits_compute=dp.get_precision('Product UoM')),
        'real': fields.float('On Hand', digits_compute=dp.get_precision('Product UoM')),
        'min': fields.float('Min', digits_compute=dp.get_precision('Product UoM')),
        'max': fields.float('Max', digits_compute=dp.get_precision('Product UoM')),
        'plan': fields.float('Plan', digits_compute=dp.get_precision('Product UoM')),
        'end': fields.float('End of Stock', readonly=True, digits_compute=dp.get_precision('Product UoM')),
        'product_uom': fields.many2one('product.uom', 'UoM', readonly=True),
    }
    
    def product_id_change(self, cr, uid, ids, product):
        res = {}
        product_obj = self.pool.get('product.product')
        if product:
            res['name'] = product_obj.name_get(cr, uid, [product])[0][1]
            res['product_uom'] = product_obj.browse(cr, uid, product).uom_id.id
        return {'value': res}



class stock_warehouse_orderpoint(osv.osv):
    _inherit = "stock.warehouse.orderpoint"
    _columns = {
        'create_uid': fields.many2one('res.users', 'Responsible'),
        'product_id': fields.many2one('product.product', 'Product', required=True, ondelete='cascade', domain=[('type', '=', 'product'), ('sale_ok', '=', False)]),
    }

    def onchange_warehouse_id(self, cr, uid, ids, warehouse_id, context=None):
        if warehouse_id:
            w = self.pool.get('res.users').browse(cr, uid, uid).warehouse_id
            if not w :
                gud = 1; loc = 12
            else:
                gud = w.id; loc = w.lot_stock_id.id
            v = {'warehouse_id': gud, 'location_id': loc}
            return {'value': v}
        return {}
