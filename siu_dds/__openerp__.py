{
    "name":"Daily Distribution Scheduling",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://openerp.com",
    "category":"Custom Modules/Manufacture",
    "description": """
        Import POS dengan API.
    """,
    "depends":["base", "stock", "mrp", "siu_stock_order"],
    "init_xml":[],
    "demo_xml":[],
    "data":[
                  "dds_view.xml",
                  ],
    "active":False,
    "installable":True
}
