# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
import time
import base64
from calendar import monthrange
import datetime

from openerp.osv import fields,osv
from openerp.tools.translate import _
#from datetime import date, timedelta, datetime

BULAN = [(0,''), (1,'Januari'), (2,'Februari'),(3,'Maret'),(4,'April'), (5,'Mei'), (6,'Juni'),
        (7,'Juli'), (8,'Agustus'), (9,'September'),(10,'Oktober'), (11,'November'), (12,'Desember')]

class wizard_report(osv.osv_memory):
    _name = "summary.report"

    def _get_loc_id(self, cr, uid, context=None):
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        res = wh_id = False
        cr.execute("""SELECT warehouse_id FROM res_users WHERE id=%s""", (uid,))
        for x in cr.fetchall():
            wh_id = x[0]
        if wh_id:       ##user.warehouse_id:
            wh_obj = self.pool.get('stock.warehouse')   #user.warehouse_id.id
            wh_ids = wh_obj.search(cr, uid, [('id','=',wh_id)], context=context)
            if wh_ids:
                whd = wh_obj.browse(cr, uid, wh_ids[0], context=context)
                res = whd.lot_stock_id and whd.lot_stock_id.id
        return res

    _columns = {
        'report_type': fields.selection(
            [('sum_stock', 'Stock Card Report'),
             ('rekap_omzet', 'Rekap Omzet'),
             ('sum_purchase', 'Purchase Summary Report'),
             ('sum_sale', 'Sale Summary Report')],
            'Report Type', required=True),
        'state_purchase': fields.selection([('all','All'),('draft', 'Draft PO'),
            ('confirmed', 'Waiting Approval'),('approved', 'Purchase Order'),('done', 'Done')], string='Status (Purchase)'),
        'xls_ok': fields.boolean('Print to XLS',
            help="Jika dicentang/ceklist maka akan dicetak dalam bentuk file Excel (xls), jika tidak dicentang maka akan dicetak dalam bentuk file PDF."),
        'start_date': fields.date('Start Date', select=True, help="Awal periode"),
        'end_date': fields.date('End Date', select=True, help="Akhir periode"),
        'sc_date': fields.date('Stock Card Date', select=True),
        'location_id': fields.many2one('stock.location', 'Location', copy=False),
        'state_sale': fields.selection([
            ('all', 'All'),('draft', 'Draft Quotation'),
            ('sent', 'Quotation Sent'),('waiting_date', 'Waiting Schedule'),
            ('progress', 'Sales Order'),('manual', 'Sale to Invoice'),
            ('shipping_except', 'Shipping Exception'),
            ('invoice_except', 'Invoice Exception'),('done', 'Done')], string='Status (Sale)'),
    }
    _defaults = {
        'report_type': 'sum_stock',
        'xls_ok': False,
        'location_id': _get_loc_id,
    }

    def print_summary_report(self, cr, uid, ids, data, context=None):
        for wzd_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            seq_dt1 = str(wzd_obj['start_date']).split('-')
            seq_dt2 = str(wzd_obj['end_date']).split('-')
            data['form']['title'] = _("Summary Report")
            if wzd_obj['report_type'] == "sum_purchase":
                data['form']['title'] = _("Purchase Summary Report\nPeriod ") + str(seq_dt1[2]) + " " + str.title(BULAN[int(seq_dt1[1])][1]) + " " + str(seq_dt1[0]) + _(" s/d ") + str(seq_dt2[2]) + " " + str.title(BULAN[int(seq_dt2[1])][1]) + " " + str(seq_dt2[0])
            elif wzd_obj['report_type'] == "sum_stock":
                data['form']['title'] = _("STOCK GUDANG SENLOG (GROCERIES 2)\nPT.PENDEKAR BODOH")
            elif wzd_obj['report_type'] == "rekap_omzet":
                data['form']['title'] = _("REKAPITULASI OMZET\nPeriod ") + str(seq_dt1[2]) + " " + str.title(BULAN[int(seq_dt1[1])][1]) + " " + str(seq_dt1[0]) + _(" s/d ") + str(seq_dt2[2]) + " " + str.title(BULAN[int(seq_dt2[1])][1]) + " " + str(seq_dt2[0])
            elif wzd_obj['report_type'] == "sum_sale":
                data['form']['title'] = _("Sale Summary Report\nPeriod ") + str(seq_dt1[2]) + " " + str.title(BULAN[int(seq_dt1[1])][1]) + " " + str(seq_dt1[0]) + _(" s/d ") + str(seq_dt2[2]) + " " + str.title(BULAN[int(seq_dt2[1])][1]) + " " + str(seq_dt2[0])

            report_name, objek = '', ''
            if wzd_obj['report_type'] == "sum_purchase":
                objek = 'purchase.order'
            elif wzd_obj['report_type'] == "rekap_omzet":
                objek = 'stock.picking'
            elif wzd_obj['report_type'] == "sum_sale":
                objek = 'sale.order'
            else:
                objek = 'product.product'

            if wzd_obj['xls_ok'] == True:
                if wzd_obj['report_type'] == "sum_purchase":
                    report_name = "summary.po.xls"
                    if wzd_obj['state_purchase'] in ("confirmed", "approved", "draft", "done"):
                        report_name = "summary.po.states.xls"
                elif wzd_obj['report_type'] == "sum_stock":
                    report_name = "summary.stock.card.xls"
                elif wzd_obj['report_type'] == "rekap_omzet":
                    report_name = "rekap.omzet.xls"
                elif wzd_obj['report_type'] == "sum_sale":
                    report_name = "summary.sale.xls"
                    if wzd_obj['state_sale'] in ("progress","manual","draft","done","sent","shipping_except","invoice_except","waiting_date"):
                        report_name = "summary.sale.states.xls"
            else:
                if wzd_obj['report_type']=="sum_purchase":
                    report_name = "summary.po.pdf"
                    if wzd_obj['state_purchase'] in ("confirmed","approved","draft","done"):
                        report_name = "summary.po.states.pdf"
                elif wzd_obj['report_type'] == "sum_stock":
                    report_name = "summary.stock.card.pdf"
                elif wzd_obj['report_type'] == "rekap_omzet":
                    report_name = "rekap.omzet.pdf"
                elif wzd_obj['report_type'] == "sum_sale":
                    report_name = "summary.sale.pdf"
                    if wzd_obj['state_sale'] in ("progress","manual","draft","done","sent","shipping_except","invoice_except","waiting_date"):
                        report_name = "summary.sale.states.pdf"

            #prev_date = (date.today() - timedelta(1)).strftime('%Y-%m-%d')
            #if wzd_obj['sc_date']:
                #sc_dt = datetime.strptime(wzd_obj['sc_date'], "%Y-%m-%d").date()
                #prev_date = (sc_dt - timedelta(1)).strftime('%Y-%m-%d')

            data['form']['loc_id'] = 0
            if wzd_obj['report_type'] == "sum_stock":
                data['form']['loc_id'] = int(wzd_obj['location_id'][0])

            data['form']['subdir_report'] = "/odoo/custom/addons/ib_reports/report/"
            data['form']['start_date'] = wzd_obj['start_date']
            data['form']['end_date'] = wzd_obj['end_date']
            #data['form']['prev_date'] = prev_date
            data['form']['curr_date'] = wzd_obj['sc_date']
            data['form']['status_purchase'] = wzd_obj['state_purchase']
            data['form']['status_sale'] = wzd_obj['state_sale']
            data['model'] = objek
            data['ids'] = self.pool.get(data['model']).search(cr,uid,[])
            return {
                    'type': 'ir.actions.report.xml',
                    'report_name': report_name,
                    'datas': data,
            }

wizard_report()


class dcost_accounting_reports(osv.osv_memory):
    _name = "dcost.accounting.reports"

    def _get_warehouse(self, cr, uid, context=None):
        usr_obj = self.pool.get('res.users')
        company_id = usr_obj._get_company(cr, uid, context=context)
        warehouse_ids = self.pool.get('stock.warehouse').search(cr, uid, [('company_id', '=', company_id)], context=context)
        user = usr_obj.browse(cr, uid, uid, context=context)
        warehouse_id = False
        if user.warehouse_id:
            warehouse_id = user.warehouse_id.id
        elif warehouse_ids:
            warehouse_id = warehouse_ids[0]
        return warehouse_id

    _columns = {
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'chart_account_id': fields.many2one('account.account', 'CoA',
            domain="[('parent_id','=',False), ('company_id','=',company_id)]", required=True),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
        'filter': fields.selection((('all', 'All Outlet'), ('outlet', 'Single Outlet')), 'Filter by', required=True),
        'tipe': fields.selection((('tb', 'Trial Balance'),('bs', 'Balance Sheet'),('pl', 'Profit Loss'),('gl', 'General Ledger')), 'Report Type', required=True),
        'period_type': fields.selection((('single', '1 Period'), ('many', 'Period Ranges')), 'Period Type', required=True),
        'period_id': fields.many2one('account.period', 'Period'),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet'),
        'xls_ok': fields.boolean('Print to XLS',
            help="Jika dicentang/ceklist maka akan dicetak dalam bentuk file Excel (xls), jika tidak dicentang maka akan dicetak dalam bentuk file PDF."),
        'period_from': fields.many2one('account.period', 'Start Period'),
        'period_to': fields.many2one('account.period', 'End Period'),
        'journal_id': fields.many2one('account.journal', 'Journal'),
        'account_id': fields.many2one('account.account', 'Account'),
    }
    _defaults = {
        'company_id': 1,
        'chart_account_id': 1,
        'fiscalyear_id': 1,
        'filter': 'all',
        'period_type': 'single',
        'xls_ok': False,
        'warehouse_id': _get_warehouse,
    }

    def company_change(self, cr, uid, ids, company_id):  #'period_from': False, 'period_to': False
        return {'value': {'chart_account_id': False, 'fiscalyear_id': False, 'period_id': False, 'period_from': False, 'period_to': False}}

    def print_dcost_acc_report(self, cr, uid, ids, data, context=None):
        now = datetime.datetime.now()
        for wzd_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            #if wzd_obj['tipe'] == "tb" and wzd_obj['period_type'] == "many":
                #raise osv.except_osv(_('Peringatan !!!'), _('Trial Balance Report tidak bisa dicetak dengan filter rentang periode tertentu (Period Ranges), tapi harus single (1 period).'))
            if wzd_obj['tipe'] == "tb" and wzd_obj['filter'] == "outlet":
                raise osv.except_osv(_('Peringatan !!!'), _('Trial Balance Report tidak bisa dicetak dengan 1 Outlet (WH), TB Report bisa dicetak dari semua Outlet (WH).'))
            if wzd_obj['tipe'] == "gl" and wzd_obj['filter'] == "all":
                raise osv.except_osv(_('Peringatan !!!'), _('General Ledger Report tidak bisa dicetak dari semua Outlet (WH), GL Report bisa dicetak hanya dari 1 Outlet/WH saja(single).'))
            #if wzd_obj['tipe'] == "gl" and wzd_obj['period_type'] == "single":
                #raise osv.except_osv(_('Peringatan !!!'), _('General Ledger Report tidak bisa dicetak dengan filter 1 period (single), GL Report bisa dicetak hanya dengan rentang periode tertentu (Period Ranges).'))

            exp = []
            period = False
            data['form']['label_year_todate'] = ""
            data['form']['period_date_start'] = time.strftime('%Y-01-01')
            data['form']['period_date_stop'] = time.strftime('%Y-12-31')
            if wzd_obj['period_type'] == "single":
                period = self.pool.get('account.period').browse(cr, uid, wzd_obj['period_id'][0])
                exp = str(period.code).split('/') #period.name
                #if wzd_obj['tipe'] == "tb" or wzd_obj['tipe'] == "gl":
                data['form']['period_date_start'] = period.date_start
                data['form']['period_date_stop'] = period.date_stop
                data['form']['label_year_todate'] = str("01")+"/"+ str(time.strftime('%Y')) + _(" - ") + str(period.name)
                if exp[0] == '00' or int(exp[0]) == 0:
                    data['form']['label_year_todate'] = str(period.date_start) + _(" - ") + str(period.date_stop)
            afy = self.pool.get('account.fiscalyear').browse(cr, uid, wzd_obj['fiscalyear_id'][0], context=context)
            data['form']['title'] = _("PT. PENDEKAR BODOH")
            if wzd_obj['tipe'] == "tb":
                data['form']['title'] += _("\nTRIAL BALANCE")
            elif wzd_obj['tipe'] == "bs":
                data['form']['title'] += _("\nNERACA (BALANCE SHEET)")
            elif wzd_obj['tipe'] == "pl":
                data['form']['title'] += _("\nLAPORAN LABA RUGI (PROFIT & LOSS)")
            elif wzd_obj['tipe'] == "gl":
                data['form']['title'] += _("\nGENERAL LEDGER")
            data['form']['title'] += _("\nFiscal Year: ") + str(afy.name)

            if wzd_obj['period_type'] == "single":
                data['form']['period_id'] = int(wzd_obj['period_id'][0])
                if exp[0] == '00' or int(exp[0]) == 0:
                    data['form']['title'] += _(" | Period: ")
                    if period and period.name:
                        data['form']['title'] += _(period.name)
                else:
                    data['form']['title'] += _(" | Period: ") + str(monthrange(int(exp[1]), int(exp[0]))[1]) + \
                                             "-"+ str.title(BULAN[int(exp[0])][1]) +"-"+ str(exp[1])

            reports = _(wzd_obj['tipe']) + "."
            if wzd_obj['filter'] == "outlet":
                reports += _("singlewh.")
            elif wzd_obj['filter']=="all":
                reports += _("manywh.")

            if wzd_obj['period_type'] == "single" and wzd_obj['tipe'] in ("tb","bs","pl"):
                reports += _("oneperiod.")
            elif wzd_obj['period_type'] == "many" and wzd_obj['tipe'] == "tb":
                reports += _("multiperiod.")

            data['form']['journal_id'] = 0
            data['form']['account_id'] = 0
            if wzd_obj['journal_id'] and wzd_obj['journal_id'][0]:
                data['form']['journal_id'] = int(wzd_obj['journal_id'][0])
                if wzd_obj['tipe'] == "gl":
                    jurnal = self.pool.get('account.journal').browse(cr, uid, wzd_obj['journal_id'][0],context=context)
                    reports += _("singlejr.")
                    data['form']['title'] += _(" | Journal: ") + str(jurnal.name)
            if wzd_obj['account_id'] and wzd_obj['account_id'][0]:
                data['form']['account_id'] = int(wzd_obj['account_id'][0])
                if wzd_obj['tipe'] == "gl":
                    akun = self.pool.get('account.account').browse(cr, uid, wzd_obj['account_id'][0], context=context)
                    reports += _("oneaccount.")
                    data['form']['title'] += _(" | Account: ") +"["+ str(akun.code) +"] "+ str(akun.name)

            if wzd_obj['xls_ok'] == True:
                reports += _("xls")
            else:
                reports += _("pdf")

            data['form']['year'] = str(now.year)
            if wzd_obj['fiscalyear_id'][0] and (afy.code or afy.name):
                data['form']['year'] = afy.code or afy.name

            if wzd_obj['period_type'] == "many":
                data['form']['period_id'] = 0
                if wzd_obj['period_from'][0]:
                    period_start = self.pool.get('account.period').browse(cr, uid, wzd_obj['period_from'][0])
                    data['form']['period_date_start'] = period_start.date_start
                    date_start = str(period_start.date_start).split('-')
                    data['form']['title'] += _(" | Period : ") + str(date_start[2]) +"/"+ str(date_start[1]) +"/"+ str(date_start[0])
                if wzd_obj['period_to'][0]:
                    period_stop = self.pool.get('account.period').browse(cr, uid, wzd_obj['period_to'][0])
                    data['form']['period_date_stop'] = period_stop.date_stop
                    date_stop = str(period_stop.date_stop).split('-')
                    data['form']['title'] += _(" to ") + str(date_stop[2]) + "/" + str(date_stop[1]) + "/" + str(date_stop[0])

            data['form']['subdir_report'] = "/odoo/custom/addons/ib_reports/report/"
            data['form']['company_id'] = int(wzd_obj['company_id'][0])
            data['form']['fiscalyear_id'] = int(wzd_obj['fiscalyear_id'][0])
            data['form']['coa_id'] = int(wzd_obj['chart_account_id'][0])
            data['form']['warehouse_id'] = int(wzd_obj['warehouse_id'][0])
            data['model'] = "account.account"
            data['ids'] = self.pool.get(data['model']).search(cr,uid,[])
            return {
                'type': 'ir.actions.report.xml',
                'report_name': reports,
                'datas': data,
            }


dcost_accounting_reports()

class reports_payable_and_paid(osv.osv_memory):
    _name = "payable.paid.report"

    _columns = {
        'report_type': fields.selection([('hutang','Kartu Hutang'),('paid','Payment Report')],
                'Report Type', required=True),
        'partner_id': fields.many2one('res.partner', 'Supplier'),
        'filter_by': fields.selection([
                ('due_date', 'Due Date'),
                ('payment_date', 'Payment Date'),
                ('inv_date', 'Invoice Date')], 'Filter By', required=True),
        'date_start': fields.date('Start date', required=True),
        'date_stop': fields.date('End date', required=True),
        'xls_ok': fields.boolean('Print to XLS',
            help="Jika dicentang/ceklist maka akan dicetak dalam bentuk file Excel (xls), jika tidak dicentang maka akan dicetak dalam bentuk file PDF."),
    }

    _defaults = {
        'report_type': 'hutang',
        'xls_ok': False,
    }

    def print_payable_paid_report(self, cr, uid, ids, data, context=None):
        if context is None: context = {}
        for wzd_obj in self.read(cr, uid, ids):
            if 'form' not in data:
                data['form'] = {}
            if wzd_obj['report_type'] == "hutang" and wzd_obj['filter_by'] == "payment_date":
                raise osv.except_osv(_('Peringatan !!!'), _("Tipe laporan Kartu Hutang tidak bisa difilter berdasarkan 'Payment Date' / tanggal pembayaran,\nsilahkan pilih yang lain..."))
            if wzd_obj['report_type'] == "paid" and wzd_obj['filter_by'] == "due_date":
                raise osv.except_osv(_('Peringatan !!!'), _("Tipe laporan pembayaran (Payment Report) tidak bisa difilter berdasarkan 'Due Date' / tanggal jatuh tempo,\nsilahkan pilih yang lain..."))

            data['form']['title'] = _("PAYABLE AND PAID REPORT")
            if wzd_obj['report_type'] == "hutang":
                data['form']['title'] = _("KARTU HUTANG")
            elif wzd_obj['report_type'] == "paid":
                data['form']['title'] = _("PAYMENT REPORT")

            if wzd_obj['partner_id'] and wzd_obj['partner_id'][0]:
                partner = self.pool.get('res.partner').browse(cr, uid, wzd_obj['partner_id'][0], context=context)
                data['form']['title'] += _("\nSupplier: ") + str.title((partner.name).encode('utf-8'))

            if wzd_obj['filter_by'] == "due_date":
                data['form']['title'] += _("\nDates due ")
            elif wzd_obj['filter_by'] == "payment_date":
                data['form']['title'] += _("\nPayment dates ")
            elif wzd_obj['filter_by'] == "inv_date":
                data['form']['title'] += _("\nInvoice dates ")

            date_start = str(wzd_obj['date_start']).split('-')
            date_stop = str(wzd_obj['date_stop']).split('-')
            data['form']['title'] += _("from ") + str(date_start[2]) +"-"+ \
                str.title(BULAN[int(date_start[1])][1]) + "-" + str(date_start[0]) + _(" to ") + \
                str(date_stop[2]) +"-"+ str.title(BULAN[int(date_stop[1])][1]) +"-"+ str(date_stop[0])

            report_name = _("")
            if wzd_obj['report_type'] == "hutang":
                report_name += _("kh.")
            elif wzd_obj['report_type'] == "paid":
                report_name += _("pr.")

            if wzd_obj['filter_by'] == "due_date":
                report_name += _("byduedate.")
            elif wzd_obj['filter_by'] == "payment_date":
                report_name += _("bypaydate.")
            elif wzd_obj['filter_by'] == "inv_date":
                report_name += _("byinvdate.")

            data['form']['partner_id'] = int(0)
            if wzd_obj['partner_id'] and wzd_obj['partner_id'][0]:
                report_name += _("supp.")
                data['form']['partner_id'] = int(wzd_obj['partner_id'][0])

            if wzd_obj['xls_ok'] == True:
                report_name += _("xls")
            else:
                report_name += _("pdf")

            data['form']['subdir_report'] = "/odoo/custom/addons/ib_reports/report/"
            data['form']['date_start'] = wzd_obj['date_start']
            data['form']['date_stop'] = wzd_obj['date_stop']
            data['model'] = "account.invoice"
            data['ids'] = self.pool.get(data['model']).search(cr, uid, [])
            return {
                'type': 'ir.actions.report.xml',
                'report_name': report_name,
                'datas': data,
            }

reports_payable_and_paid()


class inventory_mutation_report(osv.osv_memory):
    _name = "report.inventory.mutation"
    _columns = {
        'report_type': fields.selection([('inventory', 'Laporan Persediaan barang'),
                        ('opname', 'Form Stock Opname')],'Report Type', required=True),
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet (Warehouse)'), #,required=True
        'location_id': fields.many2one('stock.location', 'Location'),
        'date_start': fields.date('Beginning Date'),
        'date_stop': fields.date('End Date'),
        'xls_ok': fields.boolean('Print to XLS',
            help="Jika dicentang/ceklist maka akan dicetak dalam bentuk file Excel (xls), jika tidak dicentang maka akan dicetak dalam bentuk file PDF."),
    }

    def _get_company_id(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        return company_id or False

    _defaults = {
        'company_id': _get_company_id or 1,
        'xls_ok': False,
    }

    def print_inv_movement_report(self, cr, uid, ids, data, context=None):
        for wzd_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            data['form']['title'] = _("Inventory Movement Report\n(Laporan Persediaan)")
            report_name = _("inv.movement.")
            if wzd_obj['report_type'] == 'opname':
                report_name = _("stock.opname.")
                data['form']['title'] = _("Form Stock Opname")

            date_start = str(wzd_obj['date_start']).split('-')
            date_stop = str(wzd_obj['date_stop']).split('-')
            data['form']['title'] += _("\nFrom ") + str(date_start[2]) + "-" + str(date_start[1]) + "-" + str(date_start[0]) + _(" to ") + \
                     str(date_stop[2]) + "-" + str(date_stop[1]) + "-" + str(date_stop[0])
            #if wzd_obj['location_id'] and wzd_obj['location_id'][0]:

            if wzd_obj['warehouse_id'] and wzd_obj['warehouse_id'][0]:
                #wh_ids = self.pool.get('stock.warehouse').search(cr, uid, [('lot_stock_id','=',wzd_obj['location_id'][0])], context=context)
                warehouse = self.pool.get('stock.warehouse').browse(cr, uid, wzd_obj['warehouse_id'][0], context=context)  #wzd_obj['location_id'][0]
                lokasi = self.pool.get('stock.location').name_get(cr, uid, [warehouse.lot_stock_id.id], context=context)[0][1]
                data['form']['location_id'] = int(warehouse.lot_stock_id.id)
                data['form']['warehouse_id'] = int(wzd_obj['warehouse_id'][0])
                data['form']['title'] += _("\nOutlet (WH): ") + str(warehouse.outlet) + _(" | Location: ") + str(lokasi)
                report_name += _("wh.")
            else:
                data['form']['location_id'] = 0
                data['form']['warehouse_id'] = 0
                data['form']['title'] += _("\nOutlet (WH): All-Outlet") + _(" | Location: All-Location")

            if wzd_obj['xls_ok'] == True:
                report_name += _("xls")
            else:
                report_name += _("pdf")

            data['form']['subdir_report'] = "/odoo/custom/addons/ib_reports/report/"
            data['form']['company_id'] = int(wzd_obj['company_id'][0])
            data['form']['start_date'] = wzd_obj['date_start']
            data['form']['end_date'] = wzd_obj['date_stop']
            data['model'] = "stock.move"
            data['ids'] = self.pool.get(data['model']).search(cr,uid,[])
            return {
                'type': 'ir.actions.report.xml',
                'report_name': report_name,
                'datas': data,
            }


inventory_mutation_report()


class export_data_resource_accounting(osv.osv_memory):
    _name = "ekspor.datasource.wzd"
    _columns = {
        'company_id': fields.many2one('res.company', 'Company', required=True),
        # 'chart_account_id': fields.many2one('account.account', 'CoA',
        #         domain="[('parent_id','=',False), ('company_id','=',company_id)]"),
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year', required=True),
        'period_from': fields.many2one('account.period', 'Start Period', required=True),
        'period_to': fields.many2one('account.period', 'End Period', required=True),
        'name': fields.char('File Name', 254),
        'data_file': fields.binary('File'),
        'dbname': fields.char('DB Name'),
        # 'date_start': fields.date('Start date'),
        # 'date_stop': fields.date('End date'),
    }

    def _get_company_id(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        return company_id or False

    def _get_fiscalyear_id(self, cr, uid, context=None):
        fy_ids = self.pool.get('account.fiscalyear').search(cr, uid, [('code','=',str(time.strftime('%Y'))),('name','=',str(time.strftime('%Y')))])
        return fy_ids and fy_ids[0] or False

    def _get_dbname(self, cr, uid, context=None):
        return _(cr.dbname) or _("")

    _defaults = {
        'company_id': _get_company_id,
        'fiscalyear_id': _get_fiscalyear_id,
        'dbname': _get_dbname,
    }

    def onchange_company(self, cr, uid, ids, company_id):
        return {'value': {'fiscalyear_id': False, 'period_from': False, 'period_to': False}}

    def eksport_datasource_pivot(self, cr, uid, ids, context=None):
        val = self.browse(cr, uid, ids)[0]
        title = ''; data = ''

        obj_move = self.pool.get('account.move')
        obj_period = self.pool.get('account.period')
        obj_accounts = self.pool.get('account.account')
        obj_move_line = self.pool.get('account.move.line')

        period_date_start = time.strftime('%Y-01-01')
        period_date_stop = time.strftime('%Y-03-31')
        if val.period_from and val.period_from.id:
            period_date_start = obj_period.browse(cr, uid, val.period_from.id).date_start
        if val.period_to and val.period_to.id:
            period_date_stop = obj_period.browse(cr, uid, val.period_to.id).date_stop

        title = 'Datasource_%s_%s_to_%s.xls' % (str(cr.dbname),
                                                time.strftime('%d%b%y', time.strptime(period_date_start, '%Y-%m-%d')),
                                                time.strftime('%d%b%y', time.strptime(period_date_stop, '%Y-%m-%d')))
        data = 'sep=;\nCompany;Jenis;Outlet;Tahun;Bulan;Keterangan1;Keterangan2;IS_Item;Nominal'
        sql_query = _("SELECT COUNT(*) FROM account_analytic_line WHERE true")
        if str(cr.dbname) == "PB1" or val.dbname == "PB1":
            sql_query = '''
                SELECT c.name AS company, 
                    CASE WHEN w.name='WH Kpusat' OR w.outlet='DCOST Kpusat' THEN CAST('KPUSAT' as varchar) 
                        ELSE CAST('OUTLET' as varchar) END AS jenis, 
                    CAST(UPPER(REPLACE(w.outlet,'DCOST ','')) AS varchar) AS outlet, 
                    to_char(to_timestamp(date_part('year', ap.date_start)::text, 'YYYY'), 'YYYY') AS tahun,
                    UPPER(to_char(to_timestamp(date_part('month', ap.date_start)::text, 'MM'), 'Month')) AS bulan, 
                    CAST(CONCAT('[',q.code,'] ',q.name) AS varchar) AS parent1,  
                    CAST(CONCAT('[',p.code,'] ',p.name) AS varchar) AS parent2, 
                    CAST(CONCAT('[',aa.code,'] ',aa.name) AS varchar) AS account,  
                    CASE 
                      WHEN (SELECT COUNT(*) FROM account_analytic_line AS l 
                            WHERE l.account_id=o.id AND l.general_account_id=aa.id  
                            AND date_trunc('day',l.date)>=to_date(cast(ap.date_start as varchar), 'YYYY-MM-DD') AND 
                            date_trunc('day',l.date)<=to_date(cast(ap.date_stop as varchar), 'YYYY-MM-DD')) <= 0
                        THEN cast('0.00' as varchar)
                      ELSE 
                        (SELECT CAST(COALESCE(SUM(l.amount),0) as varchar) FROM account_analytic_line AS l 
                            WHERE l.account_id=o.id AND l.general_account_id=aa.id 
                                AND date_trunc('day',l.date)>=to_date(cast(ap.date_start as varchar), 'YYYY-MM-DD') AND 
                                date_trunc('day',l.date)<=to_date(cast(ap.date_stop as varchar), 'YYYY-MM-DD')
                            GROUP BY l.general_account_id) END AS balance
                FROM 
                    analytic_cabang AS ac LEFT JOIN account_analytic_account as o on ac.analytic_id=o.id 
                    LEFT JOIN stock_warehouse AS w ON ac.name=w.id 
                    LEFT JOIN account_account AS aa ON ac.account_id=aa.id
                    LEFT JOIN res_company AS c ON aa.company_id=c.id
                    LEFT JOIN account_account AS p ON aa.parent_id=p.id 
                    LEFT JOIN account_account AS q ON p.parent_id=q.id,
                    account_period AS ap 
                WHERE 
                    o.state NOT IN ('template','draft','close','cancelled') AND  
                    (w.code NOT IN ('skpebo','slpebo') OR w.outlet NOT IN ('DCOST SENLOG','DCOST SENKIT')) AND  
                    aa.active=true AND aa.company_id=%s AND aa.type NOT IN ('view','consolidation') AND 
                    date_trunc('day',ap.date_start) >= to_date(%s, 'YYYY-MM-DD') AND 
                    date_trunc('day',ap.date_stop) <= to_date(%s, 'YYYY-MM-DD') AND  
                    ap.special=false AND ap.fiscalyear_id=%s
                GROUP BY aa.id, c.id, w.id, o.id, ap.id, q.id, p.id   
                ORDER BY bulan, outlet, account asc'''
        elif str(cr.dbname) == "SENLOGPB5" or val.dbname == "SENLOGPB5":
            sql_query = '''
                SELECT c.name AS company, w.name AS jenis, 
                    REPLACE((SELECT name FROM res_partner WHERE id=w.partner_id),'DCOST ','') AS outlet, 
                    to_char(to_timestamp(date_part('year', ap.date_start)::text, 'YYYY'), 'YYYY') AS tahun,
                    UPPER(to_char(to_timestamp(date_part('month', ap.date_start)::text, 'MM'), 'Month')) AS bulan, 
                    CAST(CONCAT('[',q.code,'] ',q.name) AS varchar) AS parent1,  
                    CAST(CONCAT('[',p.code,'] ',p.name) AS varchar) AS parent2, 
                    CAST(CONCAT('[',aa.code,'] ',aa.name) AS varchar) AS account,  
                    CASE 
                      WHEN (SELECT COUNT(*) FROM account_analytic_line AS l 
                            WHERE l.account_id=o.id AND l.general_account_id=aa.id  
                            AND date_trunc('day',l.date)>=to_date(cast(ap.date_start as varchar), 'YYYY-MM-DD') AND 
                            date_trunc('day',l.date)<=to_date(cast(ap.date_stop as varchar), 'YYYY-MM-DD')) <= 0
                        THEN cast('0.00' as varchar)
                      ELSE 
                        (SELECT CAST(COALESCE(SUM(l.amount),0) as varchar) FROM account_analytic_line AS l 
                            WHERE l.account_id=o.id AND l.general_account_id=aa.id 
                                AND date_trunc('day',l.date)>=to_date(cast(ap.date_start as varchar), 'YYYY-MM-DD') AND 
                                date_trunc('day',l.date)<=to_date(cast(ap.date_stop as varchar), 'YYYY-MM-DD')
                            GROUP BY l.general_account_id) END AS balance
                FROM 
                    analytic_cabang AS ac LEFT JOIN account_analytic_account as o on ac.analytic_id=o.id 
                    LEFT JOIN stock_warehouse AS w ON ac.name=w.id 
                    LEFT JOIN account_account AS aa ON ac.account_id=aa.id
                    LEFT JOIN res_company AS c ON aa.company_id=c.id
                    LEFT JOIN account_account AS p ON aa.parent_id=p.id 
                    LEFT JOIN account_account AS q ON p.parent_id=q.id,
                    account_period AS ap 
                WHERE 
                    o.state NOT IN ('template','draft','close','cancelled') AND 
                    w.name IN ('SENLOGPB','SENKITPB') AND   
                    aa.active=true AND aa.company_id=%s AND aa.type NOT IN ('view','consolidation') AND 
                    date_trunc('day',ap.date_start) >= to_date(%s, 'YYYY-MM-DD') AND 
                    date_trunc('day',ap.date_stop) <= to_date(%s, 'YYYY-MM-DD') AND  
                    ap.special=false AND ap.fiscalyear_id=%s
                GROUP BY aa.id, c.id, w.id, o.id, ap.id, q.id, p.id   
                ORDER BY bulan, outlet, account asc'''

        cr.execute(sql_query, (val.company_id.id,period_date_start,period_date_stop,val.fiscalyear_id.id))
        for row in cr.dictfetchall():
            d = [str((row['company']).encode('utf-8')),
                 str((row['jenis']).encode('utf-8')),
                 str(row['outlet']),           #str((row['outlet']).encode('utf-8')),
                 str(row['tahun']),
                 str(row['bulan']),
                 str((row['parent1']).encode('utf-8')),
                 str((row['parent2']).encode('utf-8')),
                 str((row['account']).encode('utf-8')),
                 str((row['balance']).encode('utf-8'))]   ##float(row['balance'])]
            data += '\n' + ';'.join(d)

        out = base64.b64encode(data)
        self.write(cr, uid, ids, {'data_file': out, 'name': title}, context=context)

        view_rec = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'ib_reports', 'view_wzd_export_datasource_pivot')
        view_id = view_rec[1] or False

        return {
            'view_type': 'form',
            'view_id': [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': 'ekspor.datasource.wzd',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }


export_data_resource_accounting()


class laporan_rincian_aktiva(osv.osv_memory):
    _name = "asset.statement.details"
    _columns = {
        'company_id': fields.many2one('res.company', 'Company', required=True),
        'warehouse_id': fields.many2one('stock.warehouse', 'Outlet (Warehouse)'), #,required=True
        'fiscalyear_id': fields.many2one('account.fiscalyear', 'Fiscal Year'),
        'period_from': fields.many2one('account.period', 'Start Period'),
        'period_to': fields.many2one('account.period', 'End Period'),
        'xls_ok': fields.boolean('Print to XLS',
            help="Jika dicentang/ceklist maka akan dicetak dalam bentuk file Excel (xls), jika tidak dicentang maka akan dicetak dalam bentuk file PDF."),
    }

    def _get_company_id(self, cr, uid, context=None):
        company_id = self.pool.get('res.users')._get_company(cr, uid, context=context)
        return company_id or False

    def _get_fiscalyear_id(self, cr, uid, context=None):
        fy_ids = self.pool.get('account.fiscalyear').search(cr, uid, [('code','=',str(time.strftime('%Y'))),('name','=',str(time.strftime('%Y')))])
        return fy_ids and fy_ids[0] or False

    _defaults = {
        'company_id': _get_company_id or 1,
        'xls_ok': False,
        'fiscalyear_id': _get_fiscalyear_id,
    }

    def print_asset_statement_report(self, cr, uid, ids, data, context=None):
        for wzd_obj in self.read(cr,uid,ids):
            if 'form' not in data:
                data['form'] = {}
            data['form']['title'] = _("LAPORAN RINCIAN AKTIVA\n( ASSETS )")
            data['form']['warehouse_id'] = 0
            if wzd_obj['warehouse_id'] and wzd_obj['warehouse_id'][0]:
                #wh_ids = self.pool.get('stock.warehouse').search(cr, uid, [('lot_stock_id','=',wzd_obj['location_id'][0])], context=context)
                warehouse = self.pool.get('stock.warehouse').browse(cr, uid, wzd_obj['warehouse_id'][0], context=context)  #wzd_obj['location_id'][0]
                lokasi = self.pool.get('stock.location').name_get(cr, uid, [warehouse.lot_stock_id.id], context=context)[0][1]
                data['form']['warehouse_id'] = int(wzd_obj['warehouse_id'][0])
                data['form']['title'] += _("\nOutlet (WH): ") + str(warehouse.outlet) + _(" | Location: ") + str(lokasi)

            report_name = _("aset.statement.")
            if wzd_obj['warehouse_id'] and wzd_obj['warehouse_id'][0]:
                report_name += _("wh.")

            if wzd_obj['xls_ok'] == True:
                report_name += _(".xls")
            else:
                report_name += _(".pdf")

            data['form']['subdir_report'] = "/odoo/custom/addons/ib_reports/report/"
            data['form']['company_id'] = int(wzd_obj['company_id'][0])
            data['form']['fiscalyear_id'] = int(wzd_obj['fiscalyear_id'][0])
            data['form']['period_from'] = int(wzd_obj['period_from'][0])
            data['form']['period_to'] = int(wzd_obj['period_to'][0])
            data['model'] = "account.move"
            data['ids'] = self.pool.get(data['model']).search(cr,uid,[])
            return {
                'type': 'ir.actions.report.xml',
                'report_name': report_name,
                'datas': data,
            }


laporan_rincian_aktiva()

