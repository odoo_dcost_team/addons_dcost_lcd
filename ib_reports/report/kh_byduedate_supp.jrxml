<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="kh_byduedate" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="828" leftMargin="7" rightMargin="7" topMargin="8" bottomMargin="8" uuid="ffba5f8f-3fdf-4577-91e2-b5e4002b628d">
	<property name="ireport.zoom" value="1.051481698444183"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="date_start" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="date_stop" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="title" class="java.lang.String"/>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/home/baim/Documents/Customs/8.0/ib_reports/report/"]]></defaultValueExpression>
	</parameter>
	<parameter name="partner_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
    i.id AS inv_id, i.number, i.date_invoice, i.date_due, i.amount_total, p.name AS supplier,
    p.id AS supplier_id, apt.name AS payment_term, i.state, m.name AS journal_entry

FROM account_invoice AS i LEFT JOIN account_invoice_line AS l ON i.id=l.invoice_id
    LEFT JOIN res_partner AS p on i.partner_id=p.id LEFT JOIN account_payment_term AS apt ON
    i.payment_term=apt.id LEFT JOIN account_move AS m ON i.move_id=m.id

WHERE date_trunc('day',i.date_due) >= to_date($P{date_start}, 'YYYY-MM-DD') AND
    date_trunc('day',i.date_due) <= to_date($P{date_stop}, 'YYYY-MM-DD') AND i.state='open'
    AND i.partner_id=$P{partner_id}

GROUP BY i.id, p.id, apt.id, m.id

ORDER BY i.number ASC, i.date_invoice DESC]]>
	</queryString>
	<field name="inv_id" class="java.lang.Integer"/>
	<field name="number" class="java.lang.String">
		<fieldDescription><![CDATA[Number]]></fieldDescription>
	</field>
	<field name="date_invoice" class="java.sql.Date">
		<fieldDescription><![CDATA[Invoice Date]]></fieldDescription>
	</field>
	<field name="date_due" class="java.sql.Date">
		<fieldDescription><![CDATA[Due Date]]></fieldDescription>
	</field>
	<field name="amount_total" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[Total]]></fieldDescription>
	</field>
	<field name="supplier" class="java.lang.String">
		<fieldDescription><![CDATA[Supplier]]></fieldDescription>
	</field>
	<field name="supplier_id" class="java.lang.Integer">
		<fieldDescription><![CDATA[Supplier]]></fieldDescription>
	</field>
	<field name="payment_term" class="java.lang.String">
		<fieldDescription><![CDATA[Payment Terms]]></fieldDescription>
	</field>
	<field name="state" class="java.lang.String"/>
	<field name="journal_entry" class="java.lang.String"/>
	<variable name="amount_total_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{amount_total}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="75" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="69d71c65-2633-4561-ade4-915d4a40b582" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="723" height="56"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{title}]]></textFieldExpression>
			</textField>
			<image>
				<reportElement uuid="eeabe7d3-2b50-4c2a-b7cc-d9ff47705ac9" x="723" y="0" width="105" height="48"/>
				<imageExpression><![CDATA["/odoo/custom/addons/ib_reports/report/logo_dcost.png"]]></imageExpression>
			</image>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="5b8612a6-eeb1-4b09-b303-87e0c8b6e75d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="56" width="508" height="19"/>
				<textElement verticalAlignment="Middle">
					<font size="11" isBold="true"/>
					<paragraph leftIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA["Supplier: " + $F{supplier}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="34" splitType="Stretch">
			<staticText>
				<reportElement uuid="d7c511aa-4047-4ce3-beba-54df7d82688f" positionType="Float" stretchType="RelativeToTallestObject" x="38" y="0" width="130" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Number]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1bd9e532-984c-4c6f-a604-7a5e7e52854f" positionType="Float" stretchType="RelativeToTallestObject" x="168" y="0" width="65" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Invoice
Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3c544dad-0bc1-4a12-80c1-e9210a4fc5c4" positionType="Float" stretchType="RelativeToTallestObject" x="763" y="0" width="65" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Due Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="474a853b-ed4d-43b1-ad33-71c26fcc417c" positionType="Float" stretchType="RelativeToTallestObject" x="628" y="0" width="85" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total
Invoice]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="601c5b18-1b5b-46ec-8d9c-ec904d87f238" positionType="Float" stretchType="RelativeToTallestObject" x="508" y="0" width="120" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Journal]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="19861ad4-80a8-4d7d-9da4-3e3a71d5d3ad" positionType="Float" stretchType="RelativeToTallestObject" x="713" y="0" width="50" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Status Invoice]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="6dc8d22a-84c8-4aa0-83c4-baf9311fd61f" positionType="Float" stretchType="RelativeToTallestObject" x="233" y="0" width="275" height="17"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Invoice Reference]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="57e9b22f-63b4-4c02-9bc2-af6ebf6a2a52" positionType="Float" stretchType="RelativeToTallestObject" x="233" y="17" width="120" height="17"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[PO Number]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="5ffdf99d-6717-4285-a344-75f143951fc9" positionType="Float" stretchType="RelativeToTallestObject" x="353" y="17" width="65" height="17"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[PO Date]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1e586958-ddf9-4f04-a63f-89c9bcf3ea0a" positionType="Float" stretchType="RelativeToTallestObject" x="418" y="17" width="90" height="17"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Amount]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="6e448818-86a3-406a-ab55-8e36d87d7e24" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="38" height="34"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[No]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="25" splitType="Stretch">
			<frame>
				<reportElement uuid="c1cc4893-7f53-4ba5-928f-26c572c94408" positionType="Float" stretchType="RelativeToTallestObject" x="508" y="0" width="120" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="a14ecfa5-3def-4d8b-94dd-eb2762a6cf47" positionType="Float" stretchType="RelativeToTallestObject" x="628" y="0" width="85" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="1b5aec45-a5ac-4f23-9a12-ed6ec526fb42" positionType="Float" stretchType="RelativeToTallestObject" x="713" y="0" width="50" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="85cbc46b-80e1-4c0c-bb7b-b81f7e57688f" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="38" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="485e662b-7d67-4c5a-84e2-83cf2305c45d" positionType="Float" stretchType="RelativeToTallestObject" x="38" y="0" width="130" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="275f8e8d-fb05-4921-843c-5f2eced5ce60" positionType="Float" stretchType="RelativeToTallestObject" x="168" y="0" width="65" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="34158ecb-7ad8-451e-9f6e-8f56ff13a2fb" positionType="Float" stretchType="RelativeToTallestObject" x="763" y="0" width="65" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<subreport>
				<reportElement uuid="0dbbaa4f-7468-40ae-868a-aeb31ca8e96e" positionType="Float" stretchType="RelativeToTallestObject" x="233" y="0" width="275" height="25"/>
				<subreportParameter name="invoice_id">
					<subreportParameterExpression><![CDATA[$F{inv_id}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "kh_byduedate_subreport1.jasper"]]></subreportExpression>
			</subreport>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="dcab7380-e73b-428e-bf55-d2933ae46e59" positionType="Float" stretchType="RelativeToTallestObject" x="38" y="0" width="130" height="25"/>
				<textElement verticalAlignment="Top">
					<paragraph leftIndent="3" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{number}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement uuid="8e570e21-8d59-4eb0-b4e6-65851cc4c4d6" positionType="Float" stretchType="RelativeToTallestObject" x="168" y="0" width="65" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date_invoice}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement uuid="76380d5c-2a81-4847-9a3f-c8beabb868c2" positionType="Float" stretchType="RelativeToTallestObject" x="763" y="0" width="65" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{date_due}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
				<reportElement uuid="5138c4e2-deee-425c-bf5c-75d6267b83a0" positionType="Float" stretchType="RelativeToTallestObject" x="628" y="0" width="85" height="25"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<paragraph rightIndent="2" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{amount_total}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="24ee7cfd-b5fc-4804-933c-51ed444457e3" positionType="Float" stretchType="RelativeToTallestObject" x="508" y="0" width="120" height="25"/>
				<textElement verticalAlignment="Top">
					<paragraph leftIndent="3" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{journal_entry}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="6c86de5f-e5a2-4b46-8dd5-28d1aa5136fd" positionType="Float" stretchType="RelativeToTallestObject" x="713" y="0" width="50" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph leftIndent="3" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{state}.equalsIgnoreCase("draft") ?
  "Draft" :
    ($F{state}.equalsIgnoreCase("open") ?
      "Open" :
        ($F{state}.equalsIgnoreCase("paid") ?
          "Paid" :
            ($F{state}.equalsIgnoreCase("cancel") ?
              "Cancelled" : "None"))))]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="8d9e8234-9b3f-4fef-af34-11ef7d634efe" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="38" height="25"/>
				<textElement textAlignment="Center">
					<paragraph spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{REPORT_COUNT}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="16" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy HH.mm.ss" isBlankWhenNull="true">
				<reportElement uuid="38499b3f-a507-4200-9093-cdd4319bf343" x="0" y="0" width="168" height="16"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="decd3520-0659-4c9d-96f6-e8a2c282530c" x="708" y="0" width="80" height="16"/>
				<textElement textAlignment="Right">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="742c7cd8-50a4-4149-914c-962e96aa202c" x="788" y="0" width="40" height="16"/>
				<textElement>
					<font size="8"/>
					<paragraph rightIndent="2"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="20">
			<frame>
				<reportElement uuid="5b1c624d-5671-4666-a797-fa2d088496e0" positionType="Float" stretchType="RelativeToTallestObject" x="418" y="0" width="90" height="20" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
				<reportElement uuid="f72226b5-9ff8-409b-91ac-584cccea2cc0" positionType="Float" stretchType="RelativeToTallestObject" x="628" y="0" width="85" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{amount_total_1}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="7dbf83a7-cb07-42e4-998a-33ca69175cfb" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="233" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand Total ("+$F{supplier}+")"]]></textFieldExpression>
			</textField>
			<frame>
				<reportElement uuid="46969399-6242-4e80-9a49-510f2862e7c8" positionType="Float" stretchType="RelativeToTallestObject" x="713" y="0" width="115" height="20" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<staticText>
				<reportElement uuid="cbfe38e4-4acb-476a-868c-801ccbaaace0" positionType="Float" stretchType="RelativeToTallestObject" x="508" y="0" width="120" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph rightIndent="8"/>
				</textElement>
				<text><![CDATA[Total Invoice:]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="30a263d7-87f2-48d9-8ac7-7180a89cbde9" positionType="Float" stretchType="RelativeToTallestObject" x="233" y="0" width="185" height="20"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph rightIndent="8"/>
				</textElement>
				<text><![CDATA[Total PO:]]></text>
			</staticText>
			<subreport>
				<reportElement uuid="4a5eee85-059c-4d3d-8ace-fdb455ac1218" positionType="Float" stretchType="RelativeToTallestObject" x="418" y="0" width="90" height="20"/>
				<subreportParameter name="partner_id">
					<subreportParameterExpression><![CDATA[$P{partner_id}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="date_stop">
					<subreportParameterExpression><![CDATA[$P{date_stop}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="date_start">
					<subreportParameterExpression><![CDATA[$P{date_start}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "kh_byduedate_supp_subreport1.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</summary>
</jasperReport>
