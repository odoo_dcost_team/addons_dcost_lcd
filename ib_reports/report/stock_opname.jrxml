<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="inventory_movement" language="groovy" pageWidth="595" pageHeight="842" columnWidth="581" leftMargin="7" rightMargin="7" topMargin="12" bottomMargin="12" uuid="ffba5f8f-3fdf-4577-91e2-b5e4002b628d">
	<property name="ireport.zoom" value="1.2722928551174646"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="location_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="start_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="end_date" class="java.lang.String">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<parameter name="title" class="java.lang.String"/>
	<parameter name="company_id" class="java.lang.Integer">
		<defaultValueExpression><![CDATA[]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	ROW_NUMBER() OVER(ORDER BY ending.code DESC) AS num,
	ending.product_id, ending.name, ending.code,
	ending.uom, ending.status as state, ending.categ,
	COALESCE(sum(qty_begining),0) as qty_begining,
	COALESCE(sum(value_qty_begining),0) as value_qty_begining,
	COALESCE(sum(qty_purchase),0) as qty_purchase,
	COALESCE(sum(value_qty_purchase),0) AS value_qty_purchase,
	COALESCE(sum(qty_sales),0) as qty_sales,
	COALESCE(sum(value_qty_sales),0) AS value_qty_sales,
	COALESCE(sum(qty_scrap),0) as qty_scrap,
	COALESCE(sum(value_qty_scrap),0) as value_qty_scrap,
	COALESCE(sum(qty_ending),0) as qty_ending,
	COALESCE(sum(value_qty_ending),0) as value_qty_ending

FROM (SELECT product_id, name, code, uom, categ, status,
			sum(product_qty_in - product_qty_out) as qty_ending,
			sum(value_qty_in - value_qty_out) as value_qty_ending
		FROM (SELECT sm.product_id,pt.name, pp.default_code as code, pu.name as uom,
				CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
				pc.name as categ,
				COALESCE(sum(sm.product_uom_qty),0) AS product_qty_in,
				COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_in,
				0 AS product_qty_out,  0 AS value_qty_out
		      FROM stock_picking sp
                LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                LEFT JOIN stock_location sl ON sm.location_id = sl.id
				LEFT JOIN product_uom as pu on sm.product_uom=pu.id
				LEFT JOIN product_category as pc on pt.categ_id=pc.id
				LEFT JOIN stock_location as ld ON sm.location_dest_id = ld.id
			WHERE date_trunc('day',sm.date) <= to_date($P{end_date}, 'YYYY-MM-DD')
                AND sm.state = 'done' AND sm.company_id=$P{company_id} AND pt.uom_id=pu.id
				AND sl.active=true AND sl.usage not like '%internal%'
				AND ld.usage like '%internal%' AND ld.active=true
            GROUP BY sm.product_id, pt.name, pp.default_code,
				pu.name, pp.active, pc.name

            UNION

            SELECT sm.product_id, pt.name, pp.default_code as code, pu.name as uom,
			  	CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
			  	pc.name as categ, 0 AS product_qty_in, 0 AS value_qty_in,
		    	COALESCE(sum(sm.product_uom_qty),0) AS product_qty_out,
				COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_out
            FROM stock_picking sp
                LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                LEFT JOIN stock_location sl ON sm.location_id = sl.id
				LEFT JOIN product_uom as pu on sm.product_uom=pu.id
				LEFT JOIN product_category as pc on pt.categ_id=pc.id
				LEFT JOIN stock_location as ld ON sm.location_dest_id = ld.id
			WHERE date_trunc('day',sm.date) <= to_date($P{end_date}, 'YYYY-MM-DD')
                AND sm.state = 'done' AND sm.company_id=$P{company_id}
                AND sl.active=true AND sl.usage like '%internal%' AND pt.uom_id=pu.id
				AND ld.active=true AND ld.usage not like '%internal%'
            GROUP BY sm.product_id, pt.name, pp.default_code,
				pu.name, pp.active, pc.name) ending
		GROUP BY product_id, name, code, uom, categ, status) ending

	LEFT JOIN (
		SELECT sm.product_id, pt.name, pp.default_code as code,
			CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
			pu.name as uom, pc.name as categ,
			COALESCE(sum(sm.product_uom_qty),0) AS qty_purchase,
			COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_purchase
		FROM stock_picking sp
			LEFT JOIN stock_move sm ON sm.picking_id = sp.id
			LEFT JOIN product_product pp ON sm.product_id = pp.id
            LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
            LEFT JOIN stock_location sl ON sm.location_id = sl.id
			LEFT JOIN product_uom as pu on sm.product_uom=pu.id
			LEFT JOIN product_category as pc on pt.categ_id=pc.id
			LEFT JOIN stock_location ld ON sm.location_dest_id = ld.id
		WHERE date_trunc('day',sm.date) >= to_date($P{start_date}, 'YYYY-MM-DD')
            AND date_trunc('day',sm.date) <= to_date($P{end_date}, 'YYYY-MM-DD')
			AND sm.company_id=$P{company_id} AND pt.uom_id=pu.id AND sm.state = 'done'
			AND sl.usage like '%supplier%' AND sl.active=true
			AND ld.usage like '%internal%' AND ld.active=true
		GROUP BY sm.product_id, pt.name, pp.default_code, pu.name,
			pp.active, pc.name) purchases on
			ending.product_id = purchases.product_id

	LEFT JOIN (
		SELECT sm.product_id, pt.name, pp.default_code as code, pu.name as uom,
			CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
			pc.name as categ, COALESCE(sum(sm.product_uom_qty),0) AS qty_sales,
			COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_sales
		FROM stock_picking sp
			LEFT JOIN stock_move sm ON sm.picking_id = sp.id
			LEFT JOIN product_product pp ON sm.product_id = pp.id
			LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
            LEFT JOIN stock_location sl ON sm.location_id = sl.id
			LEFT JOIN product_uom as pu on sm.product_uom=pu.id
			LEFT JOIN product_category as pc on pt.categ_id=pc.id
			LEFT JOIN stock_location ld ON sm.location_dest_id = ld.id
		WHERE date_trunc('day',sm.date) >= to_date($P{start_date}, 'YYYY-MM-DD')
            AND date_trunc('day',sm.date) <= to_date($P{end_date}, 'YYYY-MM-DD')
			AND sm.company_id=$P{company_id} AND pt.uom_id=pu.id AND sm.state = 'done'
			AND ld.usage like '%customer%' AND ld.active=true
			AND sl.usage = 'internal' AND sl.active=true
        GROUP BY sm.product_id, pt.name, pp.default_code,
			pu.name, pp.active, pc.name) sales on
			ending.product_id = sales.product_id

	LEFT JOIN (
		SELECT sm.product_id, pt.name, pp.default_code as code,
			CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
			pu.name as uom, pc.name as categ,
			COALESCE(sum(sm.product_uom_qty),0) AS qty_scrap,
			COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_scrap
		FROM stock_picking sp
			LEFT JOIN stock_move sm ON sm.picking_id = sp.id
			LEFT JOIN product_product pp ON sm.product_id = pp.id
            LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
            LEFT JOIN stock_location sl ON sm.location_id = sl.id
			LEFT JOIN product_uom as pu on sm.product_uom=pu.id
			LEFT JOIN product_category as pc on pt.categ_id=pc.id
			LEFT JOIN stock_location ld ON sm.location_dest_id = ld.id
		WHERE date_trunc('day',sm.date) >= to_date($P{start_date}, 'YYYY-MM-DD')
            AND date_trunc('day',sm.date) <= to_date($P{end_date}, 'YYYY-MM-DD')
			AND sm.company_id=$P{company_id} AND sm.state = 'done' AND pt.uom_id=pu.id
			AND sl.usage not like '%inventory%' AND sl.active=true
			AND ld.usage like '%inventory%' AND ld.scrap_location=true
		GROUP BY sm.product_id, pt.name, pp.default_code, pu.name,
			pp.active, pc.name) scrapped on
			ending.product_id = scrapped.product_id

	LEFT JOIN (
		SELECT product_id, name, code, uom, categ, status,
			sum(product_qty_in - product_qty_out) as qty_begining,
			sum(value_qty_in - value_qty_out) as value_qty_begining
		FROM (SELECT sm.product_id, pt.name, pp.default_code as code,
				pu.name as uom, pc.name as categ,
				CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
				 0 AS product_qty_out, 0 AS value_qty_out,
				COALESCE(sum(sm.product_uom_qty),0) AS product_qty_in,
				COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_in
			  FROM stock_picking sp
                LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                LEFT JOIN stock_location sl ON sm.location_id = sl.id
				LEFT JOIN product_uom as pu on sm.product_uom=pu.id
				LEFT JOIN product_category as pc on pt.categ_id=pc.id
				LEFT JOIN stock_location ld ON sm.location_dest_id = ld.id
              WHERE date_trunc('day',sm.date) < to_date($P{start_date}, 'YYYY-MM-DD')
                AND sm.state = 'done' AND sm.company_id=$P{company_id} AND pt.uom_id=pu.id
				AND sl.active=true AND sl.usage not like '%internal%'
				AND ld.active=true AND ld.usage like '%internal%'
              GROUP BY sm.product_id, pt.name, pp.default_code,
				pu.name, pp.active, pc.name

              UNION

              SELECT sm.product_id, pt.name, pp.default_code as code,
				pu.name as uom, pc.name as categ,
				CASE WHEN pp.active=true THEN 'Active' ELSE 'Non Active' END AS status,
                COALESCE(sum(sm.product_uom_qty),0) AS product_qty_out,
				COALESCE(sum(sm.product_uom_qty*sm.price_unit),0) as value_qty_out,
				0 AS product_qty_in, 0 AS value_qty_in
              FROM stock_picking sp
                LEFT JOIN stock_move sm ON sm.picking_id = sp.id
                LEFT JOIN product_product pp ON sm.product_id = pp.id
                LEFT JOIN product_template pt ON pp.product_tmpl_id = pt.id
                LEFT JOIN stock_location sl ON sm.location_id = sl.id
				LEFT JOIN product_uom as pu on sm.product_uom=pu.id
				LEFT JOIN product_category as pc on pt.categ_id=pc.id
				LEFT JOIN stock_location ld ON sm.location_dest_id = ld.id
              WHERE date_trunc('day',sm.date) < to_date($P{start_date}, 'YYYY-MM-DD')
                AND sm.state = 'done' AND sm.company_id=$P{company_id}
				AND sl.usage like '%internal%' AND sl.active=true AND pt.uom_id=pu.id
				AND ld.active=true AND ld.usage not like '%internal%'
              GROUP BY sm.product_id, pt.name, pp.default_code,
				pu.name, pp.active, pc.name) begining
		GROUP BY product_id, name, code, uom, categ, status) begining on
			ending.product_id = begining.product_id

GROUP BY ending.product_id, ending.name, ending.code, ending.uom,
	ending.categ, ending.status]]>
	</queryString>
	<field name="num" class="java.lang.Long"/>
	<field name="product_id" class="java.lang.Integer">
		<fieldDescription><![CDATA[Product]]></fieldDescription>
	</field>
	<field name="name" class="java.lang.String"/>
	<field name="code" class="java.lang.String">
		<fieldDescription><![CDATA[Code]]></fieldDescription>
	</field>
	<field name="uom" class="java.lang.String"/>
	<field name="state" class="java.lang.String"/>
	<field name="categ" class="java.lang.String"/>
	<field name="qty_begining" class="java.math.BigDecimal"/>
	<field name="value_qty_begining" class="java.lang.Double"/>
	<field name="qty_purchase" class="java.math.BigDecimal"/>
	<field name="value_qty_purchase" class="java.lang.Double"/>
	<field name="qty_sales" class="java.math.BigDecimal"/>
	<field name="value_qty_sales" class="java.lang.Double"/>
	<field name="qty_scrap" class="java.math.BigDecimal"/>
	<field name="value_qty_scrap" class="java.lang.Double"/>
	<field name="qty_ending" class="java.math.BigDecimal"/>
	<field name="value_qty_ending" class="java.lang.Double"/>
	<variable name="qty_begining_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty_begining}]]></variableExpression>
	</variable>
	<variable name="value_qty_begining_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{value_qty_begining}]]></variableExpression>
	</variable>
	<variable name="qty_purchase_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty_purchase}]]></variableExpression>
	</variable>
	<variable name="value_qty_purchase_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{value_qty_purchase}]]></variableExpression>
	</variable>
	<variable name="qty_sales_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty_sales}]]></variableExpression>
	</variable>
	<variable name="value_qty_sales_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{value_qty_sales}]]></variableExpression>
	</variable>
	<variable name="qty_ending_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty_ending}]]></variableExpression>
	</variable>
	<variable name="value_qty_ending_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{value_qty_ending}]]></variableExpression>
	</variable>
	<variable name="qty_scrap_1" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty_scrap}]]></variableExpression>
	</variable>
	<variable name="value_qty_scrap_1" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$F{value_qty_scrap}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="60" splitType="Stretch">
			<image>
				<reportElement uuid="e4bb29ec-2608-4966-99cb-9c78afc4b9cd" x="466" y="0" width="115" height="55"/>
				<imageExpression><![CDATA["/odoo/custom/addons/ib_reports/report/logo_dcost.png"]]></imageExpression>
			</image>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="69d71c65-2633-4561-ade4-915d4a40b582" x="-44" y="0" width="510" height="55"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{title}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement uuid="93274701-cf89-42bb-9091-d411e2bb817c" positionType="Float" stretchType="RelativeToTallestObject" x="235" y="0" width="60" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Category]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="bb320fea-e2c1-4937-a3f8-97dde69f1164" positionType="Float" stretchType="RelativeToTallestObject" x="26" y="0" width="160" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Product]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c7fb8507-24d4-403b-84bf-1a943967f393" positionType="Float" stretchType="RelativeToTallestObject" x="186" y="0" width="49" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e782b324-e718-440f-9921-3663fcc529aa" x="295" y="0" width="130" height="15"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Quantity]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="9d8b6660-a26c-43c5-945d-a161a53cc99c" x="425" y="0" width="68" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Difference
(Selisih)]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="0a4dfb60-6830-46cc-b009-6caf3d7b97e6" x="360" y="15" width="65" height="15"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Physical]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="4a09de8e-1cc9-4c4b-97bc-d2c3719990e9" x="295" y="15" width="65" height="15"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Data]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="332a6f9f-a746-4a17-898b-1c828e215758" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="26" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[No]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="95883053-7146-48cf-9049-06e49a347b9a" x="493" y="0" width="88" height="30"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Keterangan]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="25" splitType="Stretch">
			<frame>
				<reportElement uuid="222787eb-f595-4993-a1d2-4ed51746ddb7" positionType="Float" stretchType="RelativeToTallestObject" x="235" y="0" width="60" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="9326f71d-e090-436e-843c-f744b57fe1d0" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="26" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="11590ec3-c608-4b59-bd7f-76293931d1db" positionType="Float" stretchType="RelativeToTallestObject" x="186" y="0" width="49" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="6107b09b-78dd-4c1c-be63-d97121d444bf" positionType="Float" stretchType="RelativeToTallestObject" x="295" y="0" width="65" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="70db4c97-b65c-4841-8d0e-796f0728a82d" positionType="Float" stretchType="RelativeToTallestObject" x="360" y="0" width="65" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="f313e2c8-2fdf-4689-8f93-e11b757d1898" positionType="Float" stretchType="RelativeToTallestObject" x="425" y="0" width="68" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="dc2bbf8b-8c28-4eae-a7ef-927c9ed4b2bc" positionType="Float" stretchType="RelativeToTallestObject" x="26" y="0" width="160" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="9af93dad-a308-421a-b78a-6360af55a17d" positionType="Float" stretchType="RelativeToTallestObject" x="493" y="0" width="88" height="25" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<textField isStretchWithOverflow="true" pattern="#,##0.0;(-#,##0.0)" isBlankWhenNull="true">
				<reportElement uuid="ec5c377f-02cf-4787-aacf-37737782cf4e" positionType="Float" stretchType="RelativeToTallestObject" x="295" y="0" width="65" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty_ending}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00;(#,##0.00)" isBlankWhenNull="true">
				<reportElement uuid="bfe6a873-b4ea-42e8-a138-760cb6d24341" positionType="Float" stretchType="RelativeToTallestObject" x="186" y="0" width="49" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph leftIndent="2" rightIndent="1" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{state}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="35b02f33-a622-4fbf-8613-e66776d45522" positionType="Float" stretchType="RelativeToTallestObject" x="235" y="0" width="60" height="25"/>
				<textElement textAlignment="Center">
					<paragraph leftIndent="2" rightIndent="1" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{categ}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="4f0588f4-a2dd-4934-9f98-5d4449656535" positionType="Float" stretchType="RelativeToTallestObject" x="26" y="0" width="160" height="25"/>
				<textElement verticalAlignment="Top">
					<paragraph leftIndent="3" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{name} + "\n[Satuan: "+$F{uom}+"]"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="8eb38f53-ae0b-4bfa-9d33-9a9eff58d75d" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="26" height="25"/>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<paragraph spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{num}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="15" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="83602b87-a1c1-4576-aaad-bb2dfbd49619" positionType="Float" stretchType="RelativeToTallestObject" x="461" y="0" width="80" height="15"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement uuid="a85cbb92-4e54-423c-8aca-e3f8d9ff6c7c" positionType="Float" stretchType="RelativeToTallestObject" x="541" y="0" width="40" height="15"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="8"/>
					<paragraph rightIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy HH.mm.ss" isBlankWhenNull="true">
				<reportElement uuid="03857dcb-d016-4e43-811b-0ec29ba0fb3e" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="221" height="15"/>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="23" splitType="Stretch">
			<staticText>
				<reportElement uuid="e120d0af-ded1-49a0-b49a-a56e73def323" positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="295" height="23"/>
				<box>
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
					<paragraph leftIndent="3"/>
				</textElement>
				<text><![CDATA[TOTAL]]></text>
			</staticText>
			<elementGroup/>
			<textField isStretchWithOverflow="true" pattern="#,##0.0;(-#,##0.0)" isBlankWhenNull="true">
				<reportElement uuid="98d60ccd-acf2-4188-a710-6cea2eeedd24" positionType="Float" stretchType="RelativeToTallestObject" x="295" y="0" width="65" height="23"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<paragraph leftIndent="2" spacingBefore="2"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{qty_ending_1}]]></textFieldExpression>
			</textField>
			<frame>
				<reportElement uuid="2b6e2445-60ff-43fd-87f8-0e626b923236" positionType="Float" stretchType="RelativeToTallestObject" x="360" y="0" width="65" height="23" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="54c5fd19-5429-41f4-a8cf-9663a3657c0a" positionType="Float" stretchType="RelativeToTallestObject" x="493" y="0" width="88" height="23" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
			<frame>
				<reportElement uuid="3f9d359c-958e-4408-aa52-4cab8524267a" positionType="Float" stretchType="RelativeToTallestObject" x="425" y="0" width="68" height="23" isPrintWhenDetailOverflows="true"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
			</frame>
		</band>
	</summary>
</jasperReport>
