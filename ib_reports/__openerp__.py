# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
{
    "name": "Custom Report",
    "version": "0.1",
    "category": "Custom Module",
    "depends": [
        "base",
        "account",
        "stock",
        "jasper_reports",
        "sale",
        "ib_merge_supplier_invoices",
        "website_quote",
        "aa_analytic_otomatis",
        "ib_duplicate_purchase_sale",
        ##"ib_base_senlog",
    ],
    "author":"Ibrohim Binladin | +6283871909782 | ibradiiin@gmail.com",
    "website": "http://ibrohimbinladin.wordpress.com",
    "description": """
Customization Report for D'Cost:
=========================================================
* Add button 'Print Surat Jalan/DO' on Picking Form (type:outgoing) 

* add button 'Print Receipt' on Picking Form (type:incoming) 

* Print Quo/SO untuk Logistik

* Bukti Pengeluaran Bank (account.invoice) 

* Invoice Receipt (account.invoice) - List PO dari Supplier Invoice
     
* Summary Report : Accounting, Stock, List Hutang, dll
""",
    "data": [
        "report/picking_report_view.xml",
        "wizard/wzd_report_view.xml",
        "report/report_view.xml",
        "report/action_reports.xml",
        "views/view.xml",
    ],
    "installable": True,
    "application": True,
}
