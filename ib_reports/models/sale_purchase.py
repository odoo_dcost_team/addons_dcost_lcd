# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################
from openerp import netsvc
from openerp.osv import fields, osv
#from openerp.tools.translate import _

BULAN = [(0,''), (1,'Januari'), (2,'Februari'),(3,'Maret'),(4,'April'), (5,'Mei'), (6,'Juni'),
        (7,'Juli'), (8,'Agustus'), (9,'September'),(10,'Oktober'), (11,'November'), (12,'Desember')]


class sale_order(osv.osv):
    _inherit = "sale.order"

    def print_quotation_dcost(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        context = context or {}
        for id in ids:
            order = self.pool.get('sale.order').browse(cr, uid, id, context=context)
            if order.state == 'draft':
                title = 'Quotation'
            else:
                title = 'Sales Order'
            odate = str(order.date_order).split(" ")
            date = str(odate[0]).split("-")
            date_indo = str(date[2]) + " " + str(BULAN[int(date[1])][1]).title() + " " + str(date[0]) + " " + str(
                odate[1])
            if order.state not in ('progress', 'manual', 'done', 'cancel'):
                self.write(cr, uid, ids, {'state': 'sent'})
        ##self.signal_workflow(cr, uid, ids, 'quotation_sent')
        datas = {
            'ids': ids,
            'model': 'sale.order',
            'form': self.read(cr, uid, ids[0], context=context),
            'quo_date': date_indo,
            'title': title,
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'sale.quotation.pdf',
            'datas': datas,
            'nodestroy': True
        }



class purchase_order(osv.osv):
    _inherit = "purchase.order"

    def print_purchase_order(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        context = context or {}
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'purchase.order', ids[0], 'send_rfq', cr)

        datas = {
            'ids': ids,
            'model': 'purchase.order',
            'form': self.read(cr, uid, ids[0], context=context),
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'purchase.order.pdf',
            'datas': datas,
            'nodestroy': True
        }







