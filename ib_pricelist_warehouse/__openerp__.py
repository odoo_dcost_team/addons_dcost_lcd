## -*- coding: utf-8 -*-
## © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com 
{
    "name": "Pricelist Warehouse",
    "version": "1.0",
    "category": "Custom Module",
    "website": "http://ibrohimbinladin.wordpress.com",
    "author": "Ibrohim Binladin | +62838-7190-9782 | ibradiiin@gmail.com",
    "description": """Modul kustom ini digunakan untuk pengelompokan daftar harga (pricelist) sesuai Outlet (Warehouse).""",
    "init_xml": [],
    "application": False,
    "installable": True,
    "depends": [
        "product",
        "purchase",
        "sale",
        "siu_import_pos",
        "ib_sequence_number",
    ],
    "data": [
        "views/view.xml",
    ],
    "demo_xml": [],
    "active": False,
}
