{
    "name":"HPP",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "category":"Custom Modules",
    "description": """
        To handle HPP.
    """,
    "depends":["base", "stock", "mrp", "account", "account_voucher", "analytic"],
    "init_xml":[],
    "demo_xml":[],
    "data":["hpp_view.xml"],
    "active":False,
    "installable":True
}
