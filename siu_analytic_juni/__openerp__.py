{
    "name":"Analytic Account DCOST",
    "version":"0.1",
    "author":"PT. SUMBER INTEGRASI UTAMA",
    "website":"http://myabcerp.com",
    "category":"Custom Modules",
    "description": """
        The base module to generate excel report.
    """,
    "depends":["base"],
    "init_xml":[],
    "demo_xml":[],
    "update_xml":[
            "Binjai.xml",
            "Malang.xml",
            "KrmatJ.xml",
            "Plkrya.xml",
    ],
    
    "active":False,
    "installable":True
}
