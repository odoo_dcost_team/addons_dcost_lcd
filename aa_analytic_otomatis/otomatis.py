import csv
import time
import base64
import tempfile
from datetime import date, datetime, timedelta
import cStringIO
from dateutil import parser
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class account_account(osv.osv):
    _inherit = 'account.account'
    _columns = {
        'analytic_line': fields.one2many('analytic.cabang', 'account_id', 'Group User'),
    }



class analytic_cabang(osv.osv):
    _name = "analytic.cabang"
    _columns = {
        'account_id': fields.many2one('account.account', 'Account', required=True, ondelete='cascade'),
        'name': fields.many2one('stock.warehouse', 'Outlet', required=True),
        'analytic_id': fields.many2one('account.analytic.account', 'Analytic', required=True, domain=[('type', '=', 'normal')]),
    }

class account_move_line(osv.osv):
    _inherit = 'account.move.line'
    
    def create(self, cr, uid, vals, context=None, check=True):
        user_obj = self.pool.get('res.users')
        account_obj = self.pool.get('account.account')
        ap = account_obj.browse(cr, uid, vals['account_id'])

        if not ap.active:
            raise osv.except_osv(_('Warning!'), _('Account is inactive.'))

        result = super(account_move_line, self).create(cr, uid, vals, context=context)
        # vals = {'analytic_account_id': False, 'tax_code_id': False, 'tax_amount': 0, 'name': 'r', 'journal_id': 5, 'company_id': 1, 'currency_id': False, 'credit': 100, 'date_maturity': False, 'period_id': 152, 'debit': 0, 'date': '2016-05-16', 'amount_currency': 0, 'partner_id': False, 'move_id': 265891, 'account_id': 7}
        yid = {}
        for ax in ap.analytic_line:
            yid[ax.name.id] = ax.analytic_id.id

        if 'user_id' in context and context['user_id']:
            uid = context['user_id']
        wh = user_obj.browse(cr, uid, uid).warehouse_id.id

        if yid.has_key(wh):
            self.write(cr, uid, [result], {'analytic_account_id': yid[wh]}, context=context)

        return result


