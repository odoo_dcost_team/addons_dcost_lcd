{
    "name":"Automatic Analytic",
    "version":"0.1",
    "author":"Muhammad Aziz - 087881071515",
    "category":"Custom Modules",
    "description": """
        To handle automatic analytic.
    """,
    "depends":["base", "account"],
    "init_xml":[],
    "demo_xml":[],
    ##"update_xml":["otomatis_view.xml", "security/ir.model.access.csv",],
    "data": ["otomatis_view.xml", "security/ir.model.access.csv",],
    "active":False,
    "installable":True
}
