# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################

import time
import xmlrpclib
import threading
from operator import attrgetter
from datetime import datetime
from openerp import pooler, SUPERUSER_ID
from openerp import models, api, fields as Fields
from openerp.osv import osv, orm, fields
from openerp.tools.translate import _
#from openerp.osv.fields import related
from openerp.exceptions import except_orm

db_groceries = ['LOGISTIK','SENLOGPB5', 'PB5', 'SENLOG', 'SENKIT']
db_outlets = ['PB1', 'PB', 'OUTLET', 'PEBO']

class RPCProxyOne(object):
    def __init__(self, server, ressource, user_id):
        self.server = server
        self.user_id = user_id
        local_url = 'http://%s:%d/xmlrpc/common' % (server.server_url,
                                                    server.server_port)
        rpc = xmlrpclib.ServerProxy(local_url)
        self.uid = rpc.login(server.server_db, server.login, server.password)
        local_url = 'http://%s:%d/xmlrpc/object' % (server.server_url,
                                                    server.server_port)
        self.rpc = xmlrpclib.ServerProxy(local_url)
        self.ressource = ressource

    def __getattr__(self, name):
        RPCProxy(self.server, self.user_id)
        server_db = self.server.server_db
        server_pass = self.server.password
        resource = self.ressource
        return lambda cr, uid, *args, **kwargs: self.rpc.execute(server_db,
                                                                 self.user_id or self.uid,
                                                                 server_pass,
                                                                 resource,
                                                                 name, *args)


class RPCProxy(object):
    def __init__(self, server, user):
        self.server = server
        self.user = user

    def get(self, ressource):
        return RPCProxyOne(self.server, ressource, self.user)


class DuplicateOrder(models.TransientModel):
    """Duplication process of purchasing outlets to sale of senlog """
    _name = 'duplicate.order'
    _description = "Duplicate Purchase Order To Sales Order"

    @api.v7
    def _check_order_list(self, cr, uid, context):
        if context.get('active_model', '') in ('purchase.order','sale.order'):
            ids = context['active_ids']
            fields = ['name', 'state', 'company_id', 'partner_id', 'related_docs']
            if context.get('active_model') == 'purchase.order':
                order_obj = self.pool.get('purchase.order')
                fields.append('sales_order')
            elif context.get('active_model') == 'sale.order':
                order_obj = self.pool.get('sale.order')
                fields.append('purchase_order')
            records = order_obj.read(cr, uid, ids, fields)
            for r in records:
                if context.get('active_model') == 'purchase.order':
                    if len(str(r['partner_id'][1]).strip().split(" ")) > 1 and \
                        str(r['partner_id'][1]).split(" ")[1] not in ('SENLOG', 'SENKIT', 'senkit', 'senlog', 'Senlog', 'Senkit'):
                        raise orm.except_orm(_('Warning !!!'),
                                             _('Partners purchase order is not SENLOG or SENKIT.\n[result: %s]') % (
                                             r['partner_id'][1],))
                    # if r['sales_order'] or r['related_docs']:
                    #     raise orm.except_orm(_('Warning !!!'), _('At least one of the selected purchase order is already has sales order (SO SENLOG).\n%s') % r['name'])
                if context.get('active_model') == 'sale.order':
                    cust_groceries = self.pool.get('res.partner').read(cr, uid, [r['partner_id'][0]], ['code'])[0]
                    if not cust_groceries['code']:
                        raise orm.except_orm(_('Warning !!!'), _('Kode customer didalam form tidak valid (Customer harus memiliki Partner Code yang unik masing-masing.'))
                    # if r['purchase_order'] or r['related_docs']:
                    #     raise orm.except_orm(_('Warning !!!'), _('At least one of the selected sale order is already has sales order (SO SENLOG).\n%s') % r['name'])
                if r['state'] == 'cancel':      #not in('approved','except_invoice','except_picking'):
                    raise orm.except_orm(_('Warning !!!'), _('At least one of the selected purchase order is %s! \nPurchase Order status allowed is Draft or Confirmed.') % r['state'])
                if (r['company_id'] != records[0]['company_id']):
                    raise orm.except_orm(_('Warning !!!'), _('Not all Purchase Order are at the same company!'))

        return {}

    @api.v7
    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        if context is None:
            context = {}
        res = super(DuplicateOrder, self).fields_view_get(cr, uid, view_id=view_id, view_type=view_type, context=context, toolbar=toolbar, submenu=False)
        self._check_order_list(cr, uid, context)
        return res

    @api.model
    def _default_server_destination(self):
        if self.env.cr.dbname in db_outlets:
            return self.env['duplicate.config.server'].search([('server_db', 'in', tuple(db_groceries))], limit=1).id
        elif self.env.cr.dbname in db_groceries:
            return self.env['duplicate.config.server'].search([('server_db', 'in', tuple(db_outlets))], limit=1).id
        else:
            return False

    server_url = Fields.Many2one('duplicate.config.server', "Server URL [Dest.]",
                        default=_default_server_destination, required=True)
    user_id = Fields.Many2one('res.users', "Send Result To",
                        default=lambda self: self.env.user)

    report = []
    report_total = 0
    report_create = 0
    report_write = 0

    @api.model
    def input(self, ids, value):
        return value

    @api.model
    def duplication_purchase_to_sale(self, server): ### Duplicate PO to SO ###
        context = dict(self._context or {})
        active_ids = self._context.get('active_ids', [])
        pool = pooler.get_pool(self.env.cr.dbname)
        uid = self.user_id.id or SUPERUSER_ID

        self.meta = {}
        dest_sale = {}
        pool_src = pool
        pool_dest = RPCProxy(server, SUPERUSER_ID)

        purchase_obj = pool_src.get('purchase.order')
        users_obj = pool_dest.get('res.users')
        purchase = purchase_obj.browse(self._cr, SUPERUSER_ID, active_ids)[0]
        if purchase.partner_id and purchase.partner_id.name:
            key = 'order' + str(purchase.partner_id.name.strip().split(" ")[1]).lower()
        elif self.env.user.login:
            key = str(self.env.user.login).lower()
        else:
            key = 'admin'
        real_uid = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)[0] or uid
        #pool_dest = RPCProxy(server, real_uid)

        sale_obj = pool_dest.get('sale.order')
        sale_line_obj = pool_dest.get('sale.order.line')
        history_src_obj = pool_src.get('duplicate.history')
        partner_obj = pool_dest.get('res.partner')
        warehouse_obj = pool_dest.get('stock.warehouse')
        sequence_obj = pool_dest.get('ir.sequence')
        history_obj = pool_dest.get('duplicate.history')
        po_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model','=','purchase.order')])
        po_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])
        so_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
        so_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])

        for po in purchase_obj.browse(self._cr, SUPERUSER_ID, active_ids):
            exist_oid = self.get_id(po_model_ids and po_model_ids[0], po.id) #check from history of duplication
            if exist_oid:
                dest_sale = sale_obj.read(self._cr, uid, [exist_oid], [])[0]
                if (exist_oid and dest_sale['state'] not in ('draft')):  #po.sales_order or ,'sent'
                    raise except_orm(_('Warning!'),
                                     _("%s has already created the sales order in database LOGISTIK.") % (po.name)) #SENLOGPB5
            partner_ids = partner_obj.search(self._cr, SUPERUSER_ID, ['&','|',('customer', '=', True),
                                ('name','like','%'+str(po.picking_type_id.warehouse_id.outlet).replace('DCOST ','')+'%'),
                                ('code','=',po.picking_type_id.warehouse_id.code)])
            if not partner_ids:
                raise except_orm(_('Warning!'), _("Partner names (outlet name) are not available in the LOGISTIK."))
            else:
                partner_split = str(po.partner_id.name).split(" ") ##Partner name harus mengandung SENLOG or SENKIT
                if len(partner_split)>1 and (partner_split[1] not in ('SENLOG','SENKIT','senkit','senlog','Senlog','Senkit')):
                    raise except_orm(_('Warning!'), _("Suppliers on the PO form must contain 'SENLOG' or 'SENKIT'. - result: %s") % (po.partner_id.name,))
                warehouse_ids = warehouse_obj.search(self._cr, SUPERUSER_ID,
                                    [('name','like','%'+str(partner_split[1]).upper()+'%')], limit=1)
                warehouse_dest = warehouse_ids and warehouse_ids[0] or False
                wh_code = 'XXX'
                if warehouse_ids:
                    wh_code = warehouse_obj.read(self._cr, uid, warehouse_ids, ['code'])[0]['code']
                customer = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
                values = self._prepare_sale_order(po, customer, real_uid, warehouse_dest)

                if exist_oid: #write: sale_order senlog
                    sale_id = exist_oid
                    so_number = dest_sale['name']
                    if dest_sale['state'] == 'draft':
                        values.update({'purchase_order': True, 'related_docs': po.name or ''})
                        sale_obj.write(self._cr, real_uid, [sale_id], values)
                        pol_ids = []
                        for line in po.order_line:
                            product_id = self.get_product_details(pool_dest, line, 'product_id')
                            product_description = self.get_product_details(pool_dest, line, 'description')
                            uom_id = self.get_uom_id(pool_dest, line)
                            exist_lid = self.get_id(po_line_model_ids and po_line_model_ids[0], line.id)
                            line_vals = self._prepare_sale_order_line(line, sale_id, customer, real_uid, product_id,
                                                                      uom_id, product_description)
                            if not uom_id:
                                raise except_orm(_('Warning!'), _("Product UoM are not available in the PB1 Database."))
                            if exist_lid and product_id and uom_id:
                                dest_oline = sale_line_obj.read(self._cr, real_uid, [exist_lid], [])[0]
                                if dest_oline['state'] == 'draft':
                                    sale_line_obj.write(self._cr, real_uid, [exist_lid], line_vals)
                            else:
                                add_sale_line_id = sale_line_obj.create(self._cr, real_uid, line_vals)
                                self.create_duplicate_history(pool_src, pool_dest, po_line_model_ids and po_line_model_ids[0]
                                        or False, line.id or False, so_line_model_ids and so_line_model_ids[0] or False,
                                        add_sale_line_id or False, uid, real_uid)
                            if line.id not in pol_ids:
                                pol_ids.append(line.id)

                        if pol_ids:
                            sol_ids = sale_line_obj.search(self._cr, real_uid, [('order_id', '=', sale_id)])
                            history_dst_ids = history_obj.search(self._cr, real_uid,
                                        [('source_id','in',tuple(sol_ids)),('destination_id','not in',tuple(pol_ids))])
                            history_src_ids = history_src_obj.search(self._cr, uid,
                                        [('source_id', 'not in', tuple(pol_ids)),('destination_id', 'in', tuple(sol_ids))])
                            if history_dst_ids:
                                for res in history_obj.read(self._cr, real_uid, history_dst_ids, ['source_id']):
                                    sale_line_obj.unlink(self._cr, real_uid, [res['source_id']])
                            if history_src_ids or history_dst_ids:
                                self.remove_duplicate_history(pool_dest, pool_src, history_dst_ids, history_src_ids, real_uid, uid)

                        sale_obj.message_post(self._cr, real_uid, [sale_id], body=_("Sales Order has been changed"))
                        self.report_total += 1
                        self.report_write += 1
                    else:
                        raise except_orm(_('Warning!'), _("Data can not be changed or deleted because it is not a draft."))
                else: #create: sale_order senlog
                    sale_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'sale.order.dcost', context=context)
                    nomor_so = list(str(sale_order_number))  # SO/12345 -> SO/sunter/12345
                    nomor_so.insert(3, wh_code + "/")
                    so_number = ''.join(nomor_so)
                    values.update({'name': so_number})
                    sale_id = sale_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
                    for line in po.order_line:
                        product_id = self.get_product_details(pool_dest, line, 'product_id')
                        product_description = self.get_product_details(pool_dest, line, 'description')
                        uom_id = self.get_uom_id(pool_dest, line)
                        if not uom_id:
                            raise except_orm(_('Warning!'), _("Product UoM are not available in the PB1 Database."))
                        if product_id and uom_id:
                            line_vals = self._prepare_sale_order_line(line, sale_id, customer, real_uid, product_id, uom_id, product_description)
                            sale_line_id = sale_line_obj.create(self._cr, real_uid, line_vals)
                            self.create_duplicate_history(pool_src, pool_dest, po_line_model_ids and po_line_model_ids[0]
                                    or False, line.id or False, so_line_model_ids and so_line_model_ids[0] or False,
                                    sale_line_id or False, uid, real_uid)
                    sale_obj.message_post(self._cr, real_uid, [sale_id],
                                body=_("Sales Order created"), context={'mail_create_nolog': True})
                    self.create_duplicate_history(pool_src, pool_dest, po_model_ids and po_model_ids[0] or False, po.id
                            or False, so_model_ids and so_model_ids[0] or False, sale_id or False, uid, real_uid)
                    self.report_total += 1
                    self.report_create += 1

                sale_obj.button_dummy(self._cr, uid, [sale_id])
                purchase_obj.write(self._cr, uid, [po.id], {'sales_order': True, 'related_docs': so_number or ''})
                self.meta = {}

        return True

    # @api.model
    # def duplication_sale_to_purchase(self, server): #order_ids
    #     # server_id = self.env['duplicate.config.server'].search([('server_db', 'in', tuple(db_outlets))], limit=1).id
    #     # server = self.env['duplicate.config.server'].browse(server_id)
    #     context = dict(self._context or {})
    #     active_ids = self._context.get('active_ids', [])
    #     pool = pooler.get_pool(self.env.cr.dbname)
    #     uid = self.user_id.id or SUPERUSER_ID
    #
    #     self.meta = {}
    #     purchases = {}
    #     pool_src = pool
    #     pool_dest = RPCProxy(server, SUPERUSER_ID)
    #     purchase_order = {}
    #
    #     sale_obj = pool_src.get('sale.order')
    #     users_obj = pool_dest.get('res.users')
    #     sales = sale_obj.browse(self._cr, SUPERUSER_ID, active_ids or [])[0]  # active_ids
    #     if sales.partner_id and sales.partner_id.code:
    #         key = str('adm') + str(sales.partner_id.code.strip().lower())
    #     elif self.env.user.login:
    #         key = str(self.env.user.login).lower()
    #     else:
    #         key = 'admin'
    #     user_ids = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)
    #     real_uid = users_obj.read(self._cr, SUPERUSER_ID, user_ids, context=context)[0]['id'] or uid
    #     # pool_dest = RPCProxy(server, real_uid)
    #
    #     purchase_obj = pool_dest.get('purchase.order')
    #     purchase_line_obj = pool_dest.get('purchase.order.line')
    #     history_src_obj = pool_src.get('duplicate.history')
    #     partner_obj = pool_dest.get('res.partner')
    #     warehouse_obj = pool_dest.get('stock.warehouse')
    #     sequence_obj = pool_dest.get('ir.sequence')
    #     pick_type_obj = pool_dest.get('stock.picking.type')
    #     history_obj = pool_dest.get('duplicate.history')
    #     product_obj = pool_dest.get('product.product')
    #     so_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
    #     so_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])
    #     po_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order')])
    #     po_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])
    #
    #     for sale in sale_obj.browse(self._cr, SUPERUSER_ID, active_ids or []):  # order_ids
    #         # exist_oid = self.get_id(so_model_ids and so_model_ids[0], sale.id) #check from history of duplication
    #         exist_oid = purchase_obj.search(self._cr, real_uid, [('sale_id', '=', sale.id), ('state', '!=', 'cancel')])
    #         if exist_oid:
    #             purchases = purchase_obj.read(self._cr, real_uid, exist_oid, [])[0]
    #             if (exist_oid and purchases['state'] not in ('draft')):  # sale.purchase_order or ,'sent','bid'
    #                 raise except_orm(_('Warning!!!'),
    #                                  _(
    #                                      "Purchase Order (Outlet) berstatus '%s' dan tidak bisa diduplikasi. Status yang diperbolehkan yaitu draft, sent, bid.") %
    #                                  purchases['state'])
    #         partner_ids = partner_obj.search(self._cr, real_uid, ['&', '|', ('supplier', '=', True),
    #                                                               ('name', 'like', '%' + str(
    #                                                                   sale.warehouse_id.partner_id.name).strip().replace(
    #                                                                   'DCOST ', '')),
    #                                                               ('name', 'like',
    #                                                                '%' + str(sale.warehouse_id.name).strip().replace(
    #                                                                    'PB', ''))])
    #         if not partner_ids:
    #             raise except_orm(_('Warning!'), _("Partner name (Supplier) tidak tersedia di Database PB1."))
    #         else:
    #             wh_ids = warehouse_obj.search(self._cr, real_uid, [('code', '!=', ''), ('active', '=', True)])
    #             warehouses = warehouse_obj.read(self._cr, real_uid, wh_ids, context=context)  # [0]
    #             outlet_codes = [w['code'] for w in warehouses]
    #             if (not sale.partner_id.code) or (sale.partner_id.code not in outlet_codes):
    #                 raise except_orm(_('Warning!'), _(
    #                     "Partner code [Kode Customer SO] are not available in DB PB1 [Warehouse Code]. \nresult: %s") % (
    #                                  sale.partner_id.code,))
    #             warehouse_ids = warehouse_obj.search(self._cr, real_uid,
    #                                                  [('code', 'like', '%' + sale.partner_id.code + '%')],
    #                                                  limit=1) or []
    #             pick_type_ids = pick_type_obj.search(self._cr, real_uid,
    #                                                  [('warehouse_id', '=',
    #                                                    warehouse_ids and warehouse_ids[0] or False)], limit=1)
    #             picking_type = pick_type_obj.read(self._cr, real_uid, pick_type_ids, context=context)[0]
    #             supplier = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
    #             values = self._prepare_purchase_order(sale, supplier, pick_type_ids and pick_type_ids[0] or False,
    #                                                   picking_type['default_location_dest_id'], real_uid)
    #             if exist_oid:
    #                 purchase_id = exist_oid and exist_oid[0] or purchases['id']
    #                 if purchases['state'] in ('draft', 'sent', 'bid'):
    #                     purchase_obj.write(self._cr, real_uid, [purchase_id], values)
    #                     for line in sale.order_line:
    #                         product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                         product_description = self.get_product_details(pool_dest, line, 'description')
    #                         uom_id = self.get_uom_id(pool_dest, line)
    #                         exist_pol_id = purchase_line_obj.search(self._cr, real_uid, [('sale_line_id', '=', line.id),
    #                                                                                      ('state', '!=', 'cancel')])
    #                         # exist_lid = self.get_id(so_line_model_ids and so_line_model_ids[0], line.id)
    #                         account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                         analytic_id = self.get_anaytic_id(pool_dest, account_id,
    #                                                           warehouse_ids and warehouse_ids[0] or False)
    #                         line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id,
    #                                                                       product_description, analytic_id)
    #                         if not product_id:
    #                             raise except_orm(_('Warning!'), _("Produk tidak tersedia di Database PB1."))
    #                         if not uom_id:
    #                             raise except_orm(_('Warning!'),
    #                                              _("Satuan produk (UoM) tidak tersedia di Database PB1."))
    #                         if exist_pol_id and product_id and uom_id and (line.write_date > line.create_date):
    #                             dest_poline = purchase_line_obj.read(self._cr, uid, exist_pol_id, [])[0]
    #                             if dest_poline['state'] in ('draft', 'sent'):
    #                                 purchase_line_obj.write(self._cr, real_uid, exist_pol_id, line_vals)
    #                         else:
    #                             if product_id and uom_id:
    #                                 purchase_line_obj.create(self._cr, real_uid, line_vals)
    #                 else:
    #                     raise except_orm(_('Warning!'),
    #                                      _("Data can not be changed or deleted because it is not a draft."))
    #             else:  # create: purchase_order outlet
    #                 purchase_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'purchase.order.dcost',
    #                                                                   context=context)
    #                 nomor_po = list(str(purchase_order_number))  # PO/12345 -> PO/sunter/12345
    #                 nomor_po.insert(3, str(sale.partner_id.code) + "/")
    #                 po_number = ''.join(nomor_po)
    #                 values.update({'name': str(po_number), 'sale_id': sale.id})
    #                 # purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
    #                 new_lines = []
    #                 for line in sale.order_line:
    #                     product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                     product_description = self.get_product_details(pool_dest, line, 'description')
    #                     uom_id = self.get_uom_id(pool_dest, line)
    #                     # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
    #                     account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                     analytic_id = self.get_anaytic_id(pool_dest, account_id,
    #                                                       warehouse_ids and warehouse_ids[0] or False)
    #                     if not product_id:
    #                         raise except_orm(_('Warning!'), _("Produk tidak tersedia di Database PB1."))
    #                     if not uom_id:
    #                         raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1."))
    #                     if product_id and uom_id:
    #                         line_vals = self._prepare_purchase_order_line(line, False, product_id, uom_id,
    #                                                                       product_description, analytic_id)
    #                         new_lines.append((0, 0, line_vals))
    #
    #                 values.update({'order_line': new_lines})
    #                 if len(new_lines) == len(sale.order_line):
    #                     purchase_order[sale.id] = values
    #
    #     if purchase_order:
    #         for sale in sale_obj.browse(self._cr, SUPERUSER_ID, active_ids or []): #order_ids
    #             if sale.id in purchase_order.keys():
    #                 purchase_id = purchase_obj.create(self._cr, real_uid, purchase_order[sale.id],
    #                                                   context={'mail_create_nolog': True})
    #                 purchase_obj.button_dummy(self._cr, real_uid, [purchase_id])
    #                 sale_obj.write(self._cr, uid, [sale.id], {'purchase_order': True, 'purchase_id': purchase_id,
    #                                                           'related_docs': purchase_order[sale.id]['name'] or ''})
    #                 # purchase_data = purchase_obj.read(self._cr, real_uid, [purchase_id], context=context)[0]
    #                 # user = users_obj.read(self._cr, real_uid, [purchase_data['user_id']], context=context)[0]
    #                 # if str(user.login)[3:] != str(sales.partner_id.code.strip().lower()):
    #                 #     purchase_obj.write(self._cr, real_uid, [purchase_id], {'user_id': real_uid})
    #         self.meta = {}
    #
    #     return True

    @api.model
    def duplication_sale_to_purchase(self, server):
        context = dict(self._context or {})
        active_ids = self._context.get('active_ids', [])
        pool = pooler.get_pool(self.env.cr.dbname)
        uid = self.user_id.id or SUPERUSER_ID

        self.meta = {}
        purchases = {}
        pool_src = pool
        pool_dest = RPCProxy(server, SUPERUSER_ID)

        sale_obj = pool_src.get('sale.order')
        users_obj = pool_dest.get('res.users')
        sales = sale_obj.browse(self._cr, SUPERUSER_ID, active_ids)[0]
        if sales.partner_id and sales.partner_id.code:
            key = 'adm' + sales.partner_id.code.strip().lower()
        elif self.env.user.login:
            key = str(self.env.user.login).lower()
        else:
            key = 'admin'
        real_uid = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)[0] or uid
        #pool_dest = RPCProxy(server, real_uid)

        purchase_obj = pool_dest.get('purchase.order')
        purchase_line_obj = pool_dest.get('purchase.order.line')
        history_src_obj = pool_src.get('duplicate.history')
        partner_obj = pool_dest.get('res.partner')
        warehouse_obj = pool_dest.get('stock.warehouse')
        sequence_obj = pool_dest.get('ir.sequence')
        pick_type_obj = pool_dest.get('stock.picking.type')
        history_obj = pool_dest.get('duplicate.history')
        product_obj = pool_dest.get('product.product')
        so_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
        so_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])
        po_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order')])
        po_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])

        for sale in sale_obj.browse(self._cr, SUPERUSER_ID, active_ids):
            exist_oid = self.get_id(so_model_ids and so_model_ids[0], sale.id) #check from history of duplication
            if exist_oid:
                purchases = purchase_obj.read(self._cr, real_uid, [exist_oid], [])[0]
                if (exist_oid and purchases['state'] not in ('draft')):  #sale.purchase_order or ,'sent','bid'
                    raise except_orm(_('Warning!!!'),
                                     _("Purchase Order (Outlet) berstatus '%s' dan tidak bisa diduplikasi. Status yang diperbolehkan yaitu draft, sent, bid.") % purchases['state'])
            partner_ids = partner_obj.search(self._cr, real_uid, ['&','|',('supplier', '=', True),
                                ('name','like','%'+str(sale.warehouse_id.partner_id.name).strip().replace('DCOST ','')),
                                ('name','like','%'+str(sale.warehouse_id.name).strip().replace('PB',''))])
            if not partner_ids:
                raise except_orm(_('Warning!'), _("Partner name (Supplier) tidak tersedia di Database PB1."))
            else:
                wh_ids = warehouse_obj.search(self._cr, real_uid, [('code','!=','')])
                warehouses = warehouse_obj.read(self._cr, real_uid, wh_ids, context=context)  #[0]
                outlet_codes = [w['code'] for w in warehouses]
                if (not sale.partner_id.code) or (sale.partner_id.code not in outlet_codes):
                    raise except_orm(_('Warning!'), _("Partner code [Kode Customer SO] are not available in DB PB1 [Warehouse Code]. - result: %s") % (sale.partner_id.code,))
                warehouse_ids = warehouse_obj.search(self._cr, real_uid,
                                [('code', 'like', '%' + sale.partner_id.code + '%')], limit=1) or []
                pick_type_ids = pick_type_obj.search(self._cr, real_uid,
                                [('warehouse_id', '=', warehouse_ids and warehouse_ids[0] or False)], limit=1)
                picking_type = pick_type_obj.read(self._cr, real_uid, pick_type_ids, context=context)[0]
                supplier = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
                values = self._prepare_purchase_order(sale, supplier, pick_type_ids and pick_type_ids[0] or False,
                                                      picking_type['default_location_dest_id'], real_uid)
                if exist_oid:
                    purchase_id = exist_oid
                    po_number = purchases['name']
                    if purchases['state'] == 'draft':
                        sol_ids = []
                        purchase_obj.write(self._cr, real_uid, [purchase_id], values)
                        for line in sale.order_line:
                            product_id = self.get_product_details(pool_dest, line, 'product_id')
                            product_description = self.get_product_details(pool_dest, line, 'description')
                            uom_id = self.get_uom_id(pool_dest, line)
                            exist_lid = self.get_id(so_line_model_ids and so_line_model_ids[0], line.id)
                            #product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
                            account_id = self.get_product_details(pool_dest, line, 'account_id')
                            analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
                            line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id, product_description, analytic_id)
                            if not uom_id:
                                raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1\n[%s].") % line.name)
                            if not account_id:
                                raise except_orm(_('Warning!'),
                                                 _("Account ID not found in PB1 Database\n[%s].") % line.name)
                            if exist_lid and product_id and uom_id:
                                dest_oline = purchase_line_obj.read(self._cr, uid, [exist_lid], [])[0]
                                if dest_oline['state'] == 'draft':
                                    purchase_line_obj.write(self._cr, real_uid, [exist_lid], line_vals)
                            else:
                                add_pur_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
                                self.create_duplicate_history(pool_src, pool_dest, so_line_model_ids and so_line_model_ids[0]
                                    or False, line.id or False, po_line_model_ids and po_line_model_ids[0] or False,
                                    add_pur_line_id or False, uid, real_uid)
                            if line.id not in sol_ids:
                                sol_ids.append(line.id)

                        if sol_ids:
                            pol_ids = purchase_line_obj.search(self._cr, real_uid, [('order_id', '=', purchase_id)])
                            history_dst_ids = history_obj.search(self._cr, real_uid,
                                        [('source_id','in',tuple(pol_ids)),('destination_id','not in',tuple(sol_ids))])
                            history_src_ids = history_src_obj.search(self._cr, uid,
                                        [('source_id', 'not in', tuple(sol_ids)),('destination_id', 'in', tuple(pol_ids))])
                            if history_dst_ids:
                                for res in history_obj.read(self._cr, real_uid, history_dst_ids, ['source_id']):
                                    purchase_line_obj.unlink(self._cr, real_uid, [res['source_id']])
                            if history_src_ids or history_dst_ids:
                                self.remove_duplicate_history(pool_dest, pool_src, history_dst_ids, history_src_ids, real_uid, uid)
                        purchase_obj.message_post(self._cr, real_uid, [purchase_id], body=_("Purchase Order has been changed"))
                        self.report_total += 1
                        self.report_write += 1
                    else:
                        raise except_orm(_('Warning!'), _("Data can not be changed or deleted because it is not a draft."))
                else: #create: purchase_order outlet
                    purchase_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'purchase.order.dcost', context=context)
                    nomor_po = list(str(purchase_order_number))  #PO/12345 -> PO/sunter/12345
                    nomor_po.insert(3, str(sale.partner_id.code)+ "/")
                    po_number = ''.join(nomor_po)
                    values.update({'name': str(po_number)})
                    purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
                    for line in sale.order_line:
                        product_id = self.get_product_details(pool_dest, line, 'product_id')
                        product_description = self.get_product_details(pool_dest, line, 'description')
                        uom_id = self.get_uom_id(pool_dest, line)
                        # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
                        account_id = self.get_product_details(pool_dest, line, 'account_id')
                        analytic_id = self.get_anaytic_id(pool_dest, account_id, warehouse_ids and warehouse_ids[0] or False)
                        if not uom_id:
                            raise except_orm(_('Warning!'),
                                             _("Product UoM are not available in the PB1 Database\n[%s].") % line.name)
                        if not account_id:
                            raise except_orm(_('Warning!'),
                                             _("Account ID not found in PB1 Database\n[%s].") % line.name)
                        if product_id and uom_id:
                            line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id, product_description, analytic_id)
                            purchases_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
                            self.create_duplicate_history(pool_src, pool_dest, so_line_model_ids and so_line_model_ids[0]
                                    or False, line.id or False, po_line_model_ids and po_line_model_ids[0] or False,
                                    purchases_line_id or False, uid, real_uid)
                    purchase_obj.message_post(self._cr, real_uid, [purchase_id],
                                body=_("Purchase Order created"), context={'mail_create_nolog': True})
                    self.create_duplicate_history(pool_src, pool_dest, so_model_ids and so_model_ids[0] or False,
                            sale.id or False, po_model_ids and po_model_ids[0] or False, purchase_id or False, uid, real_uid)
                    self.report_total += 1
                    self.report_create += 1

                purchase_obj.button_dummy(self._cr, uid, [purchase_id])
                sale_obj.write(self._cr, uid, [sale.id], {'purchase_order': True, 'related_docs': po_number or ''})
                self.meta = {}

        return True

    @api.model
    def get_id(self, object_id, rec_id):
        pool = pooler.get_pool(self.env.cr.dbname)
        history_pool = pool.get('duplicate.history')
        rid = history_pool.search(self._cr, self.user_id.id,
                               [('src_model_id', '=', object_id),
                                ('source_id', '=', rec_id)])
        result = False
        if rid:
            result = history_pool.read(self._cr, self.user_id.id, rid,
                                    ['destination_id'])[0]['destination_id']
        return result

    @api.multi
    def duplicate_order(self):
        start_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
        duplicate_wzd = self.browse(self.ids)[0]
        server_conf = self.env['duplicate.config.server'].browse(duplicate_wzd.server_url.id) #destination-duplicate
        if server_conf.server_db in db_groceries or (self.env.cr.dbname in db_outlets):
            self.duplication_purchase_to_sale(server_conf)  ##duplicate from PO Outlet to SO senlog
        elif server_conf.server_db in db_outlets or (self.env.cr.dbname in db_groceries):
            self.duplication_sale_to_purchase(server_conf)  ##duplicate from SO senlog to PO Outlet
        end_date = time.strftime('%Y-%m-%d, %Hh %Mm %Ss')
        if duplicate_wzd.user_id:
            cr, uid, context = self.env.args
            res_dupli = pooler.get_pool(cr.dbname).get('res.duplicate')
            if not self.report:
                self.report.append('No exception.')
            summary = '''Here is the duplication report:

Duplication started: %s
Duplication finished: %s

Duplicate records: %d
Records updated: %d
Records created: %d

Exceptions:
                ''' % (start_date, end_date, self.report_total,
                       self.report_write, self.report_create)
            summary += '\n'.join(self.report)
            if res_dupli:
                res_dupli.create(cr, uid, {
                    'name': "Duplication report",
                    'act_from': self.user_id.id,
                    'date': time.strftime('%Y-%m-%d, %H:%M:%S'),
                    'act_to': duplicate_wzd.user_id.id,
                    'body': summary,
                }, context=context)
            return {}

    @api.multi
    def duplication_multi_thread(self):
        source = self.duplicate_order()
        threaded_synchronization = threading.Thread(target=source)
        threaded_synchronization.run()
        data_obj = self.env['ir.model.data']
        id2 = data_obj._get_id('ib_duplicate_purchase_sale', 'view_duplicate_order_finish')
        if id2:
            id2 = data_obj.browse(id2).res_id
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'duplicate.order',
            'views': [(id2, 'form')],
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    @api.model
    def get_product_details(self, pool_dest, line, var=''):
        product_obj = pool_dest.get('product.product')
        product_categ_obj = pool_dest.get('product.category')
        #('name_template', 'like', str(line.product_id.name_template).split(" ")[0] + '%')
        product_ids = product_obj.search(self._cr, SUPERUSER_ID,
                ['&',('name_template', 'like', '%' + str(line.product_id.name_template) + '%'),
                    ('default_code', 'like', str(line.product_id.default_code))],
                    limit=1)

        result = False
        if product_ids:
            product = product_obj.read(self._cr, SUPERUSER_ID, product_ids, ['product_tmpl_id','name_template','type','description_sale','property_account_expense','categ_id'])[0]
            if product['type'] == 'product':
                if var=='product_id':
                    result = product_ids and product_ids[0] or False    ##line.product_id.id
                elif var == 'description':
                    result = product['name_template'] or product['description_sale']
                elif var == 'account_id':
                    if not product['property_account_expense']:
                        raise except_orm(_('Error Account!!!'), _("Result: %s\n%s\n%s") % (str(product['name_template']),product['categ_id'],product['property_account_expense']))
                    if product['property_account_expense']:
                        result = product['property_account_expense'][0]
                    elif product['categ_id']:
                        product_categ = product_categ_obj.read(self._cr, SUPERUSER_ID, [product['categ_id'][0]], [])[0]
                        if product_categ['property_account_expense_categ']:
                            result = product_categ['property_account_expense_categ'][0]
                        else:
                            result = False
                    else:
                        result = False
            else:
                result = False if var=='product_id' else ''
        else:
            result = False if var == 'product_id' else ''  ##line.product_id.id
        return result

    @api.model
    def get_uom_id(self, pool_dest, line):
        uom_obj = pool_dest.get('product.uom')
        uom_ids = uom_obj.search(self._cr, SUPERUSER_ID,
                [('name', 'like', '%' + str(line.product_uom.name) + '%'),
                 ('name', 'like', str(line.product_uom.name).split(" ")[0] + '%')],limit=1)
        if not uom_ids:
            raise except_orm(_('Warning!'), _("Satuan produk (UoM) tidak tersedia di Database PB1.\n'%s'") % (str(line.product_uom.name),))
        return uom_ids and uom_ids[0] or line.product_uom.id or False       ##line.product_uom.id


    def _prepare_sale_order(self, data, customer, user_id, warehouse_id):
        customer_pricelist = customer['property_product_pricelist']
        currency_id = data.currency_id and data.currency_id.id or data.company_id.currency_id.id
        origin2 = data.create_uid and data.create_uid.login or \
                 data.picking_type_id and data.picking_type_id.warehouse_id.outlet
        return {
            'origin': "%s | %s" % (data.name or '', origin2 or '',),
            'date_order': data.date_order or fields.datetime.now(),
            'partner_id': customer['id'],
            'pricelist_id': customer_pricelist[0] or 1,
            'currency_id': currency_id or 13,
            'company_id': data.company_id.id or 1,
            'fiscal_position': customer['property_account_position'],
            'note': "%s" % (data.notes or '',),
            'order_policy': 'manual',
            'user_id': user_id,
            'warehouse_id': warehouse_id,
            'state': 'draft',
            'purchase_order': True,
            'related_docs': data.name,
        }

    def _prepare_sale_order_line(self, line, sale_id, customer, user_id, product_id, uom_id, description):
        return {
            'order_id': sale_id,
            'name': description or line.name,
            'product_id': product_id or False,
            'price_unit': line.price_unit or 1.0,
            'state': 'draft',
            'product_uom_qty': line.product_qty,
            'product_uom': uom_id,
            'product_uos_qty': line.product_qty,
            'product_uos': uom_id,
            'tax_id': [(6, 0, [x.id for x in line.taxes_id])],
            'order_partner_id': customer['id'],
            'salesman_id': user_id,
            'company_id': line.company_id.id,
            'requirement_type': line.requirement_type,
        }

    @api.model
    def remove_duplicate_history(self, pool_dest, pool_src, history_dst_ids, history_src_ids, real_uid, uid):
        pool_dest.get('duplicate.history').unlink(self._cr, real_uid, history_dst_ids)
        pool_src.get('duplicate.history').unlink(self._cr, uid, history_src_ids)
        return True

    @api.model
    def create_duplicate_history(self, pool_src, pool_dest, src_mdl_id, src_id, dest_mdl_id, dest_id, uid, real_uid):
        pool_src.get('duplicate.history').create(self._cr, uid, {
            'src_model_id': src_mdl_id,
            'source_id': src_id,
            'dest_model_id': dest_mdl_id,
            'destination_id': dest_id,
            'user_id': uid,
        })
        pool_dest.get('duplicate.history').create(self._cr, real_uid, {
            'src_model_id': dest_mdl_id,
            'source_id': dest_id,
            'dest_model_id': src_mdl_id,
            'destination_id': src_id,
            'user_id': real_uid,
        })
        return True

    def _prepare_purchase_order(self, data, supplier, pick_type_id, loc_dest_id, user_id):
        supplier_pricelist = supplier['property_product_pricelist_purchase']
        currency_id = data.currency_id and data.currency_id.id or data.company_id.currency_id.id
        return {
            #'origin': data.name,
            'date_order': data.validity_date or data.date_order or fields.datetime.now(),
            'partner_id': supplier['id'],
            'pricelist_id': supplier_pricelist[0] or 1,
            'currency_id': currency_id or 13,
            'location_id': loc_dest_id[0] or False,
            'company_id': data.company_id.id or 1,
            'fiscal_position': supplier['property_account_position'] or False,
            'related_docs': data.name,
            'notes': data.note,
            'picking_type_id': pick_type_id,
            'create_uid': user_id,
            'write_uid': user_id,
            'sales_order': True,
            'user_id': user_id,
        }

    def _prepare_purchase_order_line(self, line, purchase_id, product_id, uom_id, description, analytic_id):
        return {
            'order_id': purchase_id,
            'name': description or line.name,
            'product_id': product_id or False,
            'product_qty': line.product_uom_qty,
            'product_uom': uom_id,
            'price_unit': line.price_unit or 1.0,
            'state': 'draft',
            'date_planned': line.order_id.validity_date or time.strftime('%Y-%m-%d, %H:%M:%S') or False,
            'requirement_type': line.requirement_type,
            'account_analytic_id': analytic_id or False,
        }

    @api.model
    def get_anaytic_id(self, pool_dest, account_id, warehouse_id):
        # analytic_obj = pool_dest.get('account.analytic.account')
        analytic_cbg_obj = pool_dest.get('analytic.cabang')
        analytic_cabang_ids = analytic_cbg_obj.search(self._cr, SUPERUSER_ID,
                                 [('name', '=', warehouse_id), ('account_id', '=', account_id)], limit=1)
        # analytic_cabang = analytic_cbg_obj.browse(self._cr, SUPERUSER_ID, analytic_cabang_ids)[0]
        analytic_cabang = analytic_cbg_obj.read(self._cr, SUPERUSER_ID, analytic_cabang_ids, [])[0]
        if not analytic_cabang_ids:
            raise except_orm(_('Warning!'), _("Mapping analytic cabang tidak tersedia di Database PB1."))
        return analytic_cabang_ids and analytic_cabang['analytic_id'][0] or False




class purchase_order(osv.osv):
    _inherit = "purchase.order"

    def action_cancel_draft(self, cr, uid, ids, context=None):
        if not len(ids):
            return False
        self.write(cr, uid, ids, {'state':'draft','shipped':0})
        self.set_order_line_status(cr, uid, ids, 'draft', context=context)
        for po in self.browse(cr, SUPERUSER_ID, ids, context=context):
            po_model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'purchase.order')])
            duplicate_ids = self.pool.get('duplicate.history').search(cr, uid, [
                ('src_model_id', '=', po_model_ids[0]), ('source_id', '=', po.id)], context=context)
            if po.sales_order or duplicate_ids:
                raise osv.except_osv(
                    _('Tidak boleh merubah status purchase order %s menjadi draft.') % (po.name),
                    _('PO ini sudah pernah dibuatkan SO di DB LOGISTIK. Silahkan buat PO baru...'))
            for picking in po.picking_ids:
                picking.move_lines.write({'purchase_line_id': False})
            for invoice in po.invoice_ids:
                po.write({'invoice_ids': [(3, invoice.id, _)]})
            for po_line in po.order_line:
                for invoice_line in po_line.invoice_lines:
                    po_line.write({'invoice_lines': [(3, invoice_line.id, _)]})
        for p_id in ids:
            # Deleting the existing instance of workflow for PO
            self.delete_workflow(cr, uid, [p_id]) # TODO is it necessary to interleave the calls?
            self.create_workflow(cr, uid, [p_id])
        return True

    def action_cancel(self, cr, uid, ids, context=None):
        dupli_svr_conf = self.pool.get('duplicate.config.server')
        svr_conf_ids = dupli_svr_conf.search(cr, uid or SUPERUSER_ID,
                        ['|',('name','like','%LOGISTIK%'),('server_db','like','%LOGISTIK%')], limit=1)  #SENLOG
        if svr_conf_ids:
            server = dupli_svr_conf.browse(cr, uid or SUPERUSER_ID, svr_conf_ids and svr_conf_ids[0])
            pool_dest = RPCProxy(server, SUPERUSER_ID)
            sale_obj = pool_dest.get('sale.order')
        dupli_history = self.pool.get('duplicate.history')
        for purchase in self.browse(cr, uid, ids, context=context):
            for pick in purchase.picking_ids:
                for move in pick.move_lines:
                    if pick.state == 'done':
                        raise osv.except_osv(
                            _('Unable to cancel the purchase order %s.') % (purchase.name),
                            _('You have already received some goods for it.  '))
            self.pool.get('stock.picking').action_cancel(cr, uid,
                        [x.id for x in purchase.picking_ids if x.state != 'cancel'],
                        context=context)
            for inv in purchase.invoice_ids:
                if inv and inv.state not in ('cancel', 'draft'):
                    raise osv.except_osv(
                        _('Unable to cancel this purchase order.'),
                        _('You must first cancel all invoices related to this purchase order.'))
            self.pool.get('account.invoice') \
                .signal_workflow(cr, uid, map(attrgetter('id'), purchase.invoice_ids), 'invoice_cancel')

            po_model_ids = self.pool.get('ir.model').search(cr, uid, [('model', '=', 'purchase.order')])
            duplicate_ids = dupli_history.search(cr, uid, [
                ('src_model_id', '=', po_model_ids[0]), ('source_id', '=', purchase.id)], context=context)
            if purchase.sales_order or duplicate_ids:
                sales_orders = dupli_history.read(cr, uid, duplicate_ids, ['dest_model_id', 'destination_id'],
                                                     context=context)
                if svr_conf_ids:
                    todo_list = []
                    for s in sales_orders:
                        sale_related = sale_obj.read(cr, uid or SUPERUSER_ID, [s['destination_id']], [])[0]
                        if sale_related['state'] not in ('cancel', 'draft'):
                            raise osv.except_osv(
                                _('Unable to cancel this purchase order.'),
                                _('SO LOGISTIK is already created for this purchase order.'))
                        else:
                            if sale_related['id'] not in todo_list:
                                todo_list.append(sale_related['id'])
                    sale_obj.action_cancel(cr, uid or SUPERUSER_ID, todo_list, context=context)
        self.signal_workflow(cr, uid, ids, 'purchase_cancel')
        return True




    # @api.model
    # def duplication_sale_to_purchase(self, server):
    #     context = dict(self._context or {})
    #     active_ids = self._context.get('active_ids', [])
    #     pool = pooler.get_pool(self.env.cr.dbname)
    #     uid = self.user_id.id or SUPERUSER_ID
    #
    #     self.meta = {}
    #     purchases = {}
    #     pool_src = pool
    #     pool_dest = RPCProxy(server, SUPERUSER_ID)
    #
    #     sale_obj = pool_src.get('sale.order')
    #     users_obj = pool_dest.get('res.users')
    #     sales = sale_obj.browse(self._cr, SUPERUSER_ID, active_ids)[0]
    #     if sales.partner_id and sales.partner_id.code:
    #         key = 'adm' + sales.partner_id.code.strip().lower()
    #     elif self.env.user.login:
    #         key = str(self.env.user.login).lower()
    #     else:
    #         key = 'admin'
    #     real_uid = users_obj.search(self._cr, SUPERUSER_ID, [('login', 'like', '%' + key + '%')], limit=1)[0] or uid
    #     # pool_dest = RPCProxy(server, real_uid)
    #
    #     purchase_obj = pool_dest.get('purchase.order')
    #     purchase_line_obj = pool_dest.get('purchase.order.line')
    #     history_src_obj = pool_src.get('duplicate.history')
    #     partner_obj = pool_dest.get('res.partner')
    #     warehouse_obj = pool_dest.get('stock.warehouse')
    #     sequence_obj = pool_dest.get('ir.sequence')
    #     pick_type_obj = pool_dest.get('stock.picking.type')
    #     history_obj = pool_dest.get('duplicate.history')
    #     product_obj = pool_dest.get('product.product')
    #     so_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order')])
    #     so_line_model_ids = pool_src.get('ir.model').search(self._cr, uid, [('model', '=', 'sale.order.line')])
    #     po_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order')])
    #     po_line_model_ids = pool_dest.get('ir.model').search(self._cr, uid, [('model', '=', 'purchase.order.line')])
    #
    #     for sale in sale_obj.browse(self._cr, SUPERUSER_ID, active_ids):
    #         exist_oid = self.get_id(so_model_ids and so_model_ids[0], sale.id)  # check from history of duplication
    #         if exist_oid:
    #             purchases = purchase_obj.read(self._cr, real_uid, [exist_oid], [])[0]
    #             if (exist_oid and purchases['state'] not in ('draft')):  # sale.purchase_order or ,'sent','bid'
    #                 raise except_orm(_('Warning!!!'),
    #                                  _(
    #                                      "Purchase Order (Outlet) berstatus '%s' dan tidak bisa diduplikasi. Status yang diperbolehkan yaitu draft, sent, bid.") %
    #                                  purchases['state'])
    #         partner_ids = partner_obj.search(self._cr, real_uid, ['&', '|', ('supplier', '=', True),
    #                                                               ('name', 'like', '%' + str(
    #                                                                   sale.warehouse_id.partner_id.name).strip().replace(
    #                                                                   'DCOST ', '')),
    #                                                               ('name', 'like',
    #                                                                '%' + str(sale.warehouse_id.name).strip().replace(
    #                                                                    'PB', ''))])
    #         if not partner_ids:
    #             raise except_orm(_('Warning!'), _("Partner name (Supplier) tidak tersedia di Database PB1."))
    #         else:
    #             wh_ids = warehouse_obj.search(self._cr, real_uid, [('code', '!=', '')])
    #             warehouses = warehouse_obj.read(self._cr, real_uid, wh_ids, context=context)  # [0]
    #             outlet_codes = [w['code'] for w in warehouses]
    #             if (not sale.partner_id.code) or (sale.partner_id.code not in outlet_codes):
    #                 raise except_orm(_('Warning!'), _(
    #                     "Partner code [Kode Customer SO] are not available in DB PB1 [Warehouse Code]. - result: %s") % (
    #                                  sale.partner_id.code,))
    #             warehouse_ids = warehouse_obj.search(self._cr, real_uid,
    #                                                  [('code', 'like', '%' + sale.partner_id.code + '%')],
    #                                                  limit=1) or []
    #             pick_type_ids = pick_type_obj.search(self._cr, real_uid,
    #                                                  [('warehouse_id', '=',
    #                                                    warehouse_ids and warehouse_ids[0] or False)], limit=1)
    #             picking_type = pick_type_obj.read(self._cr, real_uid, pick_type_ids, context=context)[0]
    #             supplier = partner_obj.read(self._cr, real_uid, partner_ids, context=context)[0]
    #             values = self._prepare_purchase_order(sale, supplier, pick_type_ids and pick_type_ids[0] or False,
    #                                                   picking_type['default_location_dest_id'], real_uid)
    #             if exist_oid:
    #                 purchase_id = exist_oid
    #                 po_number = purchases['name']
    #                 if purchases['state'] == 'draft':
    #                     sol_ids = []
    #                     purchase_obj.write(self._cr, real_uid, [purchase_id], values)
    #                     for line in sale.order_line:
    #                         product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                         product_description = self.get_product_details(pool_dest, line, 'description')
    #                         uom_id = self.get_uom_id(pool_dest, line)
    #                         exist_lid = self.get_id(so_line_model_ids and so_line_model_ids[0], line.id)
    #                         # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
    #                         account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                         analytic_id = self.get_anaytic_id(pool_dest, account_id,
    #                                                           warehouse_ids and warehouse_ids[0] or False)
    #                         line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id,
    #                                                                       product_description, analytic_id)
    #                         if not uom_id:
    #                             raise except_orm(_('Warning!'),
    #                                              _("Satuan produk (UoM) tidak tersedia di Database PB1."))
    #                         if exist_lid and product_id and uom_id:
    #                             dest_oline = purchase_line_obj.read(self._cr, uid, [exist_lid], [])[0]
    #                             if dest_oline['state'] == 'draft':
    #                                 purchase_line_obj.write(self._cr, real_uid, [exist_lid], line_vals)
    #                         else:
    #                             add_pur_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
    #                             self.create_duplicate_history(pool_src, pool_dest,
    #                                                           so_line_model_ids and so_line_model_ids[0]
    #                                                           or False, line.id or False,
    #                                                           po_line_model_ids and po_line_model_ids[0] or False,
    #                                                           add_pur_line_id or False, uid, real_uid)
    #                         if line.id not in sol_ids:
    #                             sol_ids.append(line.id)
    #
    #                     if sol_ids:
    #                         pol_ids = purchase_line_obj.search(self._cr, real_uid, [('order_id', '=', purchase_id)])
    #                         history_dst_ids = history_obj.search(self._cr, real_uid,
    #                                                              [('source_id', 'in', tuple(pol_ids)),
    #                                                               ('destination_id', 'not in', tuple(sol_ids))])
    #                         history_src_ids = history_src_obj.search(self._cr, uid,
    #                                                                  [('source_id', 'not in', tuple(sol_ids)),
    #                                                                   ('destination_id', 'in', tuple(pol_ids))])
    #                         if history_dst_ids:
    #                             for res in history_obj.read(self._cr, real_uid, history_dst_ids, ['source_id']):
    #                                 purchase_line_obj.unlink(self._cr, real_uid, [res['source_id']])
    #                         if history_src_ids or history_dst_ids:
    #                             self.remove_duplicate_history(pool_dest, pool_src, history_dst_ids, history_src_ids,
    #                                                           real_uid, uid)
    #                     purchase_obj.message_post(self._cr, real_uid, [purchase_id],
    #                                               body=_("Purchase Order has been changed"))
    #                     self.report_total += 1
    #                     self.report_write += 1
    #                 else:
    #                     raise except_orm(_('Warning!'),
    #                                      _("Data can not be changed or deleted because it is not a draft."))
    #             else:  # create: purchase_order outlet
    #                 purchase_order_number = sequence_obj.next_by_code(self._cr, real_uid, 'purchase.order.dcost',
    #                                                                   context=context)
    #                 nomor_po = list(str(purchase_order_number))  # PO/12345 -> PO/sunter/12345
    #                 nomor_po.insert(3, str(sale.partner_id.code) + "/")
    #                 po_number = ''.join(nomor_po)
    #                 values.update({'name': str(po_number)})
    #                 purchase_id = purchase_obj.create(self._cr, real_uid, values, context={'mail_create_nolog': True})
    #                 for line in sale.order_line:
    #                     product_id = self.get_product_details(pool_dest, line, 'product_id')
    #                     product_description = self.get_product_details(pool_dest, line, 'description')
    #                     uom_id = self.get_uom_id(pool_dest, line)
    #                     # product = product_obj.browse(self._cr, SUPERUSER_ID, product_id)
    #                     account_id = self.get_product_details(pool_dest, line, 'account_id')
    #                     analytic_id = self.get_anaytic_id(pool_dest, account_id,
    #                                                       warehouse_ids and warehouse_ids[0] or False)
    #                     if not uom_id:
    #                         raise except_orm(_('Warning!'), _("Product UoM are not available in the PB1 Database."))
    #                     if product_id and uom_id:
    #                         line_vals = self._prepare_purchase_order_line(line, purchase_id, product_id, uom_id,
    #                                                                       product_description, analytic_id)
    #                         purchases_line_id = purchase_line_obj.create(self._cr, real_uid, line_vals)
    #                         self.create_duplicate_history(pool_src, pool_dest,
    #                                                       so_line_model_ids and so_line_model_ids[0]
    #                                                       or False, line.id or False,
    #                                                       po_line_model_ids and po_line_model_ids[0] or False,
    #                                                       purchases_line_id or False, uid, real_uid)
    #                 purchase_obj.message_post(self._cr, real_uid, [purchase_id],
    #                                           body=_("Purchase Order created"), context={'mail_create_nolog': True})
    #                 self.create_duplicate_history(pool_src, pool_dest, so_model_ids and so_model_ids[0] or False,
    #                                               sale.id or False, po_model_ids and po_model_ids[0] or False,
    #                                               purchase_id or False, uid, real_uid)
    #                 self.report_total += 1
    #                 self.report_create += 1
    #
    #             purchase_obj.button_dummy(self._cr, uid, [purchase_id])
    #             sale_obj.write(self._cr, uid, [sale.id], {'purchase_order': True, 'related_docs': po_number or ''})
    #             self.meta = {}
    #
    #     return True
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
