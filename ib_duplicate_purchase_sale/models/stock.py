# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782 | http://ibrohimbinladin.wordpress.com
##########################################################################################################

# from openerp import models, fields, api, _
#
#
# class StockOrderpoint(models.Model):
#     _inherit = "stock.warehouse.orderpoint"
#
#     @api.model
#     def default_get(self, fields):
#         warehouse_obj = self.env['stock.warehouse']
#         user = self.env['res.users'].browse(self.env.user.id)
#         res = super(StockOrderpoint, self).default_get(fields)
#         if 'warehouse_id' not in res:
#             warehouse_ids = res.get('company_id') and warehouse_obj.search([('company_id', '=', res['company_id'])], limit=1) or []
#             if user.warehouse_id:
#                 res['warehouse_id'] = user.warehouse_id and user.warehouse_id.id
#             else:
#                 res['warehouse_id'] = warehouse_ids and warehouse_ids[0] or False
#         if 'location_id' not in res:
#             res['location_id'] = res.get('warehouse_id') and warehouse_obj.browse(res['warehouse_id']).lot_stock_id.id or False
#         return res


